Gideon Steinberg
================

I did the first gui implementation. This includes the timer_event function, the destroy function, the first implementation of the draw_car function, the draw function, the first implementation of the expose_event_callback function, and the init_gui function. These functions allow the gui to be drawn.

I also did the straight intersection implementation. This includes the click_go function the add_intersection function, the is_valid_intersection function, the first iteration of the mouse_pressed implementation allowing left clicks to add an intersection, the draw_intersection function, the undraw_intersection_square function and the init_intersections function.

I started verification by simulation. There is a flag VERIFY_GUI to run this simulation. It will then call the verify_is_valid_function which is a simulation function that checks is_valid_intersection as well as add_intersection. It tests that this CTL formula always holds for all intersections i1 and i2:
NOT ( (|i1.x-i2.x) < width + padding ) V (|i1.y-i2.y) < height + padding ) )

I had some experience with blast and got asserts and reachibility working. The reachibility can be found in gui/verification/verify_intersections.c while the assert can be found in gui/verification/verify_intersections_asserts.c. These files check very small properties intersection.x < 0 and intersection.y < 0, as I was not able to test any other properties properly. I had to simplify the intersection code quite dramatically so I do not think that it was worthwhile (I could not use functions and a lot of stuff), but it was interesting to get working.



Shafqat Bhuiyan
===============

I first set up the initial git repository and wrote the help file for git. I also set up the directory structure for the project. Once Gideon had the gui implementation working, I modified it to use Cairo for drawing rather than plain GTK. This gave us more flexibility and allowed easier implementation of features later on.

I then added the zooming and panning functionality using the scroll wheel and the middle mouse button. I also added the ability to create curved roads by right click dragging. I also changed the traffic lights to be arrows instead of squares and made the background an image of grass.

Along the way I also made optimzations to make the GUI more responsive.

For verification I tested the code for zooming in and out. This can be found in verify_zoom.c. This verifies that the GUI does not zoom out or zoom in too much. This verification is done using assertions.
