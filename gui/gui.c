#include "globals.h"

GtkWidget *window;

int simulation_is_running = 0;

float view_scale = 0.3;           // The zoom/scale of the view

//The center of the view. Don't like "initializer element is not constant" errors
float view_center_x = (MAP_WIDTH / 2.0);
float view_center_y = (MAP_HEIGHT / 2.0);

//What are these?
float old_view_center_x = 1000;
float old_view_center_y = 1000;

int moving_view = FALSE;        // Whether the view is being moved
int moving_view_origin_x = 0;   // The point at which the movement was initiated
int moving_view_origin_y = 0;
cairo_matrix_t view_matrix;     // The matrix which represents the of the view

bool arrow_being_created = false; // If the user is creating an arrow by dragging
Point arrow_origin;     // The point at which an arrows is being created
Point arrow_end;     // The temporary end point of the arrow
Arrow *parent_arrow = NULL; // The arrow to extend from

cairo_surface_t *background;

void destroy(GtkWidget *widget, gpointer data)
{
    to_continue = 0;
    cairo_surface_destroy(background);
}

void timer_event(GtkWidget *widget){
    if (simulation_is_running){
        //update all other parts only if the simulation is running
        update_infrastructure();
        update_vehicle();
        update_controller();
    }  

    repaint();
}

void verify_intersection_is_valid_function(){
    /* 
    verify that the intersection is valid function works
    it checks the following ctl formula always holds
    for any intersections i1 and i2 (i1 and i2 arnt the same intersection)
    that are valid using is_valid_intersection()
    (no BLAST although that would be nice)

    NOT ( (|i1.x-i2.x| < width + padding) V (|i1.y-i2.y| < height + padding) )
    

    while writing this a bug was found in init_intersections, that number_of_intersections is not set to 0
    */

    int i = 1;
    int j = 1;

    int intersection_diff_x;
    int intersection_diff_y;
    init_intersections();
    
    // add maximum intersections possible to find collisions
    for (i = 1; i < MAP_WIDTH;i++){
        for (j = 1; j < MAP_HEIGHT;j++){
            if (is_valid_intersection(i,j)){
                add_intersection(i,j, false);
            }
        }
    }

    // check that no two intersections are invalid
    // they are invalid if they cross each other
    for (i = 0; i < number_of_intersections;i++){
        for (j = i + 1; j < number_of_intersections;j++){
            intersection_diff_x = intersections[i].origin.x - intersections[j].origin.x;
            intersection_diff_y = intersections[i].origin.y - intersections[j].origin.y;
            if (intersection_diff_x <   (intersection_width  + intersection_padding) 
             && intersection_diff_x > - (intersection_width  + intersection_padding) 
             && intersection_diff_y <   (intersection_height + intersection_padding) 
             && intersection_diff_y > - (intersection_height + intersection_padding)){

                g_print("invalid intersection found, coords are %f,%f and %f,%f\n",intersections[i].origin.x, intersections[i].origin.y, intersections[j].origin.x, intersections[j].origin.y);
                g_print("the differences are x:%d, y:%d\n", intersection_diff_x, intersection_diff_y);
            }
        }
    }
    free(intersections);
}

void click_go(GtkWidget *widget, gpointer data)
{
    int diff_x_position = -1;
    int diff_y_position = -1;

    if (number_of_intersections == 0){
        add_intersection(MAP_WIDTH/2,MAP_HEIGHT/2,0);
        add_intersection(MAP_WIDTH/4,MAP_HEIGHT/4,0);
        add_intersection(MAP_WIDTH/2,MAP_HEIGHT/4,0);
        add_intersection(MAP_WIDTH/4,MAP_HEIGHT/2,0);
    } else if (number_of_intersections == 1){

        diff_x_position = - 250;
        if (intersections[0].origin.x < 300){
            diff_x_position = 250;
        }
        diff_y_position = - 250;
        if (intersections[0].origin.y < 300){
            diff_y_position = 250;
        }
        add_intersection(intersections[0].origin.x + diff_x_position,intersections[0].origin.y + diff_y_position,0);
        add_intersection(intersections[0].origin.x,intersections[0].origin.y + diff_y_position,0);
        add_intersection(intersections[0].origin.x + diff_x_position,intersections[0].origin.y,0);
    }
    init_infrastructure();
    init_vehicle();
    init_controller();
    
    simulation_is_running = 1;
}

void add_intersection(int x, int y, int isGenerated){
    Intersection* temp;
    int i = 0;
    // if there is space add it, otherwize increase the size of the array
    if (number_of_intersections < max_number_of_intersections){
        intersections[number_of_intersections].origin.x = x;
        intersections[number_of_intersections].origin.y = y;
        intersections[number_of_intersections].width = intersection_width;
        intersections[number_of_intersections].height = intersection_height;
        intersections[number_of_intersections].isGenerated = isGenerated;
        number_of_intersections++;                
    } else {
        // reallocate memory for the dynamic array
        temp = intersections;
        max_number_of_intersections = max_number_of_intersections * 2;
        intersections = (Intersection*) malloc(max_number_of_intersections * sizeof(Intersection));
        // copy over all intersections                
        for (i = 0; i < number_of_intersections;i++){
            intersections[i] = temp[i];
        }
        free(temp);
        // create a new intersection
        intersections[number_of_intersections].origin.x = x;
        intersections[number_of_intersections].origin.y = y;
        intersections[number_of_intersections].width = intersection_width;
        intersections[number_of_intersections].height = intersection_height;
        intersections[number_of_intersections].isGenerated = isGenerated;
        number_of_intersections++; 
    }
}

int is_valid_intersection(int x, int y){

    // Check if the intersection is within map boundaries
    if(x < intersection_width / 2 ||
            y < intersection_height / 2 ||
            x > MAP_WIDTH - intersection_width / 2 ||
            y > MAP_HEIGHT - intersection_height / 2)
    {
        return FALSE;
    }


    int i = 0;
    for (i = 0; i < number_of_intersections;i++){

        // if the y coords are equal then make sure the x coords are valid
	// this ensures that if you are adding a horizonal segment it is valid

        // if the x coords are equal then make sure the y coords are valid
	// this ensures that if you are adding a vertical segment it is valid

	// if neither of these first two conditions are met (x's are not equal or y's are not equal)
	// then make sure x and y are valid

	// if none of the above conditions are met return 0 - it is invalid 
        if ((int)y == (int)intersections[i].origin.y && (
                    x > intersections[i].origin.x + intersections[i].width + intersection_padding  || 
                    x < intersections[i].origin.x - intersections[i].width - intersection_padding)){
        } else if ((int)x == (int)intersections[i].origin.x && (
                    y > intersections[i].origin.y + intersections[i].height + intersection_padding || 
                    y < intersections[i].origin.y - intersections[i].height - intersection_padding)){
        } else if ((x > intersections[i].origin.x + intersections[i].width + intersection_padding  || 
                    x < intersections[i].origin.x - intersections[i].width - intersection_padding) &&
                (y > intersections[i].origin.y + intersections[i].height + intersection_padding || 
                 y < intersections[i].origin.y - intersections[i].height - intersection_padding)){
        } else {
            return FALSE;   
        }
    }
    return TRUE;
}

// Convert a widget coordinate to a canvas coordinate
void transform_to_canvas(float *x, float *y) {
    double dx = *x;
    double dy = *y;
    cairo_matrix_t inverted_matrix = view_matrix;
    cairo_matrix_invert(&inverted_matrix);
    cairo_matrix_transform_point (&inverted_matrix, &dx, &dy);
    *x = dx;
    *y = dy;
}


gboolean mouse_pressed (GtkWidget* widget,
  GdkEventButton * event, GdkWindowEdge edge)
{    
    if (event->type == GDK_BUTTON_PRESS)
    {
         if (event->button == 1) {
             // if the simulation is running do not add intersections
             if (simulation_is_running){
                 return FALSE;
             }

             // verify that the intersection is valid
             float x = event->x;
             float y = event->y;

             transform_to_canvas(&x, &y);

             // Snap to nearest intersection on each axis
             int i = 0;
             for (i = 0; i < number_of_intersections;i++){
                 if(fabs(x - intersections[i].origin.x) < intersection_width / 2) {
                     x = intersections[i].origin.x;
                 }
                 if(fabs(y - intersections[i].origin.y) < intersection_height / 2) {
                     y = intersections[i].origin.y;
                 }
             }


             if (is_valid_intersection(x, y)){
                 add_intersection(x, y, FALSE);

                 int old_number_of_intersections = number_of_intersections;

                 // add the other intersections
                 for (i = 0; i < old_number_of_intersections;i++){
                     // We don't need to generate intersections for previously generated intersections.
                     // This is because they will already be generated by other intersections
                     if (intersections[i].isGenerated == FALSE) {
                         if(is_valid_intersection(x, intersections[i].origin.y)) {
                             add_intersection(x, intersections[i].origin.y, TRUE);
                         }
                         if(is_valid_intersection(intersections[i].origin.x, y)) {
                             add_intersection(intersections[i].origin.x, y, TRUE);
                         }
                     }
                 }
             }
         } else if (event->button == 3) {
             arrow_origin.x = event->x;
             arrow_origin.y = event->y;

             transform_to_canvas(&arrow_origin.x, &arrow_origin.y);

             arrow_end = arrow_origin;


             // Snap to the nearest arrow if possible
             float minimum_distance = 99999;
             for(int i = 0; i < num_arrows; i++) {
                 float dist = distance(arrows[i].end, arrow_origin);
                 if(dist < intersection_width && dist < minimum_distance) {
                     minimum_distance = dist;
                     arrow_origin = arrows[i].end;
                     parent_arrow = &arrows[i];
                 }
             }

             arrow_being_created = true;

         } else if (event->button == 2) { // If middle mouse button pressed
             moving_view = TRUE;
             moving_view_origin_x = event->x;
             moving_view_origin_y = event->y;
             old_view_center_x = view_center_x;
             old_view_center_y = view_center_y;
         }
    }

    return FALSE;
}

gboolean mouse_released (GtkWidget* widget,
        GdkEventButton * event, GdkWindowEdge edge)
{    
    // If middle mouse button was released
    if(event->type == GDK_BUTTON_RELEASE) {
        if (event->button == 2) {
            moving_view = FALSE;
        } else if (event->button == 3) {
            arrow_being_created = false;

            arrow_end.x = event->x;
            arrow_end.y = event->y;
            transform_to_canvas(&arrow_end.x, &arrow_end.y);

            // Snap to the nearest arrow if possible
            float minimum_distance = 99999;
            Arrow *nearest_arrow = NULL;
            for(int i = 0; i < num_arrows; i++) {
                float dist = distance(arrows[i].start, arrow_end);
                if(dist < intersection_width && dist < minimum_distance) {
                    minimum_distance = dist;
                    arrow_end = arrows[i].start;
                    nearest_arrow = &arrows[i];
                }
            }

            // Create a new arrow
            Arrow *a = (Arrow *)malloc(sizeof(Arrow));
            init_arrow(a, arrow_origin.x, arrow_origin.y, arrow_end.x, arrow_end.y);
            arrows[num_arrows++] = *a;

            if(parent_arrow != NULL) {
                a->startConnectors[0] = parent_arrow;

                // Attach to the end of the parent arrow
                for(int i = 0; i < MAX_ARROW_CONNECTORS; i++) {
                    if(parent_arrow->endConnectors[i] == NULL) {
                        parent_arrow->endConnectors[i] = a;
                        break;
                    }
                }

                parent_arrow = NULL;
            }

            if(nearest_arrow != NULL) {
                // End at the nearest arrow
                a->endConnectors[0] = nearest_arrow;

                // Attach to the end of the parent arrow
                for(int i = 0; i < MAX_ARROW_CONNECTORS; i++) {
                    if(nearest_arrow->startConnectors[i] == NULL) {
                        nearest_arrow->startConnectors[i] = a;
                        break;
                    }
                }
            }
        }
    }

    return FALSE;
}

gboolean mouse_moved (GtkWidget* widget,
        GdkEventMotion * event, GdkWindowEdge edge)
{    

    if (moving_view) {
        // Adjust the center of the view
        view_center_x = old_view_center_x + (moving_view_origin_x - event->x)/view_scale;
        view_center_y = old_view_center_y + (moving_view_origin_y - event->y)/view_scale;

        repaint();

    }

    if(arrow_being_created) {
        arrow_end.x = event->x;
        arrow_end.y = event->y;
        transform_to_canvas(&arrow_end.x, &arrow_end.y);

        repaint();
    }


    return FALSE;
}


gboolean mouse_wheel_scrolled (GtkWidget* widget,
        GdkEventScroll * event, GdkWindowEdge edge)
{    

    if (event->direction == GDK_SCROLL_UP) {
        // Avoid zooming in too much
        if(view_scale < 5) {
            view_scale *= 1.1;
        }
    } else if (event->direction == GDK_SCROLL_DOWN) {
        // Avoid zooming out too much
        if(view_scale * MAP_WIDTH * 1.2 > widget->allocation.width ||
                view_scale * MAP_HEIGHT * 1.2 > widget->allocation.height) {
            view_scale /= 1.1;
        }
    }

    repaint();

    return FALSE;
}


void draw_car(cairo_t *cr, int i, int j)
{
    if (all_lanes[i].cars[j].invisible){
        if (!DEBUG){
            return;
        }
        cairo_set_source_rgb(cr, 0, 1, 0);
    } else {
        if (all_lanes[i].cars[j].turn_intention == LEFT_TURN) {
            cairo_set_source_rgb(cr, 1, 0, 1);
        } else {
            cairo_set_source_rgb(cr, 0, 0, 1);
        }
    }
    if(all_lanes[i].direction == LEFT || all_lanes[i].direction == RIGHT) {
        cairo_rectangle(cr, all_lanes[i].cars[j].location.x - CAR_LENGTH/2, all_lanes[i].cars[j].location.y - CAR_WIDTH/2, CAR_LENGTH, CAR_WIDTH); 
    } else {
        cairo_rectangle(cr, all_lanes[i].cars[j].location.x - CAR_WIDTH/2, all_lanes[i].cars[j].location.y - CAR_LENGTH/2, CAR_WIDTH, CAR_LENGTH); 
    }

    cairo_fill(cr);
}

void draw_line(cairo_t *cr, float x, float y, float x2, float y2) {
    cairo_move_to(cr, x, y);
    cairo_line_to(cr, x2, y2);
    cairo_stroke(cr);
}

void draw_circle(cairo_t *cr, float x, float y, float radius) {
    cairo_arc(cr, x, y, radius, 0, 2 * 3.141);
    cairo_fill(cr);
}

void draw_intersection(cairo_t *cr, int width, int height, int i)
{
    cairo_set_source_rgb(cr, 0, 0, 0);

    int cross_length = 5;
    cairo_set_line_width(cr, 2);

    // draw a cross at the center of each intersection
    draw_line(cr, 
            intersections[i].origin.x - cross_length, intersections[i].origin.y - cross_length,
            intersections[i].origin.x + cross_length, intersections[i].origin.y + cross_length);

    draw_line(cr,
            intersections[i].origin.x - cross_length, intersections[i].origin.y + cross_length,
            intersections[i].origin.x + cross_length, intersections[i].origin.y - cross_length );

    // There is no need to draw the auto generated roads because their lanes
    // have already been drawn by other roads.
    if(intersections[i].isGenerated == FALSE) {
        draw_line(cr,
                width, intersections[i].origin.y - intersections[i].height / 2,
                0    , intersections[i].origin.y - intersections[i].height / 2);

        draw_line(cr,
                width, intersections[i].origin.y + intersections[i].height / 2,
                0    , intersections[i].origin.y + intersections[i].height / 2);
        draw_line(cr,
                intersections[i].origin.x - intersections[i].width / 2, height,
                intersections[i].origin.x - intersections[i].width / 2, 0);

        draw_line(cr,
                intersections[i].origin.x + intersections[i].width / 2, height,
                intersections[i].origin.x + intersections[i].width / 2, 0);
    }

}

void undraw_intersection_square(cairo_t *cr, int i)
{

    // set the color to the background color
    cairo_set_source_rgb(cr, 1,1,1);
    cairo_set_line_width(cr, 5);

    draw_line(cr,
            intersections[i].origin.x - intersections[i].width / 2, intersections[i].origin.y - intersections[i].height / 2,
            intersections[i].origin.x + intersections[i].width / 2, intersections[i].origin.y - intersections[i].height / 2);

    draw_line(cr,
            intersections[i].origin.x - intersections[i].width / 2, intersections[i].origin.y - intersections[i].height / 2,
            intersections[i].origin.x - intersections[i].width / 2, intersections[i].origin.y + intersections[i].height / 2);

    draw_line(cr,
            intersections[i].origin.x + intersections[i].width / 2, intersections[i].origin.y +  intersections[i].height / 2,
            intersections[i].origin.x + intersections[i].width / 2, intersections[i].origin.y - intersections[i].height / 2);

    draw_line(cr,
            intersections[i].origin.x + intersections[i].width / 2, intersections[i].origin.y + intersections[i].height / 2,
            intersections[i].origin.x - intersections[i].width / 2, intersections[i].origin.y + intersections[i].height / 2);
}

void draw(cairo_t *cr, int width, int height)
{

    // Draw border around canvas
    cairo_set_source_rgb(cr, 0, 0, 1);
    draw_line(cr, 0, 0, MAP_WIDTH, 0); 
    draw_line(cr, MAP_WIDTH, 0, MAP_WIDTH, MAP_HEIGHT); 
    draw_line(cr, MAP_WIDTH, MAP_HEIGHT, 0, MAP_HEIGHT); 
    draw_line(cr, 0, MAP_HEIGHT, 0, 0); 

    cairo_pattern_t *background_pattern;
    background_pattern = cairo_pattern_create_for_surface(background);
    cairo_set_source(cr, background_pattern);
    cairo_pattern_set_extend(cairo_get_source(cr), CAIRO_EXTEND_REPEAT);
    cairo_rectangle(cr, 0, 0, MAP_WIDTH, MAP_HEIGHT);
    cairo_fill(cr);

    for(int i = 0; i < num_arrows; i++) {
        draw_arrow(cr, &arrows[i]);
    }

    // Draw preview arrow
    if(arrow_being_created) {
        Arrow temporary_arrow;
        init_arrow(&temporary_arrow, arrow_origin.x, arrow_origin.y, arrow_end.x, arrow_end.y);

        // Round off corner if needed
        if(parent_arrow != NULL) {
            cairo_set_source_rgb(cr, 0.7,0.7,0.7);
            draw_circle(cr, temporary_arrow.start.x, temporary_arrow.start.y, intersection_width / 2);
        }

        draw_arrow(cr, &temporary_arrow);
    }

    int i = 0;

    // run through all intersections figuring out how many there are
    for (i = 0; i < number_of_intersections; i++){
        draw_intersection(cr, MAP_WIDTH, MAP_HEIGHT, i);
        if (simulation_is_running)
	        draw_traffic_light(cr, i);
    }
    for (i = 0; i < number_of_intersections; i++){
        undraw_intersection_square(cr, i);  
    }
    int j = 0;
    // run through all cars figuring out how many there are
    for (i = 0; i < NUM_OF_LANES; i++) {
	foreach_car(j, all_lanes[i].start_index, all_lanes[i].end_index) {
            draw_car(cr, i, j);
        }
    }

}

void draw_arrow(cairo_t *cr, Arrow *a) {
    cairo_set_source_rgb(cr, 0.7,0.7,0.7);
    cairo_set_line_width(cr, intersection_width);
    draw_line(cr, a->start.x, a->start.y, a->end.x, a->end.y);

    // Draw the middle lane
    cairo_set_source_rgb(cr, 1,1,0);
    cairo_set_line_width(cr, intersection_width / 10);
    double dashes[2] = {50, 50};
    cairo_set_dash(cr, dashes, 2, 0);
    draw_line(cr, a->start.x, a->start.y, a->end.x, a->end.y);
    cairo_set_dash(cr, NULL, 0, 0);

    // Draw a circle to round off bends
    if(&(a->endConnectors[0]) != NULL) {
        cairo_set_source_rgb(cr, 0.7,0.7,0.7);
        draw_circle(cr, a->end.x, a->end.y, intersection_width / 2);
    }
}

gboolean expose_event_callback(GtkWidget *widget, GdkEventExpose *event, gpointer data)
{
    cairo_t *cr;

    cr = gdk_cairo_create(widget->window);

    cairo_select_font_face(cr, "Sans",
            CAIRO_FONT_SLANT_NORMAL,
            CAIRO_FONT_WEIGHT_BOLD);

    cairo_set_font_size(cr, 13);

    cairo_translate(cr,
            widget->allocation.width/2 - view_center_x * view_scale,
            widget->allocation.height/2 - view_center_y * view_scale);
    cairo_scale(cr, view_scale, view_scale);

    cairo_get_matrix(cr, &view_matrix);

    draw(cr, widget->allocation.width, widget->allocation.height);

    cairo_destroy(cr);
    return TRUE;
}

void repaint() {
    gtk_widget_queue_draw(window);
}

void init_intersections(){
    intersections = (Intersection*) malloc(4 * sizeof(Intersection)); 
    max_number_of_intersections = 4;
    number_of_intersections = 0;
}

void init_gui() {
    GtkWidget *box;
    GtkWidget *drawing_area;
    GtkWidget *go_button;
    GdkColor color;

    // verify the gui components
    if (VERIFY_GUI){
        verify_intersection_is_valid_function();
    }

	
    arrows = (Arrow*) malloc(1000 * sizeof(Arrow));
    int argc = 0;
    char **argv = 0;
    gtk_init (&argc, &argv);
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

    // set up the bg to be white
    gdk_color_parse("white", &color);
    gtk_widget_modify_bg(window, GTK_STATE_NORMAL, &color);

    g_signal_connect(window, "destroy", G_CALLBACK(destroy), NULL);

    gtk_container_set_border_width(GTK_CONTAINER(window), 10);

    box = gtk_hbox_new(FALSE,0);

    gtk_container_add(GTK_CONTAINER(window), box);

    // add drawing_area
    drawing_area = gtk_drawing_area_new();
    gtk_widget_set_size_request(drawing_area, 700, 700);
    gtk_widget_modify_bg(drawing_area, GTK_STATE_NORMAL, &color);

    g_signal_connect(G_OBJECT(drawing_area), "expose_event",
            G_CALLBACK(expose_event_callback), NULL);

    // mouse signals
    gtk_widget_add_events(drawing_area, GDK_BUTTON_PRESS_MASK);
    g_signal_connect(G_OBJECT(drawing_area), "button_press_event",
            G_CALLBACK(mouse_pressed), NULL);
    gtk_widget_add_events(drawing_area, GDK_BUTTON_RELEASE_MASK);
    g_signal_connect(G_OBJECT(drawing_area), "button_release_event",
            G_CALLBACK(mouse_released), NULL);
    gtk_widget_add_events(drawing_area, GDK_POINTER_MOTION_MASK);
    g_signal_connect(G_OBJECT(drawing_area), "motion_notify_event",
            G_CALLBACK(mouse_moved), NULL);
    g_signal_connect(G_OBJECT(drawing_area), "scroll_event",
            G_CALLBACK(mouse_wheel_scrolled), NULL);

    gtk_box_pack_start(GTK_BOX(box), drawing_area, TRUE, TRUE, 0);

    // add update timer
    g_timeout_add(MILLISECONDS_PER_UPDATE, (GSourceFunc)timer_event, drawing_area);

    // add go button
    go_button = gtk_button_new();
    g_signal_connect(G_OBJECT(go_button), "clicked",
            G_CALLBACK(click_go), NULL);

    gtk_box_pack_start(GTK_BOX(box), go_button, TRUE, TRUE, 0);
    gtk_button_set_label(GTK_BUTTON(go_button), "Go");

    background = cairo_image_surface_create_from_png("grass.png");

    gtk_widget_show_all(window);

    init_intersections();
}

void update_gui() {
    gtk_main_iteration_do(TRUE);
}


