#ifndef VTL_GUI_H
#define VTL_GUI_H

#include <gtk/gtk.h>
#include "globals.h"

extern GtkWidget *window;

extern int simulation_is_running;

extern float view_scale;               // The zoom/scale of the view
extern float view_center_x;            // The center of the view
extern float view_center_y;
extern float old_view_center_x;
extern float old_view_center_y;
extern int moving_view;                // Whether the view is being moved
extern int moving_view_origin_x;       // The point at which the movement was initiated
extern int moving_view_origin_y;
extern cairo_matrix_t view_matrix;     // The matrix which represents the of the view

void destroy(GtkWidget *widget, gpointer data);

void timer_event(GtkWidget *widget);

void click_go(GtkWidget *widget, gpointer data);

void add_intersection(int x, int y, int isGenerated);

int is_valid_intersection(int x, int y);

// Convert a widget coordinate to a canvas coordinate
void transform_to_canvas(float *x, float *y);

gboolean mouse_pressed (GtkWidget* widget,
  GdkEventButton * event, GdkWindowEdge edge);

gboolean mouse_released (GtkWidget* widget,
        GdkEventButton * event, GdkWindowEdge edge);

gboolean mouse_moved (GtkWidget* widget,
        GdkEventMotion * event, GdkWindowEdge edge);

gboolean mouse_wheel_scrolled (GtkWidget* widget,
        GdkEventScroll * event, GdkWindowEdge edge);

void init_arrow(Arrow *arrow, float x1, float y1, float x2, float y2);

void draw_arrow(cairo_t *cr, Arrow *a);

void draw_car(cairo_t *cr, int i, int j);

void draw_line(cairo_t *cr, float x, float y, float x2, float y2);

void draw_circle(cairo_t *cr, float x, float y, float radius);

void draw_intersection(cairo_t *cr, int width, int height, int i);

void undraw_intersection_square(cairo_t *cr, int i);

void draw(cairo_t *cr, int width, int height);

gboolean expose_event_callback(GtkWidget *widget, GdkEventExpose *event, gpointer data);

void repaint();

void init_intersections();

void init_gui();

void update_gui();

#endif


