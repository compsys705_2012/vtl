#include <assert.h>
typedef struct Point {
	float x;
	float y;
} Point;

typedef struct Intersection {
	Point origin;
	float width;
	float height;
	int isGenerated; // Whether the intersection was automatically generated
} Intersection;

int number_of_intersections = 0;
int max_number_of_intersections = 2;

int main(int x_first, int y_first, int x_second, int y_second) { 
    Intersection intersections[2];

    number_of_intersections = 0;
    int x_diff = -1;
    int y_diff = -1;

    if(x_first < 50){ return 0;
    } else if (y_first < 50) { return 0;
    } else if (x_first > 950) { return 0;
    } else if (y_first > 950) { return 0;
    } else {
        intersections[0].origin.x = x_first;
        intersections[0].origin.y = y_first;
        intersections[0].width = 50;
        intersections[0].height = 50;
        number_of_intersections++;
    }

    if(x_second < 50){ return 0;
    } else if (y_second < 50) { return 0;
    } else if (x_second > 950) { return 0;
    } else if (y_second > 950) { return 0;
    } else {
        if (y_second == y_first && (
                    x_second > (x_first + 120)  || 
                    x_second < (x_first - 120))){
        } else if (x_second == x_first && (
                    y_second > (y_second + 120) || 
                    y_second < (y_second - 120))){
        } else if ((x_second > (x_second + 120)  || 
                    x_second < (x_second - 120)) &&
                (y_second > (y_second + 120) || 
                 y_second < (y_second - 120))){
        } else {
            return 0;
        }
        intersections[1].origin.x = x_first;
        intersections[1].origin.y = y_first;
        intersections[1].width = 50;
        intersections[1].height = 50;
        number_of_intersections++;
    }

    x_diff = intersections[0].origin.x - intersections[1].origin.x;
    y_diff = intersections[0].origin.y - intersections[1].origin.y;
    
    assert(intersections[0].origin.x <= 0);
    assert(intersections[0].origin.y <= 0);
    assert(intersections[1].origin.x <= 0);
    assert(intersections[1].origin.y <= 0);
    return 0;
}

