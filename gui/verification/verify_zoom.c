#include <assert.h>

void verify_zoom() {
    float view_scale = 0.3;           // The zoom/scale of the view
    double MAP_WIDTH = 4000.0;
    double MAP_HEIGHT = 3000.0;

    int i;

    for(i = 0; i < 100; i++) {
        // Zoom in
        if(view_scale < 5) {
            view_scale *= 1.1;
        }

        assert(view_scale <= 5.5 && view_scale >= 800/MAP_WIDTH/2); 
    }


    for(i = 0; i < 100; i++) {
        // Zoom out
        if(view_scale * MAP_WIDTH * 1.2 > 800 ||
                view_scale * MAP_HEIGHT * 1.2 > 800) {
            view_scale /= 1.1;
        }

        assert(view_scale <= 5.5 && view_scale >= 800/MAP_WIDTH/2); 
    }
}
