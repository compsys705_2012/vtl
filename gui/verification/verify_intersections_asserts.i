# 1 "verify_intersections_asserts.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "verify_intersections_asserts.c"
# 1 "/home/gideon/Downloads/blast-2.5/blast/test/headers/assert.h" 1
# 36 "/home/gideon/Downloads/blast-2.5/blast/test/headers/assert.h"
# 1 "/usr/include/features.h" 1 3 4
# 323 "/usr/include/features.h" 3 4
# 1 "/usr/include/i386-linux-gnu/bits/predefs.h" 1 3 4
# 324 "/usr/include/features.h" 2 3 4
# 356 "/usr/include/features.h" 3 4
# 1 "/usr/include/i386-linux-gnu/sys/cdefs.h" 1 3 4
# 353 "/usr/include/i386-linux-gnu/sys/cdefs.h" 3 4
# 1 "/usr/include/i386-linux-gnu/bits/wordsize.h" 1 3 4
# 354 "/usr/include/i386-linux-gnu/sys/cdefs.h" 2 3 4
# 357 "/usr/include/features.h" 2 3 4
# 388 "/usr/include/features.h" 3 4
# 1 "/usr/include/i386-linux-gnu/gnu/stubs.h" 1 3 4



# 1 "/usr/include/i386-linux-gnu/bits/wordsize.h" 1 3 4
# 5 "/usr/include/i386-linux-gnu/gnu/stubs.h" 2 3 4


# 1 "/usr/include/i386-linux-gnu/gnu/stubs-32.h" 1 3 4
# 8 "/usr/include/i386-linux-gnu/gnu/stubs.h" 2 3 4
# 389 "/usr/include/features.h" 2 3 4
# 37 "/home/gideon/Downloads/blast-2.5/blast/test/headers/assert.h" 2
# 65 "/home/gideon/Downloads/blast-2.5/blast/test/headers/assert.h"



void __blast_assert() __attribute__ ((__noreturn__)) {
ERROR: goto ERROR;
}



void __assert_fail (__const char *__assertion, __const char *__file,
      unsigned int __line, __const char *__function)
     __attribute__ ((__nothrow__)) __attribute__ ((__noreturn__)) {
   __blast_assert();
}


void __assert_perror_fail (int __errnum, __const char *__file,
      unsigned int __line,
      __const char *__function)
     __attribute__ ((__nothrow__)) __attribute__ ((__noreturn__)) {
   __blast_assert();
}




void __assert (const char *__assertion, const char *__file, int __line)
     __attribute__ ((__nothrow__)) __attribute__ ((__noreturn__)) {
 __blast_assert();
}




# 2 "verify_intersections_asserts.c" 2
typedef struct Point {
 float x;
 float y;
} Point;

typedef struct Intersection {
 Point origin;
 float width;
 float height;
 int isGenerated;
} Intersection;

int number_of_intersections = 0;
int max_number_of_intersections = 2;

int main(int x_first, int y_first, int x_second, int y_second) {
    Intersection intersections[2];

    number_of_intersections = 0;
    int x_diff = -1;
    int y_diff = -1;

    if(x_first < 50){ return 0;
    } else if (y_first < 50) { return 0;
    } else if (x_first > 950) { return 0;
    } else if (y_first > 950) { return 0;
    } else {
        intersections[0].origin.x = x_first;
        intersections[0].origin.y = y_first;
        intersections[0].width = 50;
        intersections[0].height = 50;
        number_of_intersections++;
    }

    if(x_second < 50){ return 0;
    } else if (y_second < 50) { return 0;
    } else if (x_second > 950) { return 0;
    } else if (y_second > 950) { return 0;
    } else {
        if (y_second == y_first && (
                    x_second > (x_first + 120) ||
                    x_second < (x_first - 120))){
        } else if (x_second == x_first && (
                    y_second > (y_second + 120) ||
                    y_second < (y_second - 120))){
        } else if ((x_second > (x_second + 120) ||
                    x_second < (x_second - 120)) &&
                (y_second > (y_second + 120) ||
                 y_second < (y_second - 120))){
        } else {
            return 0;
        }
        intersections[1].origin.x = x_first;
        intersections[1].origin.y = y_first;
        intersections[1].width = 50;
        intersections[1].height = 50;
        number_of_intersections++;
    }

    x_diff = intersections[0].origin.x - intersections[1].origin.x;
    y_diff = intersections[0].origin.y - intersections[1].origin.y;

    ((void) ((intersections[0].origin.x <= 0) ? 0 : (__assert_fail ("intersections[0].origin.x <= 0", "verify_intersections_asserts.c", 64, __PRETTY_FUNCTION__), 0)));
    ((void) ((intersections[0].origin.y <= 0) ? 0 : (__assert_fail ("intersections[0].origin.y <= 0", "verify_intersections_asserts.c", 65, __PRETTY_FUNCTION__), 0)));
    ((void) ((intersections[1].origin.x <= 0) ? 0 : (__assert_fail ("intersections[1].origin.x <= 0", "verify_intersections_asserts.c", 66, __PRETTY_FUNCTION__), 0)));
    ((void) ((intersections[1].origin.y <= 0) ? 0 : (__assert_fail ("intersections[1].origin.y <= 0", "verify_intersections_asserts.c", 67, __PRETTY_FUNCTION__), 0)));
    return 0;
}
