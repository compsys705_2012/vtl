global int lockStatus = 0;

event {
  pattern { init_mutex(); }
  action { lockStatus = 0; }
}

event {
  pattern { change_mutex_state($?, held); }
  guard { lockStatus == 0 }
  action { lockStatus = 1; }
}

event {
  pattern { change_mutex_state($?, idle); }
  guard { lockStatus == 1 }
  action { lockStatus = 0; }
}
