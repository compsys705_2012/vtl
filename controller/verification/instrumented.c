#line 1 "spec.work"
int __BLAST_error  ;

#line 2 "spec.work"
void __error__(void) 
{ 

  {
#line 4
  __BLAST_error = 0;
  ERROR: 
  goto ERROR;
}
}

#line 7 "spec.work"
void __BLAST___error__(void) 
{ 

  {
#line 9
  __BLAST_error = 0;
  BERROR: 
  goto BERROR;
}
}

#line 12 "spec.work"
int lockStatus   = 0;

void __initialize__(void) ;

#line 14 "controller.h"
enum __anonenum_Direction_1 {
    NS = 0,
    SN = 1,
    EW = 2,
    WE = 3,
    NUM_DIRS = 4
};

#line 14 "controller.h"
typedef enum __anonenum_Direction_1 Direction;

#line 22
enum __anonenum_bool_2 {
    false = 0,
    true = 1
};

#line 22 "controller.h"
typedef enum __anonenum_bool_2 bool;

#line 27
enum __anonenum_mutex_state_3 {
    idle = 0,
    waiting = 1,
    held = 2,
    exiting = 3,
    dont_change = 4
};

#line 27 "controller.h"
typedef enum __anonenum_mutex_state_3 mutex_state;

#line 35
enum __anonenum_TrafLightState_4 {
    RED = 0,
    ORANGE = 1,
    GREEN = 2
};

#line 35 "controller.h"
typedef enum __anonenum_TrafLightState_4 TrafLightState;

#line 41 "controller.h"
struct __anonstruct_carProcess_5 {
   mutex_state state ;
   int priority ;
   int lamport ;
   int req ;
   int ack ;
   int timeout ;
};

#line 41 "controller.h"
typedef struct __anonstruct_carProcess_5 carProcess;

#line 52 "controller.h"
struct __anonstruct_mutex_6 {
   bool lane[4] ;
   carProcess leadCar[4] ;
   TrafLightState light[4] ;
   bool empty ;
};

#line 52 "controller.h"
typedef struct __anonstruct_mutex_6 mutex;

#line 4
void init_controller(void) ;

#line 6
void update_controller(void) ;

#line 65
void set_color(Direction direction , TrafLightState color ) ;

#line 69
void init_mutex(void) ;

#line 71
void update_mutex(void) ;

#line 1 "./fake_assert/assert.h"
void assert(int val ) 
{ 

  {
#line 1 "./fake_assert/assert.h"
  return;
}
}

#line 5 "controller.c"
mutex intersection  ;

#line 7 "controller.c"
int main(void) 
{ int i ;

  {
  {
#line 8
  init_controller();
#line 10
  i = 0;
  }
#line 10
  while (i < 4) {
    {
#line 10
    i ++;
    }
  }
#line 14
  while (1) {
    {
#line 15
    update_controller();
    }
  }
#line 18
  return (0);
}
}

#line 21 "controller.c"
int test  ;

#line 25 "controller.c"
void init_controller(void) 
{ 

  {
  {

  {
#line 17 "spec.work"
  lockStatus = 0;
  {

  }
  }
#line 26 "controller.c"
  init_mutex();
  }
#line 27
  return;
}
}

#line 66
extern int ( /* missing proto */  intersection_is_empty)() ;

#line 71
mutex_state change_mutex_state(int i , int newState ) ;

#line 29 "controller.c"
void update_controller(void) 
{ int __BLAST_NONDET ;
  int i ;
  bool cars_present ;
  bool no_controller_exists ;
  int guard_counter ;
  int tmp ;
  int tmp___0 ;
  int i___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;

  {
  {
#line 31
  i = 0;
  }
#line 31
  while (i < 4) {
#line 34
    if (__BLAST_NONDET) {
      {
#line 35
      intersection.lane[i] = 1;
      }
    }
#line 39
    if (__BLAST_NONDET) {
      {
#line 40
      intersection.empty = 1;
      }
    }
#line 43
    if ((int )intersection.light[i] == 2) {
#line 45
      if (__BLAST_NONDET) {
        {
#line 46
        intersection.lane[i] = 1;
        }
      }
#line 50
      if (__BLAST_NONDET) {
        {
#line 51
        intersection.empty = 0;
        }
      }
    }
    {
#line 56
    set_color((enum __anonenum_Direction_1 )i, 2);
#line 31
    i ++;
    }
  }
  {
#line 61
  guard_counter = 0;
  }
#line 63
  while (1) {
    {
#line 64
    update_mutex();
#line 66
    tmp = intersection_is_empty();
    }
#line 66
    if (tmp) {
      {
#line 66
      tmp___0 = 0;
      }
    } else {
      {
#line 66
      tmp___0 = 1;
      }
    }
    {
#line 66
    cars_present = (enum __anonenum_bool_2 )tmp___0;
#line 67
    no_controller_exists = 1;
#line 69
    i___0 = 0;
    }
#line 69
    while (i___0 < 4) {
      {
#line 70
      cars_present = (enum __anonenum_bool_2 )((int )cars_present |
                                               (int )intersection.lane[i___0]);
#line 71
      tmp___1 = (int )change_mutex_state(i___0, 4);
      }
#line 71
      if (tmp___1 == 2) {
        {
#line 71
        tmp___3 = 0;
        }
      } else {
        {
#line 71
        tmp___2 = (int )change_mutex_state(i___0, 4);
        }
#line 71
        if (tmp___2 == 2) {
          {
#line 71
          tmp___3 = 0;
          }
        } else {
          {
#line 71
          tmp___3 = 1;
          }
        }
      }
      {
#line 71
      no_controller_exists = (enum __anonenum_bool_2 )((int )no_controller_exists &
                                                       tmp___3);
#line 69
      i___0 ++;
      }
    }
    {
#line 75
    assert(guard_counter < 10);
#line 76
    guard_counter ++;
    }
#line 63
    if (cars_present) {
#line 63
      if (! no_controller_exists) {
#line 63
        break;
      }
    } else {
#line 63
      break;
    }
  }
#line 78
  return;
}
}

#line 80 "controller.c"
void set_color(Direction direction , TrafLightState color ) 
{ 

  {
#line 85
  switch ((int )color) {
  case 0: 
  {
#line 87
  intersection.light[direction] = 0;
  }
#line 88
  break;
  case 1: 
  {
#line 91
  intersection.light[direction] = 1;
  }
#line 92
  break;
  case 2: 
  {
#line 95
  intersection.light[direction] = 2;
  }
#line 96
  break;
  default: 
  {

  }
  COLOR_ERROR: 
  goto COLOR_ERROR;
#line 100
  break;
  }
}
}

#line 105 "controller.c"
static int state0  ;

#line 105 "controller.c"
static int state1  ;

#line 105 "controller.c"
static int state2  ;

#line 105 "controller.c"
static int state3  ;

#line 104 "controller.c"
mutex_state change_mutex_state(int i , int newState ) 
{ 

  {
#line 107
  switch (i) {
  case 0: 
  {

  }
#line 109
  if (newState != 4) {
    {
#line 110
    state0 = newState;
    }
  }
#line 113
  return ((enum __anonenum_mutex_state_3 )state0);
  case 1: 
  {

  }
#line 115
  if (newState != 4) {
    {
#line 116
    state1 = newState;
    }
  }
#line 119
  return ((enum __anonenum_mutex_state_3 )state1);
  case 2: 
  {

  }
#line 121
  if (newState != 4) {
    {
#line 122
    state2 = newState;
    }
  }
#line 125
  return ((enum __anonenum_mutex_state_3 )state2);
  case 3: 
  {

  }
#line 127
  if (newState != 4) {
    {
#line 128
    state3 = newState;
    }
  }
#line 131
  return ((enum __anonenum_mutex_state_3 )state3);
  }
#line 133
  return ((enum __anonenum_mutex_state_3 )0);
}
}

#line 135 "controller.c"
Direction samedir(Direction direction ) 
{ 

  {
#line 136
  switch ((int )direction) {
  case 0: 
  {

  }
#line 138
  return (1);
  case 1: 
  {

  }
#line 140
  return (0);
  case 2: 
  {

  }
#line 142
  return (3);
  case 3: 
  {

  }
#line 144
  return (2);
  default: 
  {

  }
  DIR_ERROR: 
  goto DIR_ERROR;
#line 147
  return (direction);
  }
}
}

#line 151 "controller.c"
void set_opposite_dirs(Direction direction , TrafLightState color ) 
{ 

  {
#line 152
  switch ((int )direction) {
  case 0: 
  {

  }
  case 1: 
  {
#line 155
  set_color(2, color);
#line 156
  set_color(3, color);
  }
#line 157
  break;
  case 2: 
  {

  }
  case 3: 
  {
#line 160
  set_color(0, color);
#line 161
  set_color(1, color);
  }
#line 162
  break;
  default: 
  {

  }
#line 164
  break;
  }
}
}

#line 168 "controller.c"
void init_mutex(void) 
{ int i ;
  mutex_state tmp ;

  {
  {
#line 169
  intersection.empty = 1;
#line 170
  i = 0;
#line 171
  i = 0;
  }
#line 171
  while (i < 4) {
    {
#line 172
    intersection.lane[i] = 0;
#line 173
    intersection.light[i] = 2;
#line 175
    tmp = change_mutex_state(i, 0);
#line 178
    intersection.leadCar[i].lamport = 0;
#line 179
    intersection.leadCar[i].req = -1;
#line 180
    intersection.leadCar[i].ack = -1;
#line 171
    i ++;
    }
  }
#line 182
  return;
}
}

#line 184 "controller.c"
void update_mutex(void) 
{ int i ;
  int tmp ;
  int lamport ;
  bool canAccess ;
  int j ;
  int j___0 ;
  Direction tmp___0 ;
  int j___1 ;

  {
  {
#line 185
  i = 0;
#line 186
  i = 0;
  }
#line 186
  while (i < 4) {
    {
#line 188
    tmp = (int )change_mutex_state(i, 4);
    }
#line 188
    switch (tmp) {
    case 0: 
    {
#line 194
    lamport = 0;
#line 195
    j = 0;
    }
#line 195
    while (j < 4) {
#line 196
      if (lamport > intersection.leadCar[j].lamport) {
        {
#line 196
        lamport = lamport;
        }
      } else {
        {
#line 196
        lamport = intersection.leadCar[j].lamport;
        }
      }
      {
#line 195
      j ++;
      }
    }
    {
#line 198
    intersection.leadCar[i].lamport = lamport + 1;
#line 199
    intersection.leadCar[i].req = intersection.leadCar[i].lamport;
#line 200
    intersection.leadCar[i].ack = intersection.leadCar[i].lamport;
    }
#line 202
    if (intersection.lane[i]) {
      {

      {
#line 24
      if (lockStatus == 0) {
#line 25
        lockStatus = 1;
      } else {
#line 26 "spec.work"
        __error__();
      }
      {

      }
      }

      {
#line 32
      if (lockStatus == 1) {
#line 33
        lockStatus = 0;
      } else {
#line 34
        __error__();
      }
      {

      }
      }
#line 203 "controller.c"
      change_mutex_state(i, 1);
      }
    }
#line 205
    break;
    case 1: 
    {

    }
#line 207
    if (! intersection.lane[i]) {
      {

      {
#line 24
      if (lockStatus == 0) {
#line 25
        lockStatus = 1;
      } else {
#line 26 "spec.work"
        __error__();
      }
      {

      }
      }

      {
#line 32
      if (lockStatus == 1) {
#line 33
        lockStatus = 0;
      } else {
#line 34
        __error__();
      }
      {

      }
      }
#line 208 "controller.c"
      change_mutex_state(i, 0);
      }
#line 209
      break;
    }
    {
#line 211
    canAccess = 1;
#line 212
    j___0 = 0;
    }
#line 212
    while (j___0 < 4) {
#line 213
      if (intersection.leadCar[j___0].ack < intersection.leadCar[i].req) {
        {
#line 214
        canAccess = 0;
        }
      } else {
#line 213
        if (intersection.leadCar[j___0].ack <= intersection.leadCar[i].req) {
#line 213
          if (intersection.leadCar[j___0].priority <
              intersection.leadCar[i].priority) {
            {
#line 214
            canAccess = 0;
            }
          }
        }
      }
      {
#line 212
      j___0 ++;
      }
    }
#line 217
    if (canAccess) {
      {

      {
#line 24
      if (lockStatus == 0) {
#line 25
        lockStatus = 1;
      } else {
#line 26 "spec.work"
        __error__();
      }
      {

      }
      }

      {
#line 32
      if (lockStatus == 1) {
#line 33
        lockStatus = 0;
      } else {
#line 34
        __error__();
      }
      {

      }
      }
#line 219 "controller.c"
      change_mutex_state(i, 2);
#line 220
      intersection.leadCar[i].timeout = 0;
      }
    }
#line 222
    break;
    case 2: 
    {
#line 224
    set_opposite_dirs((enum __anonenum_Direction_1 )i, 0);
#line 225
    set_color((enum __anonenum_Direction_1 )i, 2);
#line 226
    tmp___0 = samedir((enum __anonenum_Direction_1 )i);
#line 226
    set_color(tmp___0, 2);
#line 227
    intersection.leadCar[i].timeout ++;
    }
#line 228
    if (! intersection.lane[i]) {
      {

      {
#line 24
      if (lockStatus == 0) {
#line 25
        lockStatus = 1;
      } else {
#line 26 "spec.work"
        __error__();
      }
      {

      }
      }

      {
#line 32
      if (lockStatus == 1) {
#line 33
        lockStatus = 0;
      } else {
#line 34
        __error__();
      }
      {

      }
      }
#line 230 "controller.c"
      change_mutex_state(i, 3);
      }
    } else {
#line 228
      if (intersection.leadCar[i].timeout > 5) {
        {

        {
#line 24
        if (lockStatus == 0) {
#line 25
          lockStatus = 1;
        } else {
#line 26 "spec.work"
          __error__();
        }
        {

        }
        }

        {
#line 32
        if (lockStatus == 1) {
#line 33
          lockStatus = 0;
        } else {
#line 34
          __error__();
        }
        {

        }
        }
#line 230 "controller.c"
        change_mutex_state(i, 3);
        }
      }
    }
#line 232
    break;
    case 3: 
    {
#line 234
    j___1 = 0;
    }
#line 234
    while (j___1 < 4) {
      {
#line 236
      set_color((enum __anonenum_Direction_1 )j___1, 0);
#line 234
      j___1 ++;
      }
    }
#line 239
    if (intersection.empty) {
      {

      {
#line 24
      if (lockStatus == 0) {
#line 25
        lockStatus = 1;
      } else {
#line 26 "spec.work"
        __error__();
      }
      {

      }
      }

      {
#line 32
      if (lockStatus == 1) {
#line 33
        lockStatus = 0;
      } else {
#line 34
        __error__();
      }
      {

      }
      }
#line 241 "controller.c"
      change_mutex_state(i, 0);
      }
    }
#line 243
    break;
    default: 
    {

    }
    STATE_ERROR: 
    goto STATE_ERROR;
#line 246
    break;
    }
    {
#line 186
    i ++;
    }
  }
#line 249
  return;
}
}

void __initialize__(void) 
{ 

  {

}
}

