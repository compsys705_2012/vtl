#include "controller.h"
#include "assert.h"

//global seems not to be global in BLAST
mutex intersection;

int main() {
	init_controller();
	
	for (int i = 0; i < NUM_DIRS; i++) {
        //assert (change_mutex_state(i, dont_change) == idle);
    }
	
	for (;;) {
		update_controller();
	}
	
	return 0;
}

int test;

//int start=0;
//int end=0;
void init_controller() {
  	init_mutex();
}

void update_controller() {
	int __BLAST_NONDET;
	for (int i = 0; i < NUM_DIRS; i++) {
	
		//a car might enter the lane (non-deterministic)
		if ( __BLAST_NONDET ) {
			intersection.lane[i] = true;
		}
		
		//a car might leave the intersection area
		if ( __BLAST_NONDET ) {
			intersection.empty = true;
		}
		
		if (intersection.light[i] == GREEN) {
			//if the lane is green, a car might leave the lane
			if ( __BLAST_NONDET ) {
				intersection.lane[i] = true;
			}
			
			//a car from the green lane might enter the intersection
			if ( __BLAST_NONDET ) {
				intersection.empty = false;
			}
		}

		//these must be reset if there exists a controller
		set_color(i, GREEN);
	}
	
	bool cars_present;
	bool no_controller_exists;
	int guard_counter = 0;
	
	do {
		update_mutex();
		
		cars_present = !intersection.empty;
		no_controller_exists = true;
		
		for (int i = 0; i < NUM_DIRS; i++) {
			cars_present |= intersection.lane[i];
			no_controller_exists &= !( (change_mutex_state(i, dont_change) == held) ||
										(change_mutex_state(i, dont_change) == held) );
		}
		
		assert(guard_counter < 10);
		guard_counter++;
	} while (cars_present && no_controller_exists);
}

void set_color(Direction direction, TrafLightState color) {

	//char *directionNames[] = {"SN", "NS", "EW", "WE"};
	//char *colorNames[] = {"RED", "ORANGE", "GREEN"};
	
	switch (color) {
	case RED:
		intersection.light[direction] = RED;
		break;
	
	case ORANGE:
		intersection.light[direction] = ORANGE;
		break;
		
	case GREEN:
		intersection.light[direction] = GREEN;
		break;
		
	default:
		COLOR_ERROR: goto COLOR_ERROR;
		break;
	}
}

mutex_state change_mutex_state(int i, mutex_state newState) {
	static int state0, state1, state2, state3;
	
	switch (i) {
	case 0:
		if (newState != dont_change) {
			state0 = newState;
		}
	
		return state0;
	case 1:
		if (newState != dont_change) {
			state1 = newState;
		}
	
		return state1;
	case 2:
		if (newState != dont_change) {
			state2 = newState;
		}
	
		return state2;
	case 3:
		if (newState != dont_change) {
			state3 = newState;
		}
	
		return state3;
	}
}

Direction samedir(Direction direction) {
	switch (direction) {
	case NS:
		return SN;
	case SN:
		return NS;
	case EW:
		return WE;
	case WE:
		return EW;
	default:
		DIR_ERROR: goto DIR_ERROR;
		return direction;
	}
}

void set_opposite_dirs(Direction direction, TrafLightState color) {
	switch (direction) {
	case NS:
	case SN:
		set_color(EW, color);
		set_color(WE, color);
		break;
	case EW:
	case WE:
		set_color(NS, color);
		set_color(SN, color);
		break;
	default:
		break;
	}
}

void init_mutex() {
	intersection.empty = true;
	int i = 0;
    for (i = 0; i < NUM_DIRS; i++) {
    	intersection.lane[i] = false;
    	intersection.light[i] = GREEN;
    	
        change_mutex_state(i, idle) != idle;

        //will need to ensure that this can't roll-over, else the program might not verify.
        intersection.leadCar[i].lamport = 0;
        intersection.leadCar[i].req = -1;
        intersection.leadCar[i].ack = -1;
    }
}

void update_mutex () {
	int i = 0;
    for (i = 0; i < NUM_DIRS; i++) {

        switch ( change_mutex_state(i, dont_change) ) {
        int lamport;
        bool canAccess;
        
        case idle:
			//accept all requests
			lamport = 0;
			for (int j = 0; j < NUM_DIRS; j++) {
				lamport = max(lamport, intersection.leadCar[j].lamport);
			}
			intersection.leadCar[i].lamport = lamport + 1;
			intersection.leadCar[i].req = intersection.leadCar[i].lamport;
			intersection.leadCar[i].ack = intersection.leadCar[i].lamport;
			
			if ( intersection.lane[i] ) {
            	change_mutex_state(i, waiting);
		    }
		    break;
        case waiting:
		    if ( !intersection.lane[i] ) {
	        	change_mutex_state(i, idle);
	        	break;
			}
            canAccess = true;
            for (int j = 0; j < NUM_DIRS; j++) {
                if ( (intersection.leadCar[j].ack < intersection.leadCar[i].req) ||  ((intersection.leadCar[j].ack <= intersection.leadCar[i].req) && (intersection.leadCar[j].priority < intersection.leadCar[i].priority) ) ) {
                    canAccess = false;
                }
            }
            if ( canAccess ) {
				//printf("Lane %d is new holder.\n", i);
                change_mutex_state(i, held);
                intersection.leadCar[i].timeout = 0;
            }
            break;
        case held:
	    	set_opposite_dirs(i, RED);
	    	set_color(i, GREEN);
	    	set_color(samedir(i), GREEN);
        	intersection.leadCar[i].timeout++;
            if ((!intersection.lane[i]) || (intersection.leadCar[i].timeout > TIMEOUT)) {
            	//if (intersection.leadCar[i].timeout > TIMEOUT) printf("Timed out\n");
            	change_mutex_state(i, exiting);
            }
            break;
        case exiting:
    		for (int j = 0; j < NUM_DIRS; j++) {
				//printf("all empty\n");
				set_color(j, RED);
			}
			
        	if (intersection.empty) {
        		//printf("\b%d", intersection_is_empty(&intersection[0]));
        		change_mutex_state(i, idle);
        	}
        	break;
        default:
        	STATE_ERROR: goto STATE_ERROR;
            break;
        }
    }
}
