#ifndef VTL_CONTROLLER_H
#define VTL_CONTROLLER_H

void init_controller();

void update_controller();

#define max(i, j) ( (i>j)?i:j )
#define TIMEOUT 5
#define RADIUS 300

#define BOUNDS ((intersection_width + CAR_LENGTH)/2)

typedef enum {
	NS,
	SN,
	EW,
	WE,
	NUM_DIRS,
} Direction;

typedef enum {
	false,
	true,
} bool;

typedef enum {
    idle,
    waiting,
    held,
    exiting,
    dont_change
} mutex_state;

typedef enum {
	RED = 0,
	ORANGE = 1,
	GREEN = 2,
}TrafLightState;

typedef struct {
    mutex_state state;
    int priority;
    int lamport;
    int req;
    int ack;
    int timeout;
} carProcess;

//each intersection will be considered a mutex, controlled by the distributed mutex algorithm
//the carProcess represents the controller running in the lead car of a lane.
typedef struct {
    bool lane[NUM_DIRS]; //true if there is a car in the lane. (lane length = radius)
    carProcess leadCar[NUM_DIRS];
    TrafLightState light[NUM_DIRS];
    bool empty;
} mutex;

void createVTLController(int lane);

void updateVTLController(int lane);

void endVTLController(int lane);

void set_color(Direction direction, TrafLightState color);

TrafLightState get_color(Direction direction);

void init_mutex();

void update_mutex ();

mutex_state change_mutex_state(int i, mutex_state newState);

#endif
