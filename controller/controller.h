#ifndef VTL_CONTROLLER_H
#define VTL_CONTROLLER_H

#include "globals.h"

void init_controller();

void update_controller();

#define max(i, j) ( (i>j)?i:j )
#define NUM_DIRS 4
#define TIMEOUT 450
#define RADIUS 300

#define BOUNDS ((intersection_width + CAR_LENGTH)/2)

typedef enum {
    idle,
    waiting,
    held,
    exiting
} mutex_state;

typedef struct {
    mutex_state state;
    int priority;
    int lamport;
    int req;
    int ack;
    int timeout;
} carProcess;

//each intersection will be considered a mutex, controlled by the distributed mutex algorithm
//the carProcess represents the controller running in the lead car of a lane.
typedef struct {
    Lane *lane[NUM_DIRS*2]; //array of pointers
    carProcess leadCar[NUM_DIRS];
    Point centre;
} mutex;

void createVTLController(int lane, int loc);

void updateVTLController(int lane, int loc);

void endVTLController(int lane, int loc);

void set_color(int loc, Direction direction, TrafLightState color);

void change_mutex_state(int lane, int loc, mutex_state newState);

TrafLightState get_color(int loc, Direction direction);

extern mutex intersection[4];

void init_mutex();

void update_mutex ();

bool is_empty_lane(Lane* lane);

bool intersection_is_empty(mutex *Intersection);

#endif
