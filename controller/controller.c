#include "globals.h"

#define NUM_INTERSECTIONS 4

//Definition of externed global;
mutex intersection[NUM_INTERSECTIONS];
//int start=0;
//int end=0;
void init_controller() {
	for (int loc = 0; loc < NUM_INTERSECTIONS; loc++) {
  		init_mutex(loc);
  	}
}

void update_controller() {
	for (int loc = 0; loc < NUM_INTERSECTIONS; loc++) {
	
		for (int i = 0; i < NUM_DIRS; i++) {
			//printf("%d all empty\n", loc);
			set_color(loc, i, GREEN);
		}
		
		update_mutex(loc);
		update_mutex(loc);
	}
}

bool intersection_is_empty(mutex* Intersection) {
	for (int i = 0; i < 4; i++) {
		int j;
		Point boxLoc = Intersection->centre;
		
		if (Intersection->lane[i]->count > 0)
		foreach_car (j, Intersection->lane[i]->start_index, Intersection->lane[i]->end_index) {
			Point carLoc = Intersection->lane[i]->cars[j].location;
			if ((!Intersection->lane[i]->cars[j].invisible)
					&& ((fabs(boxLoc.x-carLoc.x) < BOUNDS) && (fabs(boxLoc.y-carLoc.y) < BOUNDS))) {
				return false;
			}
		}
		
		if (Intersection->lane[i+NUM_DIRS]->count > 0)
		foreach_car (j, Intersection->lane[i+NUM_DIRS]->start_index, Intersection->lane[i+NUM_DIRS]->end_index) {
			Point carLoc = Intersection->lane[i+NUM_DIRS]->cars[j].location;
			if ((!Intersection->lane[i+NUM_DIRS]->cars[j].invisible)
					&& (fabs(boxLoc.x-carLoc.x) < BOUNDS) && (fabs(boxLoc.y-carLoc.y) < BOUNDS)) {
				return false;
			}
		}
	}
	
	return true;
}

void set_color(int loc, Direction direction, TrafLightState color) {

	//char *directionNames[] = {"SN", "NS", "EW", "WE"};
	//char *colorNames[] = {"RED", "ORANGE", "GREEN"};
	
	Lane *lane = intersection[loc].lane[direction];
	
	switch (color) {
	
	case RED:
		create_invisible_car(lane);
		
		intersectionLights[loc].light[direction].state = RED;
			//printf("Changing intersection %d - %s lanes %s\n", loc, directionNames[direction], colorNames[color]);
		break;
	
	case ORANGE:
		intersectionLights[loc].light[direction].state = ORANGE;
		break;
		
	case GREEN:

		remove_invisible_car(lane);
		
		intersectionLights[loc].light[direction].state = GREEN;
		break;
	default:
		break;
	}
}

void change_mutex_state(int lane, int loc, mutex_state newState) {
#if DEBUG > 2
	static char *stateNames[] = { "idle", "waiting", "held", "exiting" };
	
	//unchanged, dont printf:
	if (intersection[loc].leadCar[lane].state == newState) return;
	
	//%4 ensures the int will always index the stateNames array
	if ((loc == 0) && (intersection[loc].leadCar[lane].state == held)) {
		printf("intersection %d, lane %d: %s -> %s\n", loc, lane, stateNames[intersection[loc].leadCar[lane].state%4], stateNames[newState%4]);
	}
#endif

    intersection[loc].leadCar[lane].state = newState;
}

bool is_empty_lane(Lane* lane) {
	if (lane->count == 0) {
		return true;
    } else if ( (lane->count == 1) && lane->cars[lane->start_index].invisible) {
    	return true;
    } else if (!((fabs(lane->cars[lane->start_index].location.x-lane->end_pos.x)<RADIUS)&&(fabs(lane->cars[lane->start_index].location.y-lane->end_pos.y)<RADIUS))) {
    	return true;
    }
	return false;
}

Direction samedir(Direction direction) {
	switch (direction) {
	case NS:
		return SN;
	case SN:
		return NS;
	case EW:
		return WE;
	case WE:
		return EW;
	default:
		return direction;
	}
}

void set_opposite_dirs(int loc, Direction direction, TrafLightState color) {
	switch (direction) {
	case NS:
	case SN:
		set_color(loc, EW, color);
		set_color(loc, WE, color);
		break;
	case EW:
	case WE:
		set_color(loc, NS, color);
		set_color(loc, SN, color);
		break;
	}
}

void init_mutex(int loc) {
	
	intersection[loc].centre = intersections[loc].origin;
	
	if (&intersections[loc].origin == topLeft) {
		intersection[loc].lane[EW] = all_lanes;
		intersection[loc].lane[WE] = all_lanes + 2;
		intersection[loc].lane[SN] = all_lanes + 4;
		intersection[loc].lane[NS] = all_lanes + 6;
		intersection[loc].lane[EW+NUM_DIRS] = all_lanes + 1;
		intersection[loc].lane[WE+NUM_DIRS] = all_lanes + 3;
		intersection[loc].lane[SN+NUM_DIRS] = all_lanes + 5;
		intersection[loc].lane[NS+NUM_DIRS] = all_lanes + 7;
	} else if (&intersections[loc].origin == topRight) {
		intersection[loc].lane[EW] = all_lanes + 1;
		intersection[loc].lane[WE] = all_lanes + 23;
		intersection[loc].lane[SN] = all_lanes + 15;
		intersection[loc].lane[NS] = all_lanes + 17;
		intersection[loc].lane[EW+NUM_DIRS] = all_lanes + 22;
		intersection[loc].lane[WE+NUM_DIRS] = all_lanes + 2;
		intersection[loc].lane[SN+NUM_DIRS] = all_lanes + 16;
		intersection[loc].lane[NS+NUM_DIRS] = all_lanes + 18;
	} else if (&intersections[loc].origin == botLeft) {
		intersection[loc].lane[EW] = all_lanes + 8;
		intersection[loc].lane[WE] = all_lanes + 12;
		intersection[loc].lane[SN] = all_lanes + 20;
		intersection[loc].lane[NS] = all_lanes + 7;
		intersection[loc].lane[EW+NUM_DIRS] = all_lanes + 9;
		intersection[loc].lane[WE+NUM_DIRS] = all_lanes + 13;
		intersection[loc].lane[SN+NUM_DIRS] = all_lanes + 4;
		intersection[loc].lane[NS+NUM_DIRS] = all_lanes + 21;
	} else {
		intersection[loc].lane[EW] = all_lanes + 9;
		intersection[loc].lane[WE] = all_lanes + 11;
		intersection[loc].lane[SN] = all_lanes + 14;
		intersection[loc].lane[NS] = all_lanes + 18;
		intersection[loc].lane[EW+NUM_DIRS] = all_lanes + 10;
		intersection[loc].lane[WE+NUM_DIRS] = all_lanes + 12;
		intersection[loc].lane[SN+NUM_DIRS] = all_lanes + 15;
		intersection[loc].lane[NS+NUM_DIRS] = all_lanes + 19;
	}
		
	
    for (int i = 0; i < NUM_DIRS; i++) {
    	
        change_mutex_state(i, loc, idle);

        //will need to ensure that this can't roll-over, else the program might not verify.
        intersection[loc].leadCar[i].lamport = 0;
        intersection[loc].leadCar[i].req = -1;
        intersection[loc].leadCar[i].ack = -1;
    }
}

void update_mutex (int loc) {
    for (int i = 0; i < NUM_DIRS; i++) {

        switch ( intersection[loc].leadCar[i].state ) {
        int lamport;
        bool canAccess;
        
        case idle:
			//accept all requests
			lamport = 0;
			for (int j = 0; j < NUM_DIRS; j++) {
				lamport = max(lamport, intersection[loc].leadCar[j].lamport);
			}
			intersection[loc].leadCar[i].lamport = lamport + 1;
			intersection[loc].leadCar[i].req = intersection[loc].leadCar[i].lamport;
			intersection[loc].leadCar[i].ack = intersection[loc].leadCar[i].lamport;
			
			if ( !is_empty_lane(intersection[loc].lane[i]) ) {
            	change_mutex_state(i, loc, waiting);
		    }
		    break;
        case waiting:
		    if ( is_empty_lane(intersection[loc].lane[i]) ) {
	        	change_mutex_state(i, loc, idle);
	        	break;
			}
            canAccess = true;
            for (int j = 0; j < NUM_DIRS; j++) {
                if ( (intersection[loc].leadCar[j].ack < intersection[loc].leadCar[i].req) ||  ((intersection[loc].leadCar[j].ack <= intersection[loc].leadCar[i].req) && (intersection[loc].leadCar[j].priority < intersection[loc].leadCar[i].priority) ) ) {
                    canAccess = false;
                }
            }
            if ( canAccess ) {
				//printf("Lane %d is new holder.\n", i);
                change_mutex_state(i, loc, held);
                intersection[loc].leadCar[i].timeout = 0;
            }
            break;
        case held:
	    	set_opposite_dirs(loc, intersection[loc].lane[i]->direction, RED);
	    	set_color(loc, intersection[loc].lane[i]->direction, GREEN);
	    	set_color(loc, samedir(intersection[loc].lane[i]->direction), GREEN);
        	intersection[loc].leadCar[i].timeout++;
            if ((is_empty_lane(intersection[loc].lane[i])) || (intersection[loc].leadCar[i].timeout > TIMEOUT)) {
            	if (intersection[loc].leadCar[i].timeout > TIMEOUT) printf("Timed out %d\n", loc);
            	change_mutex_state(i, loc, exiting);
            } else {
            	//printf("%d\n", loc);
            }
            break;
        case exiting:
    		for (int j = 0; j < NUM_DIRS; j++) {
				set_color(loc, j, RED);
			}
			
        	if (intersection_is_empty(&intersection[loc])) {
        		//printf("\b%d", intersection_is_empty(&intersection[0]));
        		change_mutex_state(i, loc, idle);
        	}
        	break;
        default:
            break;
        }
    }
}
