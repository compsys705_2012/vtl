#
# Makefile for VTL
#

MAIN_DIR=$(wildcard *.c)
SRC_DIRS=$(wildcard */*.c)

run : all
	./vtl

all : $(MAIN_DIR) $(SRC_DIRS)
	gcc -Wall -I. -std=c99 -g $(MAIN_DIR) $(SRC_DIRS) -o vtl -lm `pkg-config --cflags gtk+-2.0` `pkg-config --libs gtk+-2.0`
	
