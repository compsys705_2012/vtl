#ifndef VTL_GLOBALS_H
#define VTL_GLOBALS_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h> //allows using bool, true and false
#include <string.h>

//Array of cars in each lane is statically allocated
//This number may need to be larger to support higher car densities
#define MAX_CARS_PER_LANE 100
#define NUM_OF_LANES 24 //24 lanes for 4 intersections

#define LANE_DENSITY 900 //this is cars per hour (700)

#define MILLISECONDS_PER_UPDATE 100
#define MILLISECONDS_PER_SECOND 1000

//When this is 0 invisible cars are not drawn. Any other value they are drawn
//When DEBUG is 1 or more important error messages are printed
//When DEBUG is 2 or more low level error messages and important information are printed
//When DEBUG is 3 or more car spawning/lane change/car removal messages are printed.
//When DEBUG is 4 or more plate creation information is printed
//Max DEBUG level is currently 4
//Note: critical error messages print always regardless of DEBUG level
#define DEBUG 0

//When INVISIBLE_DEBUG is 1 or more some invisible car "interest" messages are printed
//When INVISIBLE DEBUG is 2 or more invisible car creation/deletion messages are printed.
//Max INVISIBLE DEBUG level is currently 2
#define INVISIBLE_DEBUG 0

#define VERIFY_GUI 0 // boolean to indicate whether to run the verification code on the gui, it may be slow so this is here to allow disabling of it

typedef enum Direction {
	UP = 0,
	DOWN = 1,
	LEFT = 2,
	RIGHT = 3,
	SN = UP,    // South to north
	NS = DOWN,  // North to south
	EW = LEFT,  // East to west
	WE = RIGHT  // West to east
} Direction;

//Each car has a TurnIntention which shows its intended direction of travel
typedef enum TurnIntention {
	LEFT_TURN,
	RIGHT_TURN,
	STRAIGHT
} TurnIntention;

typedef struct Point {
	float x;
	float y;
} Point;

/**
 * This structure represents a single car in the simulation
 * Each car has its own signals for red and orange internally.
 * Orange is currently unused but may be used later.
 * Note: cautious drivers should slow down for organge, but
 * aggresive drivers may choose to "run" orange lights.
 *
 * Invisible cars are placed at the "stop line" of the
 * intersection to cause other cars to stop

 * This is used instead of the leader car to stop other cars
 */
typedef struct Car {
	Point location;
	float speed; //km/h
	double aggressiveness; //driver aggressiveness between 0 and 1
	char plate[7]; //This is the car's licence plate, 3 letters and 3 numbers as a string. use this to ID car
	bool red; //tells the current car that the light for the intersection that it is approaching is red
	bool orange; //tells the current car that the light for the intersection that it is approaching is orange
	bool invisible; //sets the car to be dimenionless/invisible.
	TurnIntention turn_intention;
	bool picked; //chosen a turn direction
	int lane_id; //the lane which this car is in
} Car;

//This function iterates though the entire array of cars
#define foreach_car(i, start, end) for(i = start; (i < end) || ((i >= start) && (start > end)); i = ((i + 1) % MAX_CARS_PER_LANE))
#define decrement(i) ((i) = ((i) + MAX_CARS_PER_LANE -1) % MAX_CARS_PER_LANE)
#define increment(i) ((i) = ((i) + 1) % MAX_CARS_PER_LANE)
#define compare(i, start, end) (i < end) || (i > start && start > end)

/**
 * This struct describes the positions of a lane
 * A horizontal lane will have identical y co-ords
 * A vertical lane will have identical x co-ords
 * 
 * All cars in the simulation are associated with a lane. Lanes have direction.
 * Cars in a lane will always be travelling in the direction of the lane
 * 
 * Presently there is only one intersection in the simulation
 * Thus there are only 8 possible lanes.
 * Each lane has an ID number assigned as per <lane_info.txt>
 * The car at the "front" of the lane ie. closest to the next intersection is at start_index
 * The last car in the queue of cars on this lane is at end_index
 */
typedef struct Lane {
	Car cars[MAX_CARS_PER_LANE]; //This array contains all the cars that are in this lane. This is a circular array
	int start_index; //Points at the front of the array of cars
	int end_index; //Points at the back of the array of cars
	int count; //the actual number of cars in this lane
	Direction direction;
	Point start_pos;
	Point end_pos;
	float density; //This describes how many cars should be on this lane
	int lane_id;
} Lane;

typedef struct Intersection {
	Point origin;
	float width;
	float height;
	int isGenerated; // Whether the intersection was automatically generated
} Intersection;


// ====================================================================
//INFRASTRUCTURE
// ====================================================================

/* Traffic Light State */
typedef enum {
	RED = 0,
	ORANGE = 1,
	GREEN = 2,
}TrafLightState;

// Individual traffic light
typedef struct{
	float xPos;
	float yPos;
	TrafLightState state;
} trafLight;

// Traffic lights at a single intersection. One for horizontal and one for vertical.
// When turning is implemented, will need four, one for each lane.
typedef struct
{
	trafLight light[4];
} IntersectionLights;

// Internal struct used for verification by infrastructure
typedef struct Box {
	Point center;
	float width;
	float height;
	//Direction direction;
} Box;

/* ---- New Intersection ----*/
typedef struct IntersectionConstructor {
	Point origin;
	bool northDone;
	bool eastDone;
	bool southDone;
	bool westDone;
} IntersectionConstructor;

/* ---- Arrow ----*/
#define MAX_ARROW_CONNECTORS 5

// Arrow declaration.
// startConnectors is an array of pointers to arrows that connect to this arrow.
// endConnectors is an array of pointers to arrows that this arrow connects to.
// --- Please don't edit without first asking infrastructure ---
typedef struct Arrow {
	Point start;
	Point end;
	struct Arrow* endConnectors[MAX_ARROW_CONNECTORS];
	struct Arrow* startConnectors[MAX_ARROW_CONNECTORS];
} Arrow;

// Used in GUI for manual generation
void init_arrow(Arrow *arrow, float x1, float y1, float x2, float y2);

// New Intersection array
extern IntersectionConstructor* intersectionConstructors;

// Arrow array
#define MAX_ARROWS 1000

// The number of arrows generated in the map
extern int num_arrows;

// Internal variable for infrastructure group
extern int arrowNextInArray;

// The array of arrows. Contains all arrows generated in the map.
// Arrows can be generated either manually or automatically.
// Manual generation is through right click arrows on the GUI. This allows the creation of curves and other non-linear road types.
// Automatic generation. Infrastructure will generate the linear vertical and horizontal lane structure implemented in arrows,
// automatically when the go button is pressed.
// --- Please don't edit without first asking infrastructure ---
extern Arrow* arrows;

/* ---- Go Zone ----*/
// Internal variable used for verification by infrastructure
// 0 - SN
// 1 - NS
// 2 - EW
// 3 - WE
extern int max_goZone;
extern int goZone_size;
extern Box* goZone;

// Array of all traffic lights for the map. 2 Traffic lights per intersction.
extern IntersectionLights* intersectionLights;

// ====================================================================
// Other Globals
// ====================================================================

/** 
 * This is pretty much the primary structure of this simulation
 * It is the array of all the lanes in the simulation.
 * Each lane defines the physical setup of a lane
 * Each lane contains an array of Cars on that lane
 * Much of the simulation processing is done around this structure
 */
 // 24 lanes for the 4 intersection model.
 // If the 4 intersection model cannot be created, only the first 8 values in this
 // array will be used to construct the 1 intersection model.
extern Lane all_lanes[NUM_OF_LANES];

/*---------------------------Dimensions-------------------------------*/
extern const float CAR_LENGTH;
extern const float CAR_WIDTH;

/**
 * The dimensions of the map
 * Note: I have changed these from being const floats to #define
 * because of a limitation of C languge. Objects with static
 * storage duration must be initialized with "constant expressions" or
 * with aggregate initializers containing constant expressions. In C
 * what do you know, but a const is not a "constant expression". Funnily
 * enough it is in C++. Search for the error: "initializer element is not
 * constant" for more information
 */
//extern const float MAP_WIDTH;
//extern const float MAP_HEIGHT;
#define MAP_WIDTH 4000.0
#define MAP_HEIGHT 3000.0

extern const float intersection_width;
extern const float intersection_height;
extern const float intersection_padding; // How much gap to leave between intersections

extern const float TRAFFIC_LIGHT_WIDTH;
extern const float TRAFFIC_LIGHT_HEIGHT;

//Assuming the average family car is 4.4m long (See en.wikipedia.org/wiki/large_family_car)
//And that a car is 40 pixels long, then 40/4.4 = 9.1 pixels/m
#define PIXELS_PER_M 9.1

/*---------------------------End Dimensions-------------------------------*/

extern int global_speed_limit; //this is in km/h and can vary between 50 and 100;
extern const float UPDATES_PER_SECOND; //refresh rate of simulation

extern Intersection* intersections;
extern int number_of_intersections;
extern int max_number_of_intersections;

extern int to_continue;


/*----------Function Prototypes----------*/

//These functions are all defined in vtl.c
int total_cars_in_all_lanes(); //returns the total cars in the simulation
bool point_less_than(Point lhs, Point rhs);
bool point_greater_than(Point lhs, Point rhs);
bool point_equal_to(Point lhs, Point rhs);
bool indentical_y_coord(Point lhs, Point rhs);
bool indentical_x_coord(Point lhs, Point rhs);

float distance(Point from, Point to);

/*----------End Function Prototypes----------*/

#define is_almost_equal(a, b, tolerance) (fabs(a - b) <= tolerance)

#include "infrastructure/infrastructure.h"
#include "vehicle/index.h"
#include "vehicle/vehicle.h"
#include "vehicle/spawning.h"
#include "controller/controller.h"
#include "gui/gui.h"

#endif
