--------------------------------------------------------------------------------------

Devin Barry

-revised code for card following model
-revised code for car spawning
-revised driver aggressiveness
-making cars change lanes
-making cars turn
-setting up data structures
-many runtime verification functions
-many bugfixes
-updating lanes
-comprehensive code documentation and todos
-debugging functions

Devin

I have contributed work to almost every function in vehicles group
(but no work inside the verification folder)

Nearlly everything in the following files is coded by me:
	vehicles.h
	vehicles.c
	spawning.h
	spawning.c
	index.h
	index.c
	TODO.txt

The following functions from vehicles.c were contributed to (or created by) others:
	
	update_car_speed(Car *currentCar, float distance_to_car_ahead);
	void generatecarplate(char* plate);
	bool plate_is_unique(char* plate, int total_cars);

In spawning.c the following function is not created by me

	double randn_notrig(double mu, double sigma);


In spawing.c the following functions were worked on significantly by controller group
(I think mostly Buddy). Also the originals for the spawn_invis and create_invis functions
were actually stolen from controller groups folder and moved into spawning.c in thier
original incarnations. I would say these functions are a shared effort, but quite possibly
have more work in them from controller group.

	create_invisible_car(Lane *current_lane);
	spawn_invisible_car(int lane_number);
	remove_invisible_car(Lane *current_lane);

I hope I have remembered everything. There have been code tweaks and small changes
to algorithms added around the place by various people
--------------------------------------------------------------------------------------

Jun-Hong Ling
-initial code for car following model
-code integration
-initial driver aggressiveness
-some bugfixes
-verification of basic car following model

Yu liang
-car following model research(Chandler model and Gipps model)
-blast experince in reachibility checking and assertion checking, however the assertion checking was not working
-create a verification file car_init_verification.c to check init_vehicle(),init_prng(),and update_car_turning_intention()

JongSeong Song
-car plate generator
-uniqueness checking of car plates
-uniform and normal distribution random number generators
-initial code for car spawning
