// =====================================================================================
// =====================================================================================
// Including all relevant declarations from globals.h, in order to make the functions work
#include <stdio.h>
#include <string.h>
#include "stdbool2.h"

#define MAX_CARS_PER_LANE 50
#define NUM_OF_LANES 24.

//#define LANE_DENSITY 1200

#define MILLISECONDS_PER_UPDATE 100
#define MILLISECONDS_PER_SECOND 1000

typedef enum Direction {
	UP,
	DOWN,
	LEFT,
	RIGHT,
	SN = UP, 
	NS = DOWN,
	EW = LEFT,
	WE = RIGHT
} Direction;

typedef enum TurnIntention {
	LEFT_TURN,
	RIGHT_RIGHT,
	STRAIGHT
} TurnIntention;

typedef struct Point {
	float x;
	float y;
} Point;

typedef struct Car {
	Point location;
	float speed;
	char plate[7];
	BOOL red;
	BOOL orange;
	BOOL invisible;
	TurnIntention turn_intention;
	int lane_id;
} Car;

typedef struct Lane {
	Car cars[MAX_CARS_PER_LANE];
	int start_index;
	int end_index;
	int count;
	Direction direction;
	Point start_pos;
	Point end_pos;
	float density;
	int lane_id;
} Lane;

typedef struct Intersection {
	Point origin;
	float width;
	float height;
	int isGenerated;
} Intersection;

typedef enum {
	RED = 0,
	ORANGE = 1,
	GREEN = 2,
}TrafLightState;

typedef struct{
	float xPos;
	float yPos;
	TrafLightState state;
} trafLight;

typedef struct
{
    trafLight trafLightNS;
    trafLight trafLightEW;
} IntersectionLights;

typedef struct Box {
	Point center;
	float width;
	float height;
	Direction direction;
} Box;

typedef struct IntersectionConstructor {
	Point origin;
	BOOL northDone;
	BOOL eastDone;
	BOOL southDone;
	BOOL westDone;
} IntersectionConstructor;

#define MAX_ARROW_CONNECTORS 5

typedef struct Arrow {
	Point start;
	Point end;
	struct Arrow* endConnectors[MAX_ARROW_CONNECTORS];
	struct Arrow* startConnectors[MAX_ARROW_CONNECTORS];
} Arrow;

#define MAP_WIDTH 4000.0
#define MAP_HEIGHT 3000.0

Box goZone[4];

int num_arrows = 0;
Arrow* arrows;
int arrowNextInArray = 0;

IntersectionLights* intersectionLights;

IntersectionConstructor* intersectionConstructors;

Lane all_lanes[24];

const float CAR_LENGTH = 40.0;
const float CAR_WIDTH = 20.0;

const float intersection_width = 100.0;
const float intersection_height = 100.0;
const float intersection_padding = 20.0; // How much gap to leave between intersections

const float TRAFFIC_LIGHT_WIDTH = 20.0;
const float TRAFFIC_LIGHT_HEIGHT = 20.0;
#define PIXELS_PER_M 9.1

int global_speed_limit = 50;
const float UPDATES_PER_SECOND = (float)MILLISECONDS_PER_SECOND / MILLISECONDS_PER_UPDATE;

Intersection* intersections;
int number_of_intersections = 1;
int max_number_of_intersections = 5;

int to_continue = 1;

// =====================================================================================
// =====================================================================================
