#include <assert.h>

int inputarray[21] ={-5,-4.5,-4,-3,-2,0,2,4,6,8,10,12,14,16,18,20,20,20,20,20,20};

//car in front
float car1pos=-5;
int car1speed=0;


float car2pos=-20;
int car2speed=0;

int ACCEL = 2; //km/h/tick
int DECEL = 15; //km/h/tick

float anumber;
float global_speed_limit = 50;
float distancetonextcar;

int main (int argc, char* argv[])
{
	
	int i;
	for ( i=0;i<20;i++ )//car1pos
	{
		car1pos=inputarray[i];//read car in front's position from static array
		distancetonextcar=fabs(car2pos-car1pos ); //compute following car's distance to the car in front
		if(distancetonextcar< 7)//smaller than 7 meters abs distace to car in front
		{				
			car2speed=-DECEL; //slow down
		}
		else
		{
			car2speed+=ACCEL;//speed up
		}
			
		if(car2speed<0) //can't have negative speed
		{
			car2speed=0;
			
		}
		
		
		if (car2speed>=global_speed_limit)//over speed limit of 60kmh
		{
			car2speed=global_speed_limit;//clamp speed to speed limit
		}
		car2pos+=(car2speed/3.6);//update car position by adding its speed for the current tick

		if (car2speed>global_speed_limit)//speed limit in kmh, which should not be exceeded
		{
			SPEED_LIMIT_EXCEED: goto SPEED_LIMIT_EXCEED;
		}
		
		if (distancetonextcar < 3) //distance to the next car, should not be too small
		{
			DISTANCE_TOO_SMALL: goto DISTANCE_TOO_SMALL;
		}

		
		// SPEED_IS_NOT_NEGATIVE:
		SPEED_IS_NOT_NEGATIVE: assert(car2speed >=0);//following car should not have a negative speed. assert doesn't work in blast?
		
		printf("%f \t %f \n",car1pos,car2pos);


	}
		
		
	
	

	return 0;
}



		

