#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#define MAX_PLATES (MAX_CARS_PER_LANE*NUM_OF_LANES * 10)
#define MAX_CARS_PER_LANE 50
#define NUM_OF_LANES 24 
#define LANE_DENSITY 700 //this is cars per hour
//#define RAND_MAX 1;
const char *old_plates[MAX_PLATES];
 int success=0;
 int fail=0;
 
typedef enum bool{
   true,
   false,
}bool;
typedef enum TurnIntention {
	LEFT_TURN,
	RIGHT_RIGHT,
	STRAIGHT
} TurnIntention;
typedef struct Point {
	float x;
	float y;
} Point;
typedef struct Car {
	Point location;
	float speed; //km/h
	char plate[7]; //This is the car's licence plate, 3 letters and 3 numbers as a string. use this to ID car
	bool red; //tells the current car that the light for the intersection that it is approaching is red
	bool orange; //tells the current car that the light for the intersection that it is approaching is orange
	bool invisible; //sets the car to be dimenionless/invisible.
	TurnIntention turn_intention;
	int lane_id; //the lane which this car is in
} Car;
typedef struct Lane {
	Car cars[MAX_CARS_PER_LANE]; //This array contains all the cars that are in this lane. This is a circular array
	int start_index; //Points at the front of the array of cars
	int end_index; //Points at the back of the array of cars
	int count; //the actual number of cars in this lane
	float density; //This describes how many cars should be on this lane
	int lane_id;
} Lane;

Lane all_lanes[NUM_OF_LANES];


void update_car_turning_intention() {
	TurnIntention turn_intention;
	// generate random number between 0.0 and 1.0 (uniformly distributed)
	double random = ((double)rand() / (double)RAND_MAX);
	
	//set turning intention based on random number
	if (random > 0.5) turn_intention = STRAIGHT;
	else turn_intention = LEFT_TURN;
	if((random>1)||(random<0))
	{
		G_Turning:goto G_Turning;
	}
	
}
void init_prng() {
	success++;
	//this seeds the random number generator.
	//this should only be called once
	unsigned int iseed = (unsigned int)time(NULL);
	srand(iseed);
}
void init_all_cars_in_all_lanes() {
	int i, j;success++;
	for (i = 0; i <NUM_OF_LANES; i++) {
	
		all_lanes[i].density = LANE_DENSITY;
		all_lanes[i].start_index = 0;
		all_lanes[i].end_index = 0;
		all_lanes[i].count = 0;
		for (j = 0; j < MAX_CARS_PER_LANE; j++) {
			all_lanes[i].cars[j].location.x = 0;
			all_lanes[i].cars[j].location.y = 0;
			all_lanes[i].cars[j].speed = 0;
			strcpy(all_lanes[i].cars[j].plate, "000000"); //initialize plates with all zeros
			all_lanes[i].cars[j].red = false;
			all_lanes[i].cars[j].orange = false;
			all_lanes[i].cars[j].invisible = false;
			all_lanes[i].cars[j].turn_intention = STRAIGHT;
			all_lanes[i].cars[j].lane_id = all_lanes[i].lane_id; //here we copy the /
}
void init_vehicle() {
  
	//these functions are all in spawning.c
	init_prng();
	if(success!=1)
	{
		G_Prng_ERROE:goto G_Prng_ERROE;
 	}
	init_all_cars_in_all_lanes();
	if(success==2)
	{  
		G_initCar_ERROE:goto G_initCar_ERROE;
 	}
	//initialise the old plates array
	int i;
        
	for (i = 0; i < MAX_PLATES; i++) {
	 	success++;
		old_plates[i] = "000000";
	}
	if(success==(MAX_CARS_PER_LANE*NUM_OF_LANES * 10+2))
	{
	   G_Plates_ERROR:goto G_Plates_ERROR;
	}
	
}

