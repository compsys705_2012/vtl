#include "globals.h"

/**
 * @author: Devin Barry
 *
 * index.c contains functions which ensure safety when working with the circular array
 * of cars. These functions came about while searching for seg faults. I wanted to be
 * completely sure that all places in the code which were manipulating pointers to
 * circular array were doing it correctly. Thus these functions came into being. They
 * ensure, among other things that the start and end pointers never overlap.
 *
 * Many of the printf statements I added to test for error conditions during my seg fault
 * searches, I ended up leaving in these functions. These turned out to be incredibly
 * useful, because later in the development of code, these errors would print and would 
 * immediately point to incorrect implementations.
 *
 * The names of all these functions are pretty self-explanatory and thus no comments for
 * each individual function have been written
 */


/*============================ ADD AND REMOVE CAR FUNCTIONS ===============================*/

void add_car_to_front_of_lane(int lane_number) {
	//increment total number of cars in this lane
	all_lanes[lane_number].count++;

	//add car to the front of the the array
	//safe decrement. checks that index does not overlap
	decrement_start_index(lane_number);
}

void remove_car_from_front_of_lane(int lane_number, bool invisible) {
	//check if this car at the front is invisible
	if (all_lanes[lane_number].cars[all_lanes[lane_number].start_index].invisible) {
		//if we werent trying to remove an invisible car
		if (!invisible) {
			printf("\n\n\nError: ILLEGAL removing of invisible car!\n\n\n");
		}
	}
	//decrease number of cars in this lane
	all_lanes[lane_number].count--;
		
	//increment start index (car behind old car is now at front)
	//safe increment. checks that index does not overlap
	increment_start_index(lane_number);
}

int add_car_to_rear_of_lane(int lane_number) {
	//the position in array of cars where this new car is being created
	int index = all_lanes[lane_number].end_index;

	//test if index is out of bounds
	//This test can be removed if speed is important. It doesnt ever occur
	if (index > (MAX_CARS_PER_LANE - 1)) {
		printf("\n\n\nERROR - add_car_to_rear_of_lane: Index is out of bounds!\n\n\n\n");
	}
	//increment total number of cars in this lane
	all_lanes[lane_number].count++;

	//point the end index at the next free space in the array
	//safe increment. checks that index does not overlap
	increment_end_index(lane_number);

	return index;
}

void remove_car_from_rear_of_lane(int lane_number) {
	//decrease number of cars in this lane
	all_lanes[lane_number].count--;
		
	//decrement end index, point the next free space at the end car
	//safe increment. checks that index does not overlap
	decrement_end_index(lane_number); //this assumes that the car at the front is removed!
}


/*============================ INDEX FUNCTIONS ===============================*/


/**
 * Uses the increment function described in the globals.h
 * to increment the end index of the cars array belonging to
 * <lane_number>. Tests for pointer overlap.
 * Allows pointer overlap if the array is empty otherwise
 * prints an error message.
 */
void increment_end_index(int lane_number) {
	int temp_index = all_lanes[lane_number].end_index;
	increment(temp_index);
	if (temp_index != all_lanes[lane_number].start_index) {
		increment(all_lanes[lane_number].end_index);
		return;
	}
	if (all_lanes[lane_number].count == 0) {
		increment(all_lanes[lane_number].end_index);
		return;
	}
	printf("\n\nERROR- End index overlap during increment\n\n");
}

/**
 * Uses the decrement function described in the globals.h
 * to decrement the end index of the cars array belonging to
 * <lane_number>. Tests for pointer overlap.
 * Allows pointer overlap if the array is empty otherwise
 * prints an error message.
 */
void decrement_end_index(int lane_number) {
	int temp_index = all_lanes[lane_number].end_index;
	decrement(temp_index);
	if (temp_index != all_lanes[lane_number].start_index) {
		decrement(all_lanes[lane_number].end_index);
		return;
	}
	if (all_lanes[lane_number].count == 0) {
		decrement(all_lanes[lane_number].end_index);
		return;
	}
	printf("\n\nERROR- End index overlap during decrement\n\n");
}

/**
 * Uses the increment function described in the globals.h
 * to increment the start index of the cars array belonging to
 * <lane_number>. Tests for pointer overlap.
 * Allows pointer overlap if the array is empty otherwise
 * prints an error message.
 */
void increment_start_index(int lane_number) {
	int temp_index = all_lanes[lane_number].start_index;
	increment(temp_index);
	if (temp_index != all_lanes[lane_number].end_index) {
		increment(all_lanes[lane_number].start_index);
		return;
	}
	if (all_lanes[lane_number].count == 0) {
		increment(all_lanes[lane_number].start_index);
		return;
	}
	printf("\n\nERROR- Start index overlap during increment\n\n");
}

/**
 * Uses the decrement function described in the globals.h
 * to decrement the start index of the cars array belonging to
 * <lane_number>. Tests for pointer overlap.
 * Allows pointer overlap if the array is empty otherwise
 * prints an error message.
 */
void decrement_start_index(int lane_number) {
	int temp_index = all_lanes[lane_number].start_index;
	decrement(temp_index);
	if (temp_index != all_lanes[lane_number].end_index) {
		decrement(all_lanes[lane_number].start_index);
		return;
	}
	if (all_lanes[lane_number].count == 0) {
		decrement(all_lanes[lane_number].start_index);
		return;
	}
	printf("\n\nERROR- Start index overlap during decrement\n\n");
}

