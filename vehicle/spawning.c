#include "globals.h"

/* This .c file contains functions that create and destroy cars in the simulation */

/**
 * Initialises the random number generator with a seed based upon
 * current time
 */
void init_prng() {
	//this seeds the random number generator.
	//this should only be called once
	unsigned int iseed = (unsigned int)time(NULL);
	srand(iseed);
}

//this fn generates numbers from normal distbn. currently gets stuck/isbroken?
double randn_notrig(double mu, double sigma) {
		static float storedDeviate = 0;
		static int deviateAvailable = 0;
		double polar;
		double rsquared;
		double var1;
		double var2;
		double max_rand=32767;
       	//	If no deviate has been stored, the polar Box-Muller transformation is
	//	performed, producing two independent normally-distributed random
	//	deviates.  One is stored for the next round, and one is returned.
	if (!deviateAvailable) {

		//	choose pairs of uniformly distributed deviates, discarding those
		//	that don't fall within the unit circle
		do {
			int randnum1;
			double randnum2;
			randnum1=rand();
			randnum2=(double)randnum1;
			var1=2.0*( randnum2/max_rand ) - 1.0;
			randnum1=rand();
			randnum2=(double)randnum1;
			var2=2.0*( randnum2/max_rand ) - 1.0;
			rsquared=var1*var1+var2*var2;
		} while ( rsquared>=1.0 || rsquared == 0.0);

		//	calculate polar tranformation for each deviate
		polar=sqrt(-2.0*log(rsquared)/rsquared);

		//	store first deviate and set flag
		storedDeviate=var1*polar;
		deviateAvailable=1;

		//	return second deviate
		return var2*polar*sigma + mu;
	}

	//	If a deviate is available from a previous call to this function, it is
	//	returned, and the flag is set to false.
	else {
		deviateAvailable=0;
		return storedDeviate*sigma + mu;
	}

}
/**
 * This function inits all the car lanes
 * 
 * This function inits every possible Car in all the lanes in the simulation
 * (this is simply to ensure that all Cars have the correct init values)
 * No actual cars are created in this part
 */
void init_all_cars_in_all_lanes() {
	int i, j;
	for (i = 0; i < NUM_OF_LANES; i++) {
		all_lanes[i].density = LANE_DENSITY;
		all_lanes[i].start_index = 0;
		all_lanes[i].end_index = 0;
		all_lanes[i].count = 0;
		for (j = 0; j < MAX_CARS_PER_LANE; j++) {
			all_lanes[i].cars[j].location.x = 0;
			all_lanes[i].cars[j].location.y = 0;
			all_lanes[i].cars[j].speed = 0;
			all_lanes[i].cars[j].aggressiveness = 0.5; //driver aggressiveness midway between 0 & 1
			strcpy(all_lanes[i].cars[j].plate, "000000"); //initialize plates with all zeros
			all_lanes[i].cars[j].red = false;
			all_lanes[i].cars[j].orange = false;
			all_lanes[i].cars[j].invisible = false;
			all_lanes[i].cars[j].picked = false;
			all_lanes[i].cars[j].turn_intention = STRAIGHT;
			all_lanes[i].cars[j].lane_id = all_lanes[i].lane_id; //here we copy the lane ID from all_lanes
		}
	}
}

/** 
 * This function spawns cars based upon the supplied lane density
 * This function is called once per tick for each lane
 * if the density is too high, cars may appear uniform.
 *
 *
 */
void spawn_car_based_upon_lane_density(int lane_number) {
	static float cars_to_spawn[NUM_OF_LANES] = { 0.0 };	
	
	//density in cars per hour / 3600 gives cars per second
	float cars_per_second = all_lanes[lane_number].density / 3600.0;
	//this is how many cars should spawn per tick. This number is probably less than 1
	float cars_per_tick = cars_per_second / UPDATES_PER_SECOND;
	
	//each tick add the number of cars to spawn per tick
	cars_to_spawn[lane_number] += cars_per_tick;

	//get ratio of the cars to spawn and the MAX_SPAWN value
	float spawn_probability = cars_to_spawn[lane_number] / MAX_SPAWN;

	// generate random number between 0.0 and 1.0 (uniformly distributed)
	double random = ((double)rand() / (double)RAND_MAX);

	//spawn a car if the probability is great enough
	if (spawn_probability > random) {
		//if the car successfully is created
		if (safe_spawn_one_car(lane_number)) {
			cars_to_spawn[lane_number] -= 1.0; //remove this spawned car
		}
	}
}

/**
 * All real cars used in the simulation are created in this function
 */
void create_simulation_cars() {
	//Create cars manually in lane 0
	create_cars_in_lane(0, 4);
	//Create cars manually in lane 2
	create_cars_in_lane(2, 4);
	//Create cars manually in lane 4
	create_cars_in_lane(4, 4);
	//Create cars manually in lane 6
	create_cars_in_lane(6, 4);

	
}

/**
 * Creates <num_cars> regular cars in the specified lane
 */
void create_cars_in_lane(int lane_number, int num_cars) {
	int i;
	for (i = 0; i < num_cars; i++) {
		//spawn one car checking that there is space
		safe_spawn_one_car(lane_number);
	}
}

/**
 * Creates a singular car in a lane, first checking to see
 * if there is space in that lane to spawn the car
 * Will not spawn a car if there is insufficient space
 */
bool safe_spawn_one_car(int lane_number) {
	//check that there is room in the array to spawn another car
	//because of the way that the indexes work, there should always be one free slot in the array of cars
	//Additionally because of the need to spawn an invisible car at the front of any lane we should allow
	//two free slots in all lanes. This is where the 2 comes from.
	if (all_lanes[lane_number].count < (MAX_CARS_PER_LANE-2)) {
		create_regular_car(lane_number);
		return true;
	}
	//This message is somewhat important. It is almost an error
	//It should be printed on low debug levels
	if (DEBUG >= 1) {
		printf("Unable to spawn car - lane is full!\n");
	}
	return false;
}

/**
 * This function creates a new car for the simulation. A new regular car
 * is always created at the back of the lane of cars. New cars are always
 * created with an initial speed of zero.
 * 
 * @args lane_number - the lane number where the new car is being created
 *
 */
void create_regular_car(int lane_number) {
	//increments count and end index, fetches the current index
	int index = add_car_to_rear_of_lane(lane_number);
	
	//create a plate for the new car
	generatecarplate(all_lanes[lane_number].cars[index].plate);
	
	//place that car at location on the lane that it is supposed to spawn on
	all_lanes[lane_number].cars[index].location.x = all_lanes[lane_number].start_pos.x;
	all_lanes[lane_number].cars[index].location.y = all_lanes[lane_number].start_pos.y;
	
	//initial start speed of all cars is zero
	all_lanes[lane_number].cars[index].speed = 0;
	
	//initialize drive aggressiveness between 0 and 1
	all_lanes[lane_number].cars[index].aggressiveness = ((double)rand() / (double)RAND_MAX); 
	
	//all the boolean values for new real cars are false
	all_lanes[lane_number].cars[index].red = false;
	all_lanes[lane_number].cars[index].orange = false;
	all_lanes[lane_number].cars[index].invisible = false;
	//all cars current start of going straight
	all_lanes[lane_number].cars[index].turn_intention = STRAIGHT;
	
	 //The new car has the same lane_id as the lane in which it is in
	all_lanes[lane_number].cars[index].lane_id = all_lanes[lane_number].lane_id;

	//print debug message if set
	if (DEBUG >= 3) {
		printf("Created car with aggressiveness %f\n",all_lanes[lane_number].cars[index].aggressiveness);
		printf("Created car with plate \"%s\" in lane %d\n", all_lanes[lane_number].cars[index].plate, lane_number);
	}
}

/**
 * This function creates a new invisible car for the simulation. A new
 * invisible car is always created at the front of the lane of cars. It
 * is always stopped. If the VTL model tries to spawn an invisible car
 * behind the car which is currently at the front or too close to the
 * front of the present front car, it will be disallowed and this function
 * will return false. If an invisible car is spawned it will return true.
 * 
 * @args lane_number - the lane number where the new car is being created
 *
 * @return bool - whether the inivisble car was successully created.
 *
 * TODO This function has been broken by VTL people. It needs to be
 * fixed. It always creates invisible car now.
 */
bool create_invisible_car(Lane *current_lane) {
	int lane_id = current_lane->lane_id;
	spawn_invisible_car(lane_id); //try to create an invisible car
	
	//test physical the distance between the front car and the invisible car
	float distance = distance_between_front_cars(lane_id);
	
	//If distance is less than 50, the centre points of a the cars are less than one car length
	//apart. This means the cars are inside each other, or the invisible car is actually behind
	//the front in the array (in physical distance)
	if (distance < (CAR_LENGTH + MIN_DISTANCE_BETWEEN_CARS)) {
		if (INVISIBLE_DEBUG >= 1) {
			printf("Small invisible car distance - swapping cars!\n");
		}	
		//here two cars are having their positions swapped
		//doesnt seem very clever. This breaks ordering in the array
		//cars will no longer be ordered based upon their position.
		//This swap can only be performed with certainty if the distance is < 0
		//Swapping with distance < 50 could mean cars become incorectly ordered.

		int start = current_lane->start_index;
		int second = current_lane->start_index;
		increment(second);
		Car temp = current_lane->cars[start];
		current_lane->cars[start] =  current_lane->cars[second];
		current_lane->cars[second] = temp;
		return false;
	}
	if (INVISIBLE_DEBUG >= 2) {
		printf("Added invisible car to front of lane %d (Distance is %.0f).\n", lane_id, distance);
	}
	return true;
}

/**
 * This function creates a new invisible car for the simulation. A new
 * invisible car is always created at the front of the lane of cars. It
 * is always stopped.
 * 
 * @args lane_number - the lane number where the new car is being created
 */
void spawn_invisible_car(int lane_number) {
	//if there is an invisible car there already, dont create another
	if (all_lanes[lane_number].cars[all_lanes[lane_number].start_index].invisible) return;

	//increments count, decrements start_index
	add_car_to_front_of_lane(lane_number);
	
	//shorthand for the index position where this new invisible car will be placed
	int index = all_lanes[lane_number].start_index;
	
	//clear the plate of the car.
	strcpy(all_lanes[lane_number].cars[index].plate, "000000"); //Set the plate to be all zeros
	
	//place the invisible car inside the intersection
	if (all_lanes[lane_number].direction == WE) {
		all_lanes[lane_number].cars[index].location.x = all_lanes[lane_number].end_pos.x + INVIS_OFFSET;
	} else if (all_lanes[lane_number].direction == EW) {
		all_lanes[lane_number].cars[index].location.x = all_lanes[lane_number].end_pos.x - INVIS_OFFSET;
	}
	else {
		all_lanes[lane_number].cars[index].location.x = all_lanes[lane_number].end_pos.x;
	}
	if (all_lanes[lane_number].direction == SN) {
		all_lanes[lane_number].cars[index].location.y = all_lanes[lane_number].end_pos.y - INVIS_OFFSET;
	} else if (all_lanes[lane_number].direction == NS) {
		all_lanes[lane_number].cars[index].location.y = all_lanes[lane_number].end_pos.y + INVIS_OFFSET;
	}
	else {
		all_lanes[lane_number].cars[index].location.y = all_lanes[lane_number].end_pos.y;
	}
	
	all_lanes[lane_number].cars[index].speed = 0; //initial start speed of all cars is zero
	all_lanes[lane_number].cars[index].red = true; //invisible cars always have red lights
	all_lanes[lane_number].cars[index].orange = false;
	all_lanes[lane_number].cars[index].invisible = true; //don't draw the car
	all_lanes[lane_number].cars[index].turn_intention = STRAIGHT; //just here for completeness
	
	//The new car has the same lane_id as the lane in which it is in
	all_lanes[lane_number].cars[index].lane_id = all_lanes[lane_number].lane_id;
}


/**
 * This function removes a car from its lane when it drives outside the
 * limits of the lane
 * 
 * There is implicit assumption in this function that the car the drives
 * outside the limits of the lane is at the front of the array
 * 
 * This is a very weak assumption and should be tested here!!
 * TODO
 */
void remove_regular_car(Car *currentCar) {
	if (outside_lane_limits(currentCar)) {
		//delete car from its lane
		//this assumes that the car at the front is removed!
		remove_car_from_front_of_lane(currentCar->lane_id, false); //regular car
		
		//Safety checks for car removal
		if (all_lanes[currentCar->lane_id].count < 0) {
			printf("\n\n\n\nERROR - remove_regular_car: lane count less than 0!\n\n\n");
		}
		
		//print debug message if set
		if (DEBUG >= 3) {
			printf("Removed car from lane %d\n", currentCar->lane_id);
		}
	}
}


/**
 * This function removes an invisible car from the front of the lane when
 * this is needed in the VTL code.
 */
void remove_invisible_car(Lane *current_lane) {
	int lane_id = current_lane->lane_id;
	int second_index = all_lanes[lane_id].start_index;
	increment(second_index);
	if (all_lanes[lane_id].count > 0) {
		if (all_lanes[lane_id].cars[all_lanes[lane_id].start_index].invisible) {
			//delete the car at the front of the lane
			remove_car_from_front_of_lane(lane_id, true); //invisible
		
			//print debug message if set
			if (INVISIBLE_DEBUG >= 2) {
				printf("Removed invisible car from lane %d\n", lane_id);
			}
		}
		else if (all_lanes[lane_id].cars[second_index].invisible) {
			Car temp = current_lane->cars[current_lane->start_index];
			remove_car_from_front_of_lane(lane_id, true);
			current_lane->cars[current_lane->start_index] = temp;
			
		} else {
		
			//print debug message if set
			if (INVISIBLE_DEBUG >= 2) {
				printf("Failed to remove invisible car from lane %d: front car does not appear invisible.\n", lane_id);
			}
		}
	}
	else {
		//print debug message if set
		if (INVISIBLE_DEBUG >= 2) {
			printf("Failed to remove invisible car from lane %d: Lane is empty.\n", lane_id);
		}
	}
}
