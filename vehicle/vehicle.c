#include "globals.h"

/*---------- global variables used only in vehicle.c ----------*/

//we allow ten times the maximum number of cars * lanes for the plate array
#define MAX_PLATES (MAX_CARS_PER_LANE*NUM_OF_LANES * 50)

//number of pixels away from the end of the lane that is considered "near" the end
#define NEAR_END 200

//size is 27 to include null terminating char
const char ALPHABET[27] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

//These are constants used in the vehicle following model
//They are not used raw like this but are modified by other factors
const float ACCEL = 3.0; //km/h/tick
const float DECEL = 3.5; //km/h/tick
const float ORANGE_DECEL = 5.0; //km/h/tick

//driver agressiveness effect, 1.0 is default
//determines how much driver aggressiveness affects car behaviour
//higher numbers cause a greater effect
//tested range betwen 0 and 1.5
const float AGGRESSIVENESS_EFFECT = 1.0;

//used in calculating the impact of driver aggresiveness on speed
#define SPEED_CONSTANT 15.0

//this array contains all the plates that have already been generated
char *old_plates[MAX_PLATES];

//Starting lanes Clockwise from lane 0
const int START_LANES[8] = {0, 6, 17, 23, 11, 14, 20, 8};

//restricted starting lanes for testing only
//const int START_LANES[4] = {17, 23, 11, 14};


/*============================ MAIN FUNCTIONS FOR INIT AND UPDATE ===============================*/

void init_vehicle() {
	//these functions are all in spawning.c
	init_prng();
	init_all_cars_in_all_lanes();
	
	//initialise the old plates array
	//TODO not sure if this is needed anymore
	int i;
	for (i = 0; i < MAX_PLATES; i++) {
		old_plates[i] = "000000";
	}
	//This function creates cars statically and is no longer used
	//except possibly for testing
	//create_simulation_cars();
}

/**
 * Called each tick to update the locations are all cars
 */
void update_vehicle() {
	for (int i = 0; i <  NUM_OF_LANES; i++) {
		update_car_lane(&all_lanes[i]);
		
		//only create cars in "spawning" lanes
		for (int j = 0; j < (sizeof(START_LANES) / sizeof (int)); j++) {
			if (i == START_LANES[j]) {
				spawn_car_based_upon_lane_density(i);
				break;
			}
		}
	}
}

/**
 * This function will update the positions of all cars in a certain lane
 *
 * The main loop in this function comes from the macro defined in globals.h
 * It ensures that the circular buffer is always iterated in the correct
 * order. The car at the front (closest to the intersection) of the lane
 * will always occur first in the loop (when i = startIndex). i will
 * increment through all cars (including possibly wrapping around at the
 * end of the array and counting up again) untill endIndex is reached.
 * 
 * accessing the current car can be done as follows:
 * current_lane->cars[i]            <---This is the current car
 */
void update_car_lane(Lane *current_lane) {
	int i;
	float distance_to_car_ahead;
	float distance_to_end;

	int startIndex = current_lane->start_index;
	int endIndex = current_lane->end_index;

	foreach_car(i, startIndex, endIndex) {
		//fetch the distance of this car from the end of the lane
		distance_to_end = distance_to_end_of_lane(current_lane->lane_id, &(current_lane->cars[i]));

		//test that this car has its co-ordinates matched correctly
		//note this can be commented out to improve speed
		test_coordinate_correctness(current_lane->lane_id, &(current_lane->cars[i]));

		if (distance_to_end < NEAR_END) {
			//car turning direction is randomly picked
			update_car_turning_intention(&(current_lane->cars[i]));
		}

		//if this is the front car in the lane, it gets special treatment
		if (i == startIndex) {
			//if the car at the front of the array is invisible, nothing below applies to it
			if (current_lane->cars[i].invisible) continue;

			//here we would look ahead to next lane and fetch distance to car at end
			//of that lane.
			distance_to_car_ahead = distance_to_car_ahead_next_lane(distance_to_end, current_lane->lane_id, &(current_lane->cars[i]));
			//distance_to_car_ahead = distance_to_rear_car_in_next_lane(current_lane->lane_id, &(current_lane->cars[i]));
			
			//The leader car works just like other cars in the car following model,
			//but has very large distance to "car in front".
			//If there is no next lane, the distance to the car ahead is set to 1000
			// or more by the function above.
				
			if (car_is_at_end_of_lane(current_lane->lane_id, &(current_lane->cars[i]))) {
				//printf("Car has reached end of lane!\n");

				//TODO possibly change this to work on all cars
				//To decrease processing quantity only leader car is tested for lane transitions
				//and removal. This works with all the other cars in the lane too

				//cars are tested for the possibility of them transitioning between lanes
				//TODO it should be possible for multiple cars to transition between lanes per tick
				if (transition_car_to_new_lane(current_lane->lane_id, &(current_lane->cars[i]), -distance_to_end)) {
					continue; //if car has moved lane, it is no longer there. dont do any more processing with it
				}
			
				//cars are also tested for being outside the bounds of the lanes
				remove_regular_car(&(current_lane->cars[i])); //gets rid of cars that go outside bounds

			}
		}
		else {
			int i_prev = i;
			decrement(i_prev);
			distance_to_car_ahead = distance_to_car_in_front(&(current_lane->cars[i]), &(current_lane->cars[i_prev]));
		}
		
		//update the speed and position of the current car based upon the car following model
		car_following_model(&(current_lane->cars[i]), distance_to_car_ahead);
	}
}

/**
 * This function updates the position of the current car assuming it is the leader
 * 
 */
void leader_car_model(Car *currentCar) {
	//change the current cars speed
	update_car_speed(currentCar, 1000.0); //1000 pixels to car in front
	//move car forward in the direction it was travellening at the new speed
	update_car_position(currentCar, 1000.0);
}

/**
 * This function updates the position of the current car, but obeys the car following model
 * The model used here is the "Devin Car Following Model"
 */
void car_following_model(Car *currentCar, float distance_to_car_ahead) {
	//adjust the speed of the current car based on the distance to the next car
	update_car_speed(currentCar, fabs(distance_to_car_ahead));
	//move car forward in the direction it was travellening at the new speed
	update_car_position(currentCar, distance_to_car_ahead);
}

/**
 * This function is the core of the Devin Car Following Model
 * This function adjusts the speed of the car based on its distance to the
 * car in front. The closer the current car gets to the car in front,
 * the faster the current car slows down (Thanks for this Buddy)
 * 
 * If the next car in front is very far away, then the current car will
 * speed up untill it reaches the speed limit of road. (currently the
 * global speed limit)
 */
void update_car_speed(Car *currentCar, float distance_to_car_ahead) {
	const float following_distance = 100; //This is currently the value of two and a half car lengths
	
	//for the purposes of drawing cars we must account for the length of a car
	//in the distance to the car in front calculations
	float seperation_distance = distance_to_car_ahead - CAR_LENGTH;
	//seperation_distance = seperation_distance*PIXELS_PER_M; //distance is in metres

	//this value determines how much driver aggressiveness affects speed
	float speed_impact = currentCar->aggressiveness * AGGRESSIVENESS_EFFECT * SPEED_CONSTANT;

	//this value is the drivers own speed limit based upon their aggressiveness
	//This calculation will leave a driver that has average agressiveness with indentical speed to the speed limit
	//This value could actually be stored permanently for each car becuse it does not change
	//save calculating it every time.
	float driver_speed_limit = global_speed_limit + speed_impact - (SPEED_CONSTANT / 2);

	//if the orange or red lights are set for any car then always slow down regardless
	//Orange light causes drivers to slow down to 15 km/h
	if (currentCar->orange) {
		currentCar->speed -= ORANGE_DECEL;
		if (currentCar->speed < 15) {
			currentCar->speed = 15;
		}
		return;
	}

	//red light causes the car to slow down. It may eventually stop
	if (currentCar->red) {
		currentCar->speed -= DECEL;
	}

	//TODO
	//calculate difference between speed of current car and speed of car in front
	//use this speed difference as a multiplier of the distance to car ahead (divide currentcar speed by car in front speed)
	//That way if the speed difference is a large positive number (car ahead is slower than current car)
	//the "distance to start braking" will be larger
	//if the car speeds are identical the "difference to start braking" will be normal distance (multiply by unity)
	//if we are travelling much slower than car in front then distance to start braking will be small
	//these changes should improve this model significantly

	
	
	//This part of the model keeps cars at a safe distance from each other when stopping
	else if (seperation_distance < following_distance) {
		//slow down proportionally depending on distance to next car
		//the closer we are to the car in the front, the faster we slow down
		currentCar->speed -= (DECEL * (following_distance / seperation_distance));
		//currentCar->speed -= DECEL*(following_distance-seperation_distance)/following_distance;
	}
	
	//This part of the model is used for normal following scenarios
	//if we are closer than 100 meters abs distace to carInFront then slow down
	//else if(distance_to_car_ahead < 100) {
		//slow down proportionally depending on distance to next car
		//the closer we are to the car in the front, the faster we slow down
		//currentCar->speed -= DECEL*(100-distance_to_car_ahead)/100; 
	//}
	
	//otherwise speed up
	else {
		//speed up based on driver aggressiveness. More aggressive drivers accelerate faster
		currentCar->speed  += (ACCEL + (currentCar-> aggressiveness  * AGGRESSIVENESS_EFFECT));
	}
	
	//dont allow negative speeds
	if(currentCar->speed < 0) {
		currentCar->speed = 0;
	}

	//dont allow the driver to accelerate above his personal speed limit
	if (currentCar->speed >= driver_speed_limit) {
		currentCar->speed = driver_speed_limit;
	}	
}
	

/**
 * This function updates the position of the current car
 * based on its current speed and direction
 * Y decreases when car travelling UP
 * Y increases when car travelling DOWN
 * X increases when car travelling RIGHT
 * X decreases when car travelling LEFT
 */
void update_car_position(Car *currentCar, float distance_to_car_ahead) {
	//speed in km/h divide 3.6 to get m/s
	//multiply by pixels/m to get pixels per second
	//divide by UPDATES_PER_SECOND to get the correct number of pixels to travel per tick
	float speed_pixels_tick = (currentCar->speed) / 3.6 * PIXELS_PER_M / UPDATES_PER_SECOND;
	
	//Test if we are about to move this car too close to the car in front
	//We must allow space for the length of the car when considering this limit
	if ((distance_to_car_ahead - speed_pixels_tick) < (MIN_DISTANCE_BETWEEN_CARS + CAR_LENGTH)) {
		
		//This message cannot be uncommented until some further bugs are worked out
		//Because cars spawn ontop of each other, this message fires as cars are
		//spawned. If the car following model is written correctly, it should never
		//print during normal operation of the simulation.
		//printf("Error in car following model!\n");
		
		//TODO this is only temporary untill spawing and models are improved
		//as a temporary solution we force the min distance seperation
		float max_move_this_tick = distance_to_car_ahead - (MIN_DISTANCE_BETWEEN_CARS + CAR_LENGTH);
		speed_pixels_tick = (max_move_this_tick > 0) ? max_move_this_tick : 0;
	}
	
	//update position of currentCar changing its current location
	//by the amount of pixels it has moved this tick
	//This is dependant on how fast it is travelling and 
	//also on the direction of the lane in which this car is in
	switch (all_lanes[currentCar->lane_id].direction) {
	case UP: //SN
		currentCar->location.y -= speed_pixels_tick;
		break;
	case DOWN: //NS
		currentCar->location.y += speed_pixels_tick;
		break;
	case LEFT: //EW
		currentCar->location.x -= speed_pixels_tick;
		break;
	case RIGHT: //WE
		currentCar->location.x += speed_pixels_tick;
		break;
	default:
		printf("\n\n\nERROR!!!! - invalid direction!!!\n\n\n");
		break;
	}
}

/**
 * Randomly chooses a turning direction if one has not already been 
 * chosen. When a turning direction is chosen, picked is set to true.
 * This prevents a turning direction from being chosen again.
 */
void update_car_turning_intention(Car *currentCar) {
	if (currentCar->picked) return;
	// generate random number between 0.0 and 1.0 (uniformly distributed)
	double random = ((double)rand() / (double)RAND_MAX);
	
	//set turning intention based on random number
	if (random > 0.5) currentCar->turn_intention = STRAIGHT;
	else currentCar->turn_intention = LEFT_TURN;

	currentCar->picked = true;
}

/**
 * This function tests whether a car has driven outside the limits of its lane
 * This is dependant on the direction of the lane in which this car is in
 * If the car is greater than the lane max or less than the lane min this
 * function will return true, indicating the car is outside its limits
 */
bool outside_lane_limits(Car *currentCar) {
	Point end_pos = all_lanes[currentCar->lane_id].end_pos;
	Point start_pos = all_lanes[currentCar->lane_id].start_pos;
	Point car_pos = currentCar->location;
	switch (all_lanes[currentCar->lane_id].direction) {
	case DOWN: //SN
		return (point_greater_than(car_pos, end_pos) || point_less_than(car_pos, start_pos));
		//return (car_pos.y > end_pos.y); //y is greater than
		break;
	case UP: //NS
		return (point_less_than(car_pos, end_pos) || point_greater_than(car_pos, start_pos));
		//return (car_pos.y < end_pos.y); //y is lesss than
		break;
	case LEFT: //EW
		return (point_less_than(car_pos, end_pos) || point_greater_than(car_pos, start_pos));
		//return (car_pos.x < end_pos.x); //x is less than
		break;
	case RIGHT: //WE
		return (point_greater_than(car_pos, end_pos) || point_less_than(car_pos, start_pos));
		//return (car_pos.x > end_pos.x); //x is greater than
		break;
	default:
		printf("\n\n\nERROR!!!! - invalid direction!!!\n\n\n");
		break;
	}
	return false;
}

/*============================ LANE PARAMETER DETECTION ===============================*/

/**
 * Tests whether car co-orindates of the currentCar match
 * the lane it is supposed to be in. This is the lane with
 * lane number <lane_id>. Prints an error message if there
 * is a mismatch.
 * This function serves no useful purpose other than error
 * checking. It can be removed if speed of the program
 * is important.
 */
void test_coordinate_correctness(int lane_id, Car *currentCar) {
	//the direction of the lane in which this car is in
	Direction lane_direction = all_lanes[lane_id].direction;
	
	//the end co-ordinates of the lane in which this car is in
	Point lane_end = all_lanes[lane_id].end_pos;
	
	if (lane_direction == WE) {
		//Here lane y-coord SHOULD ALWAYS equal car y-coord
		if (!indentical_y_coord(currentCar->location, lane_end)) {
			printf("ERROR. car_is_at_end_of_lane Co-orindate mismatch\n");
		}
	}
	
	if (lane_direction == EW) {
		//Here lane y-coord SHOULD ALWAYS equal car y-coord
		if (!indentical_y_coord(currentCar->location, lane_end)) {
			printf("ERROR. car_is_at_end_of_lane Co-orindate mismatch\n");
		}
	}
	
	if (lane_direction == SN) {
		//Here lane x-coord SHOULD ALWAYS equal car x-coord
		if (!indentical_x_coord(currentCar->location, lane_end)) {
			printf("ERROR. car_is_at_end_of_lane Co-orindate mismatch\n");
		}
	}
	
	if (lane_direction == NS) {
		//Here lane x-coord SHOULD ALWAYS equal car x-coord
		if (!indentical_x_coord(currentCar->location, lane_end)) {
			printf("ERROR. car_is_at_end_of_lane Co-orindate mismatch\n");
		}
	}
}

/**
 * Tests whether car has reached the end of its lane
 *
 * @return boolean indicating whether currentCar is at the
 *         end of lane with <lane_id>
 */
bool car_is_at_end_of_lane(int lane_id, Car *currentCar) {
	//the end co-ordinates of the lane in which this car is in
	Point lane_end = all_lanes[lane_id].end_pos;

	//if the direction of the lane in which this car is in is WE
	if (all_lanes[lane_id].direction == WE) {
		if (currentCar->location.x >= lane_end.x) return true;
		return false;
	}
	
	if (all_lanes[lane_id].direction == EW) {
		if (currentCar->location.x <= lane_end.x) return true;
		return false;
	}
	
	if (all_lanes[lane_id].direction == SN) {
		if (currentCar->location.y <= lane_end.y) return true;
		return false;
	}
	
	if (all_lanes[lane_id].direction == NS) {
		if (currentCar->location.y >= lane_end.y) return true;
	}
	return false;
}

/*============================ LANE MOVING ===============================*/

/**
 * This function works like a lookup table. Depending on the direction
 * of the lane and the turning direction of the car, a specfic lane
 * transition function is called.
 *
 * overlap contains data about how much car has overlapped its old lane
 * this is used to correct the position in the new lane
 * 
 * @return boolean indicating whether a car was moved to the next lane
 *         or not
 */
bool transition_car_to_new_lane(int lane_id, Car *currentCar, float overlap) {
	TurnIntention turn_intention = currentCar->turn_intention;
	
	if (lane_id == 0) { //Lane direction is WE
		if (turn_intention == STRAIGHT) return copy_car_to_new_lane(0, 1, overlap); //straight through from 0 is to 1
		if (turn_intention == LEFT_TURN) return copy_car_to_new_lane(0, 5, overlap); //left from 0 is to 5
		return false;
	}

	if (lane_id == 1) { //Lane direction is WE
		if (turn_intention == STRAIGHT) return copy_car_to_new_lane(1, 22, overlap); //straight through from 1 is to 22
		if (turn_intention == LEFT_TURN) return copy_car_to_new_lane(1, 16, overlap); //left from 1 is to 16
		return false;
	}
	
	if (lane_id == 2) { //Lane direction is EW
		if (turn_intention == STRAIGHT) return copy_car_to_new_lane(2, 3, overlap); //straight through from 2 is to 3
		if (turn_intention == LEFT_TURN) return copy_car_to_new_lane(2, 7, overlap); //left from 2 is to 7
		return false;
	}

	if (lane_id == 3) { //Lane direction is EW
		return false; //3 is an end lane
	}
	
	if (lane_id == 4) { //Lane direction is SN
		if (turn_intention == STRAIGHT) return copy_car_to_new_lane(4, 5, overlap); //straight through from 4 is to 5
		if (turn_intention == LEFT_TURN) return copy_car_to_new_lane(4, 3, overlap); //left from 4 is to 3
		return false;
	}

	if (lane_id == 5) { //Lane direction is SN
		return false; //5 is an end lane
	}
	
	if (lane_id == 6) { //Lane direction is NS
		if (turn_intention == STRAIGHT) return copy_car_to_new_lane(6, 7, overlap); //straight through from 6 is to 7
		if (turn_intention == LEFT_TURN) return copy_car_to_new_lane(6, 1, overlap); //left from 6 is to 1
		return false;
	}

	if (lane_id == 7) { //Lane direction is NS
		if (turn_intention == STRAIGHT) return copy_car_to_new_lane(7, 21, overlap); //straight through from 7 is to 21
		if (turn_intention == LEFT_TURN) return copy_car_to_new_lane(7, 9, overlap); //left from 7 is to 9
		return false;
	}

	if (lane_id == 8) { //Lane direction is WE
		if (turn_intention == STRAIGHT) return copy_car_to_new_lane(8, 9, overlap); //straight through from 8 is to 9
		if (turn_intention == LEFT_TURN) return copy_car_to_new_lane(8, 4, overlap); //left from 8 is to 4
		return false;
	}

	if (lane_id == 9) { //Lane direction is WE
		if (turn_intention == STRAIGHT) return copy_car_to_new_lane(9, 10, overlap); //straight through from 9 is to 10
		if (turn_intention == LEFT_TURN) return copy_car_to_new_lane(9, 15, overlap); //left from 9 is to 15
		return false;
	}
	
	if (lane_id == 10) { //Lane direction is WE
		return false; //10 is an end lane
	}

	if (lane_id == 11) { //Lane direction is EW
		if (turn_intention == STRAIGHT) return copy_car_to_new_lane(11, 12, overlap); //straight through from 11 is to 12
		if (turn_intention == LEFT_TURN) return copy_car_to_new_lane(11, 19, overlap); //left from 11 is to 19
		return false;
	}
	
	if (lane_id == 12) { //Lane direction is EW
		if (turn_intention == STRAIGHT) return copy_car_to_new_lane(12, 13, overlap); //straight through from 12 is to 13
		if (turn_intention == LEFT_TURN) return copy_car_to_new_lane(12, 21, overlap); //left from 12 is to 21
		return false; //else its all good
	}

	if (lane_id == 13) { //Lane direction is EW
		return false; //13 is an end lane
	}
	
	if (lane_id == 14) { //Lane direction is SN
		if (turn_intention == STRAIGHT) return copy_car_to_new_lane(14, 15, overlap); //straight through from 14 is to 15
		if (turn_intention == LEFT_TURN) return copy_car_to_new_lane(14, 12, overlap); //left from 14 is to 12
		return false;
	}

	if (lane_id == 15) { //Lane direction is SN
		if (turn_intention == STRAIGHT) return copy_car_to_new_lane(15, 16, overlap); //straight through from 15 is to 16
		if (turn_intention == LEFT_TURN) return copy_car_to_new_lane(15, 2, overlap); //left from 15 is to 2
		return false;
	}

	if (lane_id == 16) { //Lane direction is SN
		return false; //16 is an end lane
	}

	if (lane_id == 17) { //Lane direction is NS
		if (turn_intention == STRAIGHT) return copy_car_to_new_lane(17, 18, overlap); //straight through from 17 is to 18
		if (turn_intention == LEFT_TURN) return copy_car_to_new_lane(17, 22, overlap); //left from 17 is to 22
		return false;
	}

	if (lane_id == 18) { //Lane direction is NS
		if (turn_intention == STRAIGHT) return copy_car_to_new_lane(18, 19, overlap); //straight through from 18 is to 19
		if (turn_intention == LEFT_TURN) return copy_car_to_new_lane(18, 10, overlap); //left from 18 is to 10
		return false;
	}
	
	if (lane_id == 19) { //Lane direction is NS
		return false; //19 is an end lane
	}

	if (lane_id == 20) { //Lane direction is SN
		if (turn_intention == STRAIGHT) return copy_car_to_new_lane(20, 4, overlap); //straight through from 20 is to 4
		if (turn_intention == LEFT_TURN) return copy_car_to_new_lane(20, 13, overlap); //left from 20 is to 13
		return false;
	}

	if (lane_id == 21) { //Lane direction is NS
		return false; //21 is an end lane
	}

	if (lane_id == 22) { //Lane direction is WE
		return false; //22 is an end lane
	}
	
	if (lane_id == 23) { //Lane direction is EW
		if (turn_intention == STRAIGHT) return copy_car_to_new_lane(23, 2, overlap); //straight through from 23 is to 2
		if (turn_intention == LEFT_TURN) return copy_car_to_new_lane(23, 18, overlap); //left from 23 is to 18
	}
	return false;
}


/*================================ TURNING and TRANSITION FUNCTIONS ===================================*/

/**
 * copies the car out of its current lane array and into its new lane array
 * This function should copy a car from the front of the old lane into the
 * back of the new lane
 * TODO: make cases for if orientation of current lane & next lane is different
 */
bool copy_car_to_new_lane(int current_lane_id, int next_lane_id, float overlap) {
	//Check to see if the new lane actually has space for another car
	//Two spare slots, just like with vehicle spawning are to allow one space for the invisible car
	//and on space always free for correct pointer operation
	if (all_lanes[next_lane_id].count >= (MAX_CARS_PER_LANE-2)) {
		printf("\n\n\nError: Lane %d full - unable to move car to new lane\n\n\n", next_lane_id);
		return false; //this will cause the car to be deleted from its current lane
	}

	//move car from front of <current_lane_id> to the back of <next_lane_id>
	int current_lane_start_index = all_lanes[current_lane_id].start_index;
	int next_lane_end_index = all_lanes[next_lane_id].end_index;
	
	//this is the car to be moved
	Car *currentCar = &(all_lanes[current_lane_id].cars[current_lane_start_index]);
	//this is the open slot in the next lane
	Car *nextCar = &(all_lanes[next_lane_id].cars[next_lane_end_index]);

	//set the lane_id of the "new car" to the lane it is about to be created in
	nextCar->lane_id = next_lane_id;
	//Preserve turning intention untill after we have found cars new location
	nextCar->turn_intention = currentCar->turn_intention;

	//Set the co-ordinates for the new car to the correct location for its lane transition
	adjust_transition_location(nextCar, currentCar, overlap);
	
	//Copy some of the specs of the "old car" over to the "new car"
	strcpy(nextCar->plate, currentCar->plate); //copy its licence plate
	
	//cars speed doesnt change as it moves over intersection (maybe it does change in future)
	nextCar->speed = currentCar->speed;
	//red and orange lights are reset during lane change
	nextCar->red = false;
	nextCar->orange = false;
	nextCar->invisible = currentCar->invisible; //This is bit strange. Whats going on here?
	
	//The car has just arrived into this new lane. Its turning direction is therefore straight
	//If he wants to turn, he will decide to do this some way further down the road
	nextCar->turn_intention = STRAIGHT;
	nextCar->picked = false; //he has not picked turning direction yet
	
	//change lane data to show a new car has been added
	add_car_to_rear_of_lane(next_lane_id); //end_index now points to the next free space
	
	//finally the "old car" should be removed from its original lane
	remove_car_from_front_of_lane(current_lane_id, false); //regular car
	
	//print debug message if set
	if (DEBUG >= 3) {
		if (currentCar->turn_intention == LEFT_TURN) {
			printf("Moved car from lane %d to new lane %d. LEFT_TURN\n", current_lane_id, next_lane_id);
		}
		else {
			printf("Moved car from lane %d to new lane %d. STRAIGHT\n", current_lane_id, next_lane_id);
		}
	}

	return true;
}


/**
 * This function deals with the physical location of the car as it moves lanes
 * The direction of its turn will influence how it will peform this co-ordinate
 * update.
 *
 * Here currentCar is the car that has just been created in the new lane
 * oldCar is the car from the previous lane, it will be deleted.
 */
void adjust_transition_location(Car *currentCar, Car *oldCar, float overlap) {
	int lane_id = currentCar->lane_id;
	if (currentCar->turn_intention == STRAIGHT) {
		set_position_straight(currentCar, oldCar, lane_id);
		return;
	}
	if (currentCar->turn_intention == LEFT_TURN) {
		set_position_left_turn(currentCar, lane_id, overlap);
		return;
	}
	if (currentCar->turn_intention == RIGHT_TURN) {
		//not supported yet
		return;
	}
}


/**
 * If the car has performed a left turn, it should be placed at the start
 * point of the new lane.
 *
 * If the car has "overlapped" the edge of its old lane, it will have
 * this overlap value added to its spawn point on the new lane
 *
 * TODO: check that these are adding or subtracting in the correct
 * direction
 */
void set_position_left_turn(Car *currentCar, int new_lane_id, float overlap) {
	currentCar->location.x = all_lanes[new_lane_id].start_pos.x;
	currentCar->location.y = all_lanes[new_lane_id].start_pos.y;

	//update position of currentCar by changing its current location by the
	// amount it has overlapped. This is dependant on the direction of the
	// lane in which this car is in
	switch (all_lanes[new_lane_id].direction) { //direction of the lane which this car has just moved into
	case UP: //SN
		currentCar->location.y -= overlap;
		break;
	case DOWN: //NS
		currentCar->location.y += overlap;
		break;
	case LEFT: //EW
		currentCar->location.x -= overlap;
		break;
	case RIGHT: //WE
		currentCar->location.x += overlap;
		break;
	default:
		printf("\n\n\nERROR!!!! - invalid direction!!!\n\n\n");
		break;
	}
}

/**
 * If the car is going straight, it would "teleport" were it started
 * at the beginning of the new lane. To solve this problem we are
 * going to set its new co-ordinates to exactly the same as its old
 * co-ordinates.
 * 
 * This should cause no problems because co-ordinates for for cars
 * in the lane are not directly connected with the lane. Thus the 
 * car can drive in "no-mans land" while it crosses the intersection.
 * It should then be inside the next lanes co-ordinates after this.
 *
 * There is no overlap for this transition type because the car
 * does not have a sudden "change" in its co-ordinates.
 * There is a check for correct co-ordinate system use however.
 */
void set_position_straight(Car *currentCar, Car *oldCar, int new_lane_id) {
	currentCar->location.x = oldCar->location.x;
	currentCar->location.y = oldCar->location.y;
	//direction of the lane which this car has just moved into
	switch (all_lanes[new_lane_id].direction) {
	case UP: //SN
		if (currentCar->location.x != all_lanes[new_lane_id].start_pos.x) {
			printf("set_position_straight - Error with lane-cordinates - SN!\n");
		}
		break;
	case DOWN: //NS
		if (currentCar->location.x != all_lanes[new_lane_id].start_pos.x) {
			printf("set_position_straight - Error with lane-cordinates - NS!\n");
		}
		break;
	case LEFT: //EW
		if (currentCar->location.y != all_lanes[new_lane_id].start_pos.y) {
			printf("set_position_straight - Error with lane-cordinates - EW!\n");
		}
		break;
	case RIGHT: //WE
		if (currentCar->location.y != all_lanes[new_lane_id].start_pos.y) {
			printf("set_position_straight - Error with lane-cordinates - WE!\n");
		}
		break;
	default:
		printf("\n\n\nERROR!!!! - invalid direction!!!\n\n\n");
		break;
	}
}
	

/*============================ PLATE GENERATION ===============================*/

/**
 * Checks whether a plate is unique from the list of all generated plates
 * Returns false is a match is found (plate is NOT uniqe)
 * Returns true if the plate is unique.
 */
bool plate_is_unique(char* plate, int total_cars) {
	int i;
	for (i = 0; i < total_cars; i++) {
		if (strcmp(plate, old_plates[i]) == 0) {
			if (DEBUG >= 2) {
				printf("Found matching plate in database: \"%s\"\n", plate);
			}
			return false;
		}
	}
	return true;
}

//this function generates a car plate string, starting at the given char pointer
void generatecarplate(char* plate) {
	static unsigned int total_cars = 0;
	int i,j; //temp variables
	char h;
	
	if (total_cars >= (MAX_PLATES-1)) {
		printf("\n\n\nERROR: Plates array is full!!\n\n\n");
		return;
	}
	
	//Use remainder 26 to generate numbers between 0 and 25
	for (i=0; i<3; i++) { //generate 3 letters
		j=rand()%26;
		//pick a random letter from the list of available letters
		plate[i] = ALPHABET[j];
	}
	
	//generate 3 numbers
	for (i=3; i<6; i++) {
		j=rand()%10;
		h=(char)(((int)'0')+j); 
		plate[i] = h;
	}	
	plate[6]='\0'; //null character terminator EOS
	
	//Debug message about plate creation. This only occurs on DEBUG 3 or higher
	if (DEBUG >= 4) {
		printf("Created plate: \"%s\"\n", plate);
	}
	
	//If this plate is unique, add it to the old_plates array
	if (plate_is_unique(plate, total_cars)) {
		//allocate new memory in the old plates array.
		old_plates[total_cars]=(char*) malloc(7*sizeof(char));
		//copy the newly created plate into the array
		strncpy(old_plates[total_cars], plate,7);
		total_cars++;
		//for (i = 0; i < total_cars; i++) {
		//	printf("Plate %d: \"%s\"\n", i, old_plates[i]);
		//}
		return;
	}
	//else try to create another plate
	generatecarplate(plate);
	return;
	
}

/*============================ DISTANCE FUNCTIONS ===============================*/

/**
 * This function finds the distance to the car ahead by looking in the next
 * lane. The next lane is defined as the lane that the currentCar is going
 * to turn into.
 *
 * Distance to the car in front is made of the sum of
 * 		distance from rear car in next lane to the start of that lane
 * 		distance from this car to the end of this lane
 * 			if straight - length of intersection
 *			if left 0
 *
 */
float distance_to_car_ahead_next_lane(float distance_lane_end, int lane_id, Car *currentCar) {
	//distance available in next lane
	float dainl = distance_available_in_next_lane(lane_id, currentCar);

	//if car is turning left
	if (currentCar->turn_intention == LEFT_TURN) {
		return distance_lane_end + dainl;
	}

	//if car is going straight, there is the length of the intersection to consider
	//according to infrastructure.c, the distance between lanes is actually only half
	//of the intersection width.
	if (currentCar->turn_intention == STRAIGHT) {
		return distance_lane_end + dainl + (intersection_width / 2);
		//return distance_lane_end + dainl;
	}

	printf("\n\n\nERROR - Unsupport direction!!!\n\n\n");
	return 1000.0;
}


/**
 * This function finds the distance between the start of the next lane
 * and the rear most car in that lane.
 * This is useful to determine if there is enough physical space to move
 * another car into that lane.
 *
 * This function assumes that a turning direction has been chosen for
 * currentCar. The turning direction is needed to know which lane to
 * look in. In other words which lane is the "next lane".
 *
 * If there are no cars in the next lane or there is no next lane,
 * this function returns a distance of 1000.0
 */
float distance_available_in_next_lane(int lane_id, Car *currentCar) {
	//get the lane "in front"
	int next_lane_id = get_transition_lane_number(lane_id, currentCar);

	//if the turn direction is not valid, or there is no next lane
	if (next_lane_id == -1) {
		//Then distance available in next lane is 1000 pixels
		return 1000.0;
	}
	
	//if there are no cars in the next lane
	if (all_lanes[next_lane_id].count < 1) {
		//Then distance available in next lane is 1000 pixels
		return 1000.0;
	}
	
	//If we are here we know that there must be at least one car
	//in the next lane.
	//fetch the car at the rear of the of the next lane	

	//This is the end index of lane we will transition to
	int last_car_index = all_lanes[next_lane_id].end_index;
	//This now points to the last car in this lane
	decrement(last_car_index);

	Car *rear_car_next_lane = &(all_lanes[next_lane_id].cars[last_car_index]);
	
	//distance between start of next lane and the rear car in that lane
	return distance_to_start_of_lane(next_lane_id, rear_car_next_lane);
}


/**
 * This function works like a lookup table. Depending on the direction
 * of the lane and the turning direction of the car, a specfic lane
 * number is returned. This lane number is the lane number which the
 * currentCar will transition into (using its current TurnIntention)
 */
int get_transition_lane_number(int lane_id, Car *currentCar) {
	TurnIntention turn_intention = currentCar->turn_intention;
	
	if (lane_id == 0) { //Lane direction is WE
		if (turn_intention == STRAIGHT) return 1; //straight through from 0 is to 1
		if (turn_intention == LEFT_TURN) return 5; //left from 0 is to 5
		return -1;
	}

	if (lane_id == 1) { //Lane direction is WE
		if (turn_intention == STRAIGHT) return 22; //straight through from 1 is to 22
		if (turn_intention == LEFT_TURN) return 16; //left from 1 is to 16
		return -1;
	}
	
	if (lane_id == 2) { //Lane direction is EW
		if (turn_intention == STRAIGHT) return 3; //straight through from 2 is to 3
		if (turn_intention == LEFT_TURN) return 7; //left from 2 is to 7
		return -1;
	}

	if (lane_id == 3) { //Lane direction is EW
		return -1; //3 is an end lane
	}
	
	if (lane_id == 4) { //Lane direction is SN
		if (turn_intention == STRAIGHT) return 5; //straight through from 4 is to 5
		if (turn_intention == LEFT_TURN) return 3; //left from 4 is to 3
		return -1;
	}

	if (lane_id == 5) { //Lane direction is SN
		return -1; //5 is an end lane
	}
	
	if (lane_id == 6) { //Lane direction is NS
		if (turn_intention == STRAIGHT) return 7; //straight through from 6 is to 7
		if (turn_intention == LEFT_TURN) return 1; //left from 6 is to 1
		return -1;
	}

	if (lane_id == 7) { //Lane direction is NS
		if (turn_intention == STRAIGHT) return 21; //straight through from 7 is to 21
		if (turn_intention == LEFT_TURN) return 9; //left from 7 is to 9
		return -1;
	}

	if (lane_id == 8) { //Lane direction is WE
		if (turn_intention == STRAIGHT) return 9; //straight through from 8 is to 9
		if (turn_intention == LEFT_TURN) return 4; //left from 8 is to 4
		return -1;
	}

	if (lane_id == 9) { //Lane direction is WE
		if (turn_intention == STRAIGHT) return 10; //straight through from 9 is to 10
		if (turn_intention == LEFT_TURN) return 15; //left from 9 is to 15
		return -1;
	}
	
	if (lane_id == 10) { //Lane direction is WE
		return -1; //10 is an end lane
	}

	if (lane_id == 11) { //Lane direction is EW
		if (turn_intention == STRAIGHT) return 12; //straight through from 11 is to 12
		if (turn_intention == LEFT_TURN) return 19; //left from 11 is to 19
		return -1;
	}
	
	if (lane_id == 12) { //Lane direction is EW
		if (turn_intention == STRAIGHT) return 13; //straight through from 12 is to 13
		if (turn_intention == LEFT_TURN) return 21; //left from 12 is to 21
		return -1; //else its all good
	}

	if (lane_id == 13) { //Lane direction is EW
		return -1; //13 is an end lane
	}
	
	if (lane_id == 14) { //Lane direction is SN
		if (turn_intention == STRAIGHT) return 15; //straight through from 14 is to 15
		if (turn_intention == LEFT_TURN) return 12; //left from 14 is to 12
		return -1;
	}

	if (lane_id == 15) { //Lane direction is SN
		if (turn_intention == STRAIGHT) return 16; //straight through from 15 is to 16
		if (turn_intention == LEFT_TURN) return 2; //left from 15 is to 2
		return -1;
	}

	if (lane_id == 16) { //Lane direction is SN
		return -1; //16 is an end lane
	}

	if (lane_id == 17) { //Lane direction is NS
		if (turn_intention == STRAIGHT) return 18; //straight through from 17 is to 18
		if (turn_intention == LEFT_TURN) return 22; //left from 17 is to 22
		return -1;
	}

	if (lane_id == 18) { //Lane direction is NS
		if (turn_intention == STRAIGHT) return 19; //straight through from 18 is to 19
		if (turn_intention == LEFT_TURN) return 10; //left from 18 is to 10
		return -1;
	}
	
	if (lane_id == 19) { //Lane direction is NS
		return -1; //19 is an end lane
	}

	if (lane_id == 20) { //Lane direction is SN
		if (turn_intention == STRAIGHT) return 4; //straight through from 20 is to 4
		if (turn_intention == LEFT_TURN) return 13; //left from 20 is to 13
		return -1;
	}

	if (lane_id == 21) { //Lane direction is NS
		return -1; //21 is an end lane
	}

	if (lane_id == 22) { //Lane direction is WE
		return -1; //22 is an end lane
	}
	
	if (lane_id == 23) { //Lane direction is EW
		if (turn_intention == STRAIGHT) return 2; //straight through from 23 is to 2
		if (turn_intention == LEFT_TURN) return 18; //left from 23 is to 18
	}
	return -1;
}

/**
 * Calculates the distance in pixels between the front car and the next
 * car behind it in the array.
 * 
 * Note: Cars are represented at having a single point location
 * When these cars are drawn in the simulation they actually have length
 * and breadth. This function does not take into account length and breadth
 * of the car. It simply returns the point differences betwen the front
 * car location and the car behind it location.
 *
 * If the car in front is actually behind the current car, then this
 * calculation will return a negative number!
 */
float distance_between_front_cars(int lane_number) {
	//distance between front cars set to 1000 if there arent front cars
	if (all_lanes[lane_number].count < 2) return 1000;
	
	int front_index = all_lanes[lane_number].start_index;
	int behind_index = front_index;
	increment(behind_index); //next car after the front car

	Car *front_car = &(all_lanes[lane_number].cars[front_index]);
	Car *behind_car = &(all_lanes[lane_number].cars[behind_index]);
	
	return distance_to_car_in_front(behind_car, front_car);
}


/**
 * Calculates the distance in pixels to the car directly in front
 * of the current car. The car in front is the next ahead in the array.
 * 
 * Note: Cars are represented at having a single point location
 * When these cars are drawn in the simulation they actually have length
 * and breadth. This function does not take into account length and breadth
 * of the car and simply returns the difference in pixels between the 
 * two points representing the the two cars passed as arguments to this
 * function.
 *
 * If the car in front is actually behind the current car, then this
 * calculation will return a negative number!
 */
float distance_to_car_in_front(Car *currentCar, Car *carInFront) {
	float distance_to_car_ahead;
	//the direction of the lane in which this car is in
	Direction current_car_direction = all_lanes[currentCar->lane_id].direction;
	
	//compute the distance to the next car based on lane direction & appropriate xy coords
	//this distance is in pixels and should always be positive
	switch(current_car_direction) {
		case UP: //SN
			//car ahead should have a smaller y than current car
			distance_to_car_ahead = currentCar->location.y - carInFront->location.y;
			break;
		case DOWN:
			//car ahead should have a larger y than current car
			distance_to_car_ahead = carInFront->location.y - currentCar->location.y;
			break;                                  
		case LEFT:
			//car ahead should have a smaller x than current car
			distance_to_car_ahead = currentCar->location.x - carInFront->location.x;
			break;
		case RIGHT:
			//car ahead should have a larger x than current car
			distance_to_car_ahead = carInFront->location.x - currentCar->location.x;
			break;
		default:
			distance_to_car_ahead = 40; //40 pixels
			printf("\n\n\nERROR!!!! - invalid direction!!!\n\n\n");
			break;
	}
	return distance_to_car_ahead;
}

/**
 * Gets the distance of currentCar from the start of the lane
 * with lane number <lane_id>. Distance to the start of the
 * lane is in pixels.
 */
float distance_to_start_of_lane(int lane_id, Car *currentCar) {
	//the start co-ordinates of the lane in which this car is in
	Point lane_start = all_lanes[lane_id].start_pos;
	
	//if the direction of the lane in which this car is in is WE
	if (all_lanes[lane_id].direction == WE) {
		//then car x should be greater than start x
		return (currentCar->location.x - lane_start.x);
	}
	
	if (all_lanes[lane_id].direction == EW) {
		return (lane_start.x - currentCar->location.x);
	}
	
	if (all_lanes[lane_id].direction == SN) {
		return (lane_start.y - currentCar->location.y);
	}
	
	if (all_lanes[lane_id].direction == NS) {
		return (currentCar->location.y - lane_start.y);
	}
	printf("\n\n\nERROR!!!! - invalid direction!!!\n\n\n");
	return 0;
}

/**
 * Gets the distance of currentCar from the end of the lane
 * with lane number <lane_id>. Distance to the end of the
 * lane is in pixels.
 */
float distance_to_end_of_lane(int lane_id, Car *currentCar) {
	//the end co-ordinates of the lane in which this car is in
	Point lane_end = all_lanes[lane_id].end_pos;
	
	//if the direction of the lane in which this car is in is WE
	if (all_lanes[lane_id].direction == WE) {
		//then lane x should be greater than car x
		return (lane_end.x - currentCar->location.x);
	}
	
	if (all_lanes[lane_id].direction == EW) {
		return (currentCar->location.x - lane_end.x);
	}
	
	if (all_lanes[lane_id].direction == SN) {
		return (currentCar->location.y - lane_end.y);
	}
	
	if (all_lanes[lane_id].direction == NS) {
		return (lane_end.y - currentCar->location.y);
	}
	printf("\n\n\nERROR!!!! - invalid direction!!!\n\n\n");
	return 0;
}


