#ifndef VTL_INDEX_H
#define VTL_INDEX_H

#include "globals.h"

/**
 * @author: Devin Barry
 *
 * index.h contains the function prototypes for the functions
 * from index.c
 */


/*------------Function Prototypes-----------*/

void add_car_to_front_of_lane(int lane_number);
void remove_car_from_front_of_lane(int lane_number, bool invisible);
int add_car_to_rear_of_lane(int lane_number);
void remove_car_from_rear_of_lane(int lane_number);

void increment_end_index(int lane_number);
void decrement_end_index(int lane_number);
void increment_start_index(int lane_number);
void decrement_start_index(int lane_number);

/*----------End Function Prototypes----------*/

#endif
