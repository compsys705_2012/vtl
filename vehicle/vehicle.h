#ifndef VTL_VEHICLE_H
#define VTL_VEHICLE_H

#include "globals.h"

/*------------Global Variables for Vehicles -----------*/

#define MIN_DISTANCE_BETWEEN_CARS 20 //this is in pixels


/*------------Function Prototypes-----------*/

void init_vehicle();
void update_vehicle();

void update_car_lane(Lane *current_car_lane);
void car_following_model(Car *currentCar, float distance_to_car_ahead);

void update_car_position(Car *currentCar, float distance_to_car_ahead);
void update_car_speed(Car *currentCar, float distance_to_car_ahead);
void update_car_turning_intention(Car *currentCar);

bool outside_lane_limits(Car *currentCar);

void test_coordinate_correctness(int lane_id, Car *currentCar);
bool car_is_at_end_of_lane(int lane_id, Car *currentCar);

bool transition_car_to_new_lane(int lane_id, Car *currentCar, float overlap);
bool copy_car_to_new_lane(int current_lane_id, int next_lane_id, float overlap);
void adjust_transition_location(Car *currentCar, Car *oldCar, float overlap);
void set_position_left_turn(Car *currentCar, int new_lane_id, float overlap);
void set_position_straight(Car *currentCar, Car *oldCar, int new_lane_id);

void generatecarplate(char* plate);
bool plate_is_unique(char* plate, int total_cars);

float distance_to_car_ahead_next_lane(float distance_lane_end, int lane_id, Car *currentCar);
float distance_available_in_next_lane(int lane_id, Car *currentCar);
int get_transition_lane_number(int lane_id, Car *currentCar);
float distance_between_front_cars(int lane_number);
float distance_to_car_in_front(Car *currentCar, Car *carInFront);
float distance_to_start_of_lane(int lane_id, Car *currentCar);
float distance_to_end_of_lane(int lane_id, Car *currentCar);

/*----------End Function Prototypes----------*/

#endif

