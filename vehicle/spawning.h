#ifndef VTL_SPAWNING_H
#define VTL_SPAWNING_H

#include "globals.h"

/*------------Function Prototypes-----------*/

void init_prng();
void init_all_cars_in_all_lanes();

void create_simulation_cars();
void create_cars_in_lane(int lane_number, int num_cars);

void spawn_car_based_upon_lane_density(int lane_number);
bool safe_spawn_one_car(int lane_number);

void create_regular_car(int lane_number);
void remove_regular_car(Car *currentCar);

bool create_invisible_car(Lane *current_lane);
void spawn_invisible_car(int lane_number);
void remove_invisible_car(Lane *current_lane);

/*----------End Function Prototypes----------*/


#define INVIS_OFFSET ((CAR_LENGTH + MIN_DISTANCE_BETWEEN_CARS) - (CAR_LENGTH/2 + intersection_width/4))

//determines the spread of car creation during spawning
//higher values will cause more "randomness", lower values a more even spread
#define MAX_SPAWN 10.0

#endif

