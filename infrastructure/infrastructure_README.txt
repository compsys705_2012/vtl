----------------------------------------------------------------------------------------
 VTL INFRASTRUCTURE
----------------------------------------------------------------------------------------
This directory contains the infrastructure implementation as well as verification attempt using
the model checker BLAST.

See infrastructure.h for details on the implementation, as well as the report.

Work done by group members:

Shikhsa Sridhar
- Traffic light implementation and verification: init_traffic_lights(), draw_traffic_lights(), all the traffic light verification files.
- Group coding efforts

Jamie Lin
- Arrow and GoZone implementation and verification: init_box(), create_arrows(), void addPre(int num), create_vertical(), create_horizontal(), all the arrow verification files.
- Group coding efforts

Sean Marriott
- Lane structure implementation and verification: create_intersections(), init_lanes()
- Group coding efforts
- Initial integration of all groups
