#ifndef VTL_INFRASTRUCTURE_H
#define VTL_INFRASTRUCTURE_H

/*
----------------------------------------------------------------------------------------
Functions related to the infrastructure of VTL, such as the creation of roads, traffic lights and
intersections.
----------------------------------------------------------------------------------------
The layout of the lanes and their data structures have three different options.
1. Hardcoded 1-intersection model.
    This was the first model developed and uses 8 lanes.
2. Hardcode 4-intersection model.
    This was included in the final implementation and uses 24 lanes.
3. Arrow intersection model.
    Lanes are constructed as a sequence of arrows connected head to tail. Arrows
    are considered an intersection when more than two arrow tails or heads connect at the same spot.
    The arrow solution is currently in the program, however the cars and VTL controller follow the
    hardcoded 4-intersection model. Arrows can be generated either manually through the GUI interface by right
    clicking and dragging arrows, or automatically by simply running the simulation. When automatic arrows are generated
    in the create_arrows function, they are generated according to the "+" shape intersection only. The arrow generated roads
    are either horizontal or vertical, according to how many intersections were spawned by the GUI.
    Curved roads can only be achieved by manual clicking from the GUI.

GoZones are also created in this infrastructure model, which is an array of rectangular areas which defines
valid areas of the road. Non-functional feature used for verification purposes.       

===================================================================
Arrow algorithm (automatic generation)

loop through intersections[]
{
    if (north of intersection arrows created == false)
        create arrows north of intersection
    if (east of intersection arrows created == false)
        create arrows east of intersection
    if (south of intersection arrows created == false)
        create arrows south of intersection
    if (west of intersection arrows created == false)
        create arrows west of intersection
}
===================================================================                         
*/

#include <gtk/gtk.h>
#include "globals.h"

// Initialize function called by GUI, when simulation starts
void init_infrastructure();

// Update function called by GUI, when simulation is running
void update_infrastructure();

// GoZone creation
void init_box(Point* centre);

// Create all traffic lights
void init_traffic_lights();

// Draw method called in gui.c
void draw_traffic_light(cairo_t *cr, int i);

// Create the hardcoded intersections, using either the 1 or 4 intersection model.
void init_lanes();

// Initialize the lane data structure
void create_Intersections();

// Initialize and create the arrow model.
void create_arrows();

// Used in create_arrows
void addPre(int num);
void create_vertical(Point start, Point end);
void create_horizontal(Point start, Point end);

// The four points in the hardcoded 4-intersection model.
extern Point* topLeft;
extern Point* topRight;
extern Point* botLeft;
extern Point* botRight;

#endif
