#include "globals.h"

//----------------------------------------------------------------------------------------
// GoZone creation
//----------------------------------------------------------------------------------------
void init_box(Point* centre) {
    /*
	float xOne = centre->x - (intersection_width/4) ;
	float yOne = MAP_WIDTH/2;
	Point pOne = {xOne, yOne};
	Box box_SN = {.center = pOne, .width = (int)intersection_width/2, .height = MAP_HEIGHT, .direction = SN};

	float xTwo = centre->x + (intersection_width/4) ;
	float yTwo = MAP_WIDTH/2;
	Point pTwo = {xTwo, yTwo};
	Box box_NS = {pTwo, intersection_width/2, MAP_WIDTH, NS};

	float xThree = MAP_WIDTH/2;
	float yThree = centre->y - (intersection_height/4) ;
	Point pThree = {xThree, yThree};
	Box box_EW = {pThree, MAP_WIDTH, intersection_height/2, EW};

	float xFour = MAP_WIDTH/2;
	float yFour = centre->y + (intersection_height/4) ;
	Point pFour = {xFour, yFour};
	Box box_WE = {pFour, MAP_WIDTH, intersection_height/2, WE};
	
	goZone[0] = box_SN;
	goZone[1] = box_NS;
	goZone[2] = box_EW;
	goZone[3] = box_WE; 
	*/
	
	max_goZone = number_of_intersections * 2;
	goZone = (Box*) malloc(max_goZone * sizeof(Box));
	
	float x;
	float y;
	bool found = false;
	
	for(int i=0; i<number_of_intersections; i++) {
	    if(goZone_size == 0) {
	        x = intersections[i].origin.x;
	        y = MAP_HEIGHT/2;
	        Point ptOne = {x, y};
	        Box boxOne = {ptOne, intersection_width, MAP_HEIGHT};
	        goZone[0] = boxOne;
	        goZone_size += 1;
	        
	        x = MAP_WIDTH/2;
	        y = intersections[i].origin.y;
	        Point ptTwo = {x, y};
	        Box boxTwo = {ptTwo, MAP_WIDTH, intersection_height};
	        goZone[1] = boxTwo;
	        goZone_size += 1;
	    }
	    else
	    {
	        found = false;
	        x = intersections[i].origin.x;
	        y = MAP_HEIGHT/2;
	        for(int j=0; j<goZone_size; j++) {
	            if(x==goZone[j].center.x && y==goZone[j].center.y) {
	                found = true;
	            }
	        }
	        if(found) {
	            Point ptOne = {x, y};
	            Box boxOne = {ptOne, intersection_width, MAP_HEIGHT};
	            goZone[goZone_size] = boxOne;
	            goZone_size += 1;
	        }
	        
	        found = false;
	        x = MAP_WIDTH/2;
	        y = intersections[i].origin.y;
	        for(int j=0; j<goZone_size; j++) {
	            if(x==goZone[j].center.x && y==goZone[j].center.y) {
	                found = true;
	            }
	        }
	        if(found) {
	            Point ptTwo = {x, y};
	            Box boxTwo = {ptTwo, MAP_WIDTH, intersection_height};
	            goZone[goZone_size] = boxTwo;
	            goZone_size += 1;
	        } 
	    }
	}
}

//----------------------------------------------------------------------------------------
// Spawns traffic lights around each intersection.
// Traffic lights are stored in the intersectionLights array, which is a global
//----------------------------------------------------------------------------------------
void init_traffic_lights()
{
    int margin;

    margin = ((0.5 * (int)intersection_width) + 50);

    intersectionLights = (IntersectionLights*) malloc(number_of_intersections * sizeof(IntersectionLights));

    //Initialise position of traffic light
    for (int n = 0; n < number_of_intersections; ++n)
    {
        
        
        intersectionLights[n].light[SN].xPos = intersections[n].origin.x - margin + 20;
        intersectionLights[n].light[SN].yPos = intersections[n].origin.y - margin + 250;
		intersectionLights[n].light[NS].xPos = intersections[n].origin.x + margin - 20;
        intersectionLights[n].light[NS].yPos = intersections[n].origin.y + margin - 250;
        
        intersectionLights[n].light[EW].xPos = intersections[n].origin.x + margin + 50;
        intersectionLights[n].light[EW].yPos = intersections[n].origin.y + margin - 20;
		intersectionLights[n].light[WE].xPos = intersections[n].origin.x - margin - 50;
        intersectionLights[n].light[WE].yPos = intersections[n].origin.y - margin + 20;
        
        //Initialise state of traffic light
        intersectionLights[n].light[SN].state = GREEN;
        intersectionLights[n].light[NS].state = GREEN;
        intersectionLights[n].light[EW].state = GREEN;
        intersectionLights[n].light[WE].state = GREEN;
    }
}

void draw_traffic_arrow(cairo_t *cr, float x, float y, Direction d) {
    cairo_set_line_width(cr, 8);

    const float arrow_size = 50;

    switch(d)
    {
        case SN:
            cairo_move_to(cr, x, y + arrow_size);
            break;
        case NS:
            cairo_move_to(cr, x, y - arrow_size);
            break;
        case EW:
            cairo_move_to(cr, x + arrow_size, y);
            break;
        case WE:
            cairo_move_to(cr, x - arrow_size, y);
            break;
    }

    cairo_line_to(cr, x, y);
    switch(d)
    {
        case SN:
            cairo_line_to(cr, x + arrow_size / 3.5, y + arrow_size / 3.5);
            break;
        case NS:
            cairo_line_to(cr, x + arrow_size / 3.5, y - arrow_size / 3.5);
            break;
        case EW:
            cairo_line_to(cr, x + arrow_size / 3.5, y + arrow_size / 3.5);
            break;
        case WE:
            cairo_line_to(cr, x - arrow_size / 3.5, y + arrow_size / 3.5);
            break;
    }

    cairo_stroke(cr);
    cairo_move_to(cr, x, y);

    switch(d)
    {
        case SN:
            cairo_line_to(cr, x - arrow_size / 3.5, y + arrow_size / 3.5);
            break;
        case NS:
            cairo_line_to(cr, x - arrow_size / 3.5, y - arrow_size / 3.5);
            break;
        case EW:
            cairo_line_to(cr, x + arrow_size / 3.5, y - arrow_size / 3.5);
            break;
        case WE:
            cairo_line_to(cr, x - arrow_size / 3.5, y - arrow_size / 3.5);
            break;
    }

    cairo_stroke(cr);

}

//----------------------------------------------------------------------------------------
// Draw method for traffic lights called in gui.c
//----------------------------------------------------------------------------------------
void draw_traffic_light(cairo_t *cr, int i)
{
    switch (intersectionLights[i].light[SN].state)
    {
    case RED:
        cairo_set_source_rgb(cr, 1, 0, 0);
        break;
    case ORANGE:
        cairo_set_source_rgb(cr, 1, 0.5, 0);
        break;
    case GREEN:
        cairo_set_source_rgb(cr, 0, 1, 0);
        break;
    }

    draw_traffic_arrow(cr, intersectionLights[i].light[SN].xPos, intersectionLights[i].light[SN].yPos, SN);

    switch (intersectionLights[i].light[NS].state)
    {
    case RED:
        cairo_set_source_rgb(cr, 1, 0, 0);
        break;
    case ORANGE:
        cairo_set_source_rgb(cr, 1, 0.5, 0);
        break;
    case GREEN:
        cairo_set_source_rgb(cr, 0, 1, 0);
        break;
    }
    
	draw_traffic_arrow (cr, intersectionLights[i].light[NS].xPos, intersectionLights[i].light[NS].yPos, NS);
    
    switch (intersectionLights[i].light[EW].state)
    {
    case RED:
        cairo_set_source_rgb(cr, 1, 0, 0);
        break;
    case ORANGE:
        cairo_set_source_rgb(cr, 1, 0.5, 0);
        break;
    case GREEN:
        cairo_set_source_rgb(cr, 0, 1, 0);
        break;
    }

    draw_traffic_arrow(cr, intersectionLights[i].light[EW].xPos, intersectionLights[i].light[EW].yPos, EW);
    
    switch (intersectionLights[i].light[WE].state)
    {
    case RED:
        cairo_set_source_rgb(cr, 1, 0, 0);
        break;
    case ORANGE:
        cairo_set_source_rgb(cr, 1, 0.5, 0);
        break;
    case GREEN:
        cairo_set_source_rgb(cr, 0, 1, 0);
        break;
    }

    draw_traffic_arrow(cr, intersectionLights[i].light[WE].xPos, intersectionLights[i].light[WE].yPos, WE);
}

//----------------------------------------------------------------------------------------
// Creates the hardcoded lane model, which can either be the 1-intersection model or the 4-intersection model.
// If the function cannot find 4 points that adhere to the 4-intersection model, then the function will revert
// to the 1-intersection model.
// The 1-intersection model is defined in lane_info_old.txt
// The 4-intersection model, which is used by the vehicle and VTL controller segments, is defined in lane_info_four_intersections.txt
//----------------------------------------------------------------------------------------
void init_lanes()
{
    // Check for the four valid intersections. If found the 4-intersection model will be used.
	bool fourIntersections = false;

	//lane_ids for each lane are really important for vehicle group
	//they need to be assigned by infrastructure when new lanes are created
	//this is the way that we know how to move cars between lanes
	//please follow id format that is laid out in <lane_info_old.txt> or <lane_info_four_intersections.txt>
	for (int i = 0; i < NUM_OF_LANES; ++i)
	{
	    all_lanes[i].lane_id = i;
	}
    
    int minX = 4000;
    int minY = 4000;

    // Find the most top left intersection
    for (int n = 0; n < number_of_intersections; ++n)
    {
        if (intersections[n].origin.x < minX && intersections[n].origin.y < minY)
        {
            printf("Infrastructure: Found top left (%f,%f)\n", intersections[n].origin.x, intersections[n].origin.y);
            fourIntersections = true;
            topLeft = &intersections[n].origin;
            minX = intersections[n].origin.x;
            minY = intersections[n].origin.y;
        }
    }
    
    // Find the topRight intersection
    if (fourIntersections == true)
    {
        fourIntersections = false;
        minX = 4000;
        for (int n = 0; n < number_of_intersections; ++n)
        {
            if (intersections[n].origin.x < minX && intersections[n].origin.x > topLeft->x && intersections[n].origin.y == topLeft->y)
            {
                printf("Infrastructure: Found top right (%f,%f)\n", intersections[n].origin.x, intersections[n].origin.y);
                fourIntersections = true;
                topRight = &intersections[n].origin;
            }
        }
    }
    
    // Find the bottomLeft intersection
    if (fourIntersections == true)
    {
        fourIntersections = false;
        minY = 4000;
        for (int n = 0; n < number_of_intersections; ++n)
        {
            if (intersections[n].origin.y < minY && intersections[n].origin.y > topLeft->y && intersections[n].origin.x == topLeft->x)
            {
                printf("Infrastructure: Found bot left (%f,%f)\n", intersections[n].origin.x, intersections[n].origin.y);
                fourIntersections = true;
                botLeft = &intersections[n].origin;
            }
        }
    }
    
    // Find the bottomRight intersection
    if (fourIntersections == true)
    {
        fourIntersections = false;
        for (int n = 0; n < number_of_intersections; ++n)
        {
            if (intersections[n].origin.x == topRight->x && intersections[n].origin.y == botLeft->y)
            {
                printf("Infrastructure: Found bot right (%f,%f)\n", intersections[n].origin.x, intersections[n].origin.y);
                fourIntersections = true;
                botRight = &intersections[n].origin;
            }
        }
    }

    // The 1-intersection model uses the first intersection created.
    Point* centre = &intersections[0].origin;

    // Construct the original 1 intersection model, if we can't find a four intersection structure
	// See lane_info_old.txt for array visualization
    if (fourIntersections == false)
    {
        printf("Using one intersection\n");
	    all_lanes[0].direction = WE;
	    Point start_pos0 = { 0, centre->y - intersection_width/4 };
	    Point end_pos0 = { centre->x, centre->y - intersection_width/4 };
	    all_lanes[0].start_pos = start_pos0;
	    all_lanes[0].end_pos = end_pos0;

	    all_lanes[1].direction = WE;
	    Point start_pos1 = { centre->x, centre->y - intersection_width/4 };
	    Point end_pos1 = { MAP_WIDTH, centre->y - intersection_width/4 };
	    all_lanes[1].start_pos = start_pos1;
	    all_lanes[1].end_pos = end_pos1;
	
	    all_lanes[2].direction = EW;
	    Point start_pos2 = { MAP_WIDTH, centre->y + intersection_width/8 };
	    Point end_pos2 = { centre->x, centre->y + intersection_width/8 };
	    all_lanes[2].start_pos = start_pos2;
	    all_lanes[2].end_pos = end_pos2;

	    all_lanes[3].direction = EW;
	    Point start_pos3 = { centre->x, centre->y + intersection_width/8 };
	    Point end_pos3 = { 0, centre->y + intersection_width/8 };
	    all_lanes[3].start_pos = start_pos3;
	    all_lanes[3].end_pos = end_pos3;

	    all_lanes[4].direction = SN;
	    Point start_pos4 = { centre->x - intersection_width/4, MAP_HEIGHT };
	    Point end_pos4 = { centre->x - intersection_width/4, centre->y };
	    all_lanes[4].start_pos = start_pos4;
	    all_lanes[4].end_pos = end_pos4;

	    all_lanes[5].direction = SN;
	    Point start_pos5 = { centre->x - intersection_width/4, centre->y };
	    Point end_pos5 = { centre->x - intersection_width/4, 0 };
	    all_lanes[5].start_pos = start_pos5;
	    all_lanes[5].end_pos = end_pos5;

	    all_lanes[6].direction = NS;
	    Point start_pos6 = { centre->x + intersection_width/8, 0 };
	    Point end_pos6 = { centre->x + intersection_width/8, centre->y };
	    all_lanes[6].start_pos = start_pos6;
	    all_lanes[6].end_pos = end_pos6;

	    all_lanes[7].direction = NS;
	    Point start_pos7 = { centre->x + intersection_width/8, centre->y };
	    Point end_pos7 = { centre->x + intersection_width/8, MAP_HEIGHT };
	    all_lanes[7].start_pos = start_pos7;
	    all_lanes[7].end_pos = end_pos7;
	}
	// Construct the four intersection model
	// See lane_info_four_intersections.txt for array visualization
	else
	{
        printf("Using four intersections\n");
	    // WE -------- top
	    all_lanes[0].direction = WE;
	    Point start_pos0 = { 0, topLeft->y - intersection_width/4 };
	    Point end_pos0 = { topLeft->x - intersection_width/4, topLeft->y - intersection_width/4 };
	    all_lanes[0].start_pos = start_pos0;
	    all_lanes[0].end_pos = end_pos0;

	    all_lanes[1].direction = WE;
	    Point start_pos1 = { topLeft->x + intersection_width/4, topLeft->y - intersection_width/4 };
	    Point end_pos1 = { topRight->x - intersection_width/4, topLeft->y - intersection_width/4 };
	    all_lanes[1].start_pos = start_pos1;
	    all_lanes[1].end_pos = end_pos1;
	    
	    all_lanes[22].direction = WE;
	    Point start_pos22 = { topRight->x + intersection_width/4, topLeft->y - intersection_width/4 };
	    Point end_pos22 = { MAP_WIDTH, topLeft->y - intersection_width/4 };
	    all_lanes[22].start_pos = start_pos22;
	    all_lanes[22].end_pos = end_pos22;
	    
	    // WE -------- bot
	    all_lanes[8].direction = WE;
	    Point start_pos8 = { 0, botLeft->y - intersection_width/4 };
	    Point end_pos8 = { botLeft->x - intersection_width/4, botLeft->y - intersection_width/4 };
	    all_lanes[8].start_pos = start_pos8;
	    all_lanes[8].end_pos = end_pos8;

	    all_lanes[9].direction = WE;
	    Point start_pos9 = { botLeft->x + intersection_width/4, botLeft->y - intersection_width/4 };
	    Point end_pos9 = { botRight->x - intersection_width/4, botLeft->y - intersection_width/4 };
	    all_lanes[9].start_pos = start_pos9;
	    all_lanes[9].end_pos = end_pos9;
	    
	    all_lanes[10].direction = WE;
	    Point start_pos10 = { botRight->x + intersection_width/4, botLeft->y - intersection_width/4 };
	    Point end_pos10 = { MAP_WIDTH, botLeft->y - intersection_width/4 };
	    all_lanes[10].start_pos = start_pos10;
	    all_lanes[10].end_pos = end_pos10;
	
	    // EW -------- top
	    all_lanes[23].direction = EW;
	    Point start_pos23 = { MAP_WIDTH, topRight->y + intersection_width/4 };
	    Point end_pos23 = { topRight->x + intersection_width/4, topRight->y + intersection_width/4 };
	    all_lanes[23].start_pos = start_pos23;
	    all_lanes[23].end_pos = end_pos23;
	    
	    all_lanes[2].direction = EW;
	    Point start_pos2 = { topRight->x - intersection_width/4, topRight->y + intersection_width/4 };
	    Point end_pos2 = { topLeft->x + intersection_width/4, topRight->y + intersection_width/4 };
	    all_lanes[2].start_pos = start_pos2;
	    all_lanes[2].end_pos = end_pos2;

	    all_lanes[3].direction = EW;
	    Point start_pos3 = { topLeft->x - intersection_width/4, topRight->y + intersection_width/4 };
	    Point end_pos3 = { 0, topRight->y + intersection_width/4 };
	    all_lanes[3].start_pos = start_pos3;
	    all_lanes[3].end_pos = end_pos3;
	    
	    // EW -------- bot
	    all_lanes[11].direction = EW;
	    Point start_pos11 = { MAP_WIDTH, botRight->y + intersection_width/4 };
	    Point end_pos11 = { botRight->x + intersection_width/4, botRight->y + intersection_width/4 };
	    all_lanes[11].start_pos = start_pos11;
	    all_lanes[11].end_pos = end_pos11;
	    
	    all_lanes[12].direction = EW;
	    Point start_pos12 = { botRight->x - intersection_width/4, botRight->y + intersection_width/4 };
	    Point end_pos12 = { botLeft->x + intersection_width/4, botRight->y + intersection_width/4 };
	    all_lanes[12].start_pos = start_pos12;
	    all_lanes[12].end_pos = end_pos12;

	    all_lanes[13].direction = EW;
	    Point start_pos13 = { botLeft->x - intersection_width/4, botRight->y + intersection_width/4 };
	    Point end_pos13 = { 0, botRight->y + intersection_width/4 };
	    all_lanes[13].start_pos = start_pos13;
	    all_lanes[13].end_pos = end_pos13;

        // SN -------- left
	    all_lanes[20].direction = SN;
	    Point start_pos20 = { botLeft->x - intersection_width/4, MAP_HEIGHT };
	    Point end_pos20 = { botLeft->x - intersection_width/4, botLeft->y + intersection_width/4 };
	    all_lanes[20].start_pos = start_pos20;
	    all_lanes[20].end_pos = end_pos20;
        
	    all_lanes[4].direction = SN;
	    Point start_pos4 = { botLeft->x - intersection_width/4, botLeft->y - intersection_width/4 };
	    Point end_pos4 = { botLeft->x - intersection_width/4, topLeft->y + intersection_width/4 };
	    all_lanes[4].start_pos = start_pos4;
	    all_lanes[4].end_pos = end_pos4;

	    all_lanes[5].direction = SN;
	    Point start_pos5 = { botLeft->x - intersection_width/4, topLeft->y - intersection_width/4 };
	    Point end_pos5 = { botLeft->x - intersection_width/4, 0 };
	    all_lanes[5].start_pos = start_pos5;
	    all_lanes[5].end_pos = end_pos5;
	    
        // SN -------- right
	    all_lanes[14].direction = SN;
	    Point start_pos14 = { botRight->x - intersection_width/4, MAP_HEIGHT };
	    Point end_pos14 = { botRight->x - intersection_width/4, botRight->y + intersection_width/4 };
	    all_lanes[14].start_pos = start_pos14;
	    all_lanes[14].end_pos = end_pos14;
        
	    all_lanes[15].direction = SN;
	    Point start_pos15 = { botRight->x - intersection_width/4, botRight->y - intersection_width/4 };
	    Point end_pos15 = { botRight->x - intersection_width/4, topRight->y + intersection_width/4 };
	    all_lanes[15].start_pos = start_pos15;
	    all_lanes[15].end_pos = end_pos15;

	    all_lanes[16].direction = SN;
	    Point start_pos16 = { botRight->x - intersection_width/4, topRight->y - intersection_width/4 };
	    Point end_pos16 = { botRight->x - intersection_width/4, 0 };
	    all_lanes[16].start_pos = start_pos16;
	    all_lanes[16].end_pos = end_pos16;

        // NS -------- left
	    all_lanes[6].direction = NS;
	    Point start_pos6 = { topLeft->x + intersection_width/4, 0 };
	    Point end_pos6 = { topLeft->x + intersection_width/4, topLeft->y - intersection_width/4 };
	    all_lanes[6].start_pos = start_pos6;
	    all_lanes[6].end_pos = end_pos6;

	    all_lanes[7].direction = NS;
	    Point start_pos7 = { topLeft->x + intersection_width/4, topLeft->y + intersection_width/4 };
	    Point end_pos7 = { topLeft->x + intersection_width/4, botLeft->y - intersection_width/4 };
	    all_lanes[7].start_pos = start_pos7;
	    all_lanes[7].end_pos = end_pos7;
	    
	    all_lanes[21].direction = NS;
	    Point start_pos21 = { topLeft->x + intersection_width/4, botLeft->y + intersection_width/4 };
	    Point end_pos21 = { topLeft->x + intersection_width/4, MAP_HEIGHT };
	    all_lanes[21].start_pos = start_pos21;
	    all_lanes[21].end_pos = end_pos21;
	    
        // NS -------- right
	    all_lanes[17].direction = NS;
	    Point start_pos17 = { topRight->x + intersection_width/4, 0 };
	    Point end_pos17 = { topRight->x + intersection_width/4, topRight->y - intersection_width/4 };
	    all_lanes[17].start_pos = start_pos17;
	    all_lanes[17].end_pos = end_pos17;

	    all_lanes[18].direction = NS;
	    Point start_pos18 = { topRight->x + intersection_width/4, topRight->y + intersection_width/4 };
	    Point end_pos18 = { topRight->x + intersection_width/4, botRight->y - intersection_width/4 };
	    all_lanes[18].start_pos = start_pos18;
	    all_lanes[18].end_pos = end_pos18;
	    
	    all_lanes[19].direction = NS;
	    Point start_pos19 = { topRight->x + intersection_width/4, botRight->y + intersection_width/4 };
	    Point end_pos19 = { topRight->x + intersection_width/4, MAP_HEIGHT };
	    all_lanes[19].start_pos = start_pos19;
	    all_lanes[19].end_pos = end_pos19;
	}
}

//----------------------------------------------------------------------------------------
// Initialize internal data structure used to create arrows.
//----------------------------------------------------------------------------------------
void create_Intersections()
{
	for(int i=0; i<number_of_intersections; i++) {
	
		intersectionConstructors[i].origin.x = intersections[i].origin.x;
		intersectionConstructors[i].origin.y = intersections[i].origin.y;
		
		intersectionConstructors[i].northDone = false;
		intersectionConstructors[i].eastDone = false;
		intersectionConstructors[i].southDone = false;
		intersectionConstructors[i].westDone = false;
	}
}

//----------------------------------------------------------------------------------------
//add relation of previous arrow and next arrow
//----------------------------------------------------------------------------------------
void addPre(int num)
{	
	for(int i=0; i<num; i++) {
		int nextInArray = 0;
		
		//if my starting point is some arrow's end point
		if(arrows[i].end.x == arrows[num].start.x && arrows[i].end.y == arrows[num].start.y) {
			//arrow[i] is my pre
			for(int j=0; j<MAX_ARROW_CONNECTORS; j++) {
				if (arrows[num].startConnectors[j] ==  NULL)
				{
					nextInArray = j;
					break;
				}
			}
			arrows[num].startConnectors[nextInArray] = &arrows[i];
			
			//i am arrow[i]'s next
			for(int j=0; j<MAX_ARROW_CONNECTORS; j++) {
			    if (arrows[num].endConnectors[j] ==  NULL)
				{
					nextInArray = j;
					break;
				}
			}
			arrows[i].endConnectors[nextInArray] = &arrows[num];
		}
		
		//if my end point is also some arrow's end point
		if(arrows[i].end.x == arrows[num].end.x && arrows[i].end.y == arrows[num].end.y) {
			//arrow[i] is my next
			for(int j=0; j<MAX_ARROW_CONNECTORS; j++) {
			    if (arrows[num].endConnectors[j] ==  NULL)
				{
					nextInArray = j;
					break;
				}
			}
			arrows[num].endConnectors[nextInArray] = &arrows[i];
			
			//i am arrow[i]'s next
			for(int j=0; j<MAX_ARROW_CONNECTORS; j++) {
				if (arrows[num].endConnectors[j] ==  NULL)
				{
					nextInArray = j;
					break;
				}
			}
			arrows[i].endConnectors[nextInArray] = &arrows[num];
		}
		
		//if my start point is also some arrow's start point
		if(arrows[i].start.x == arrows[num].start.x && arrows[i].start.y == arrows[num].start.y) {
			//arrow[i] is my pre
			for(int j=0; j<MAX_ARROW_CONNECTORS; j++) {
				if (arrows[num].startConnectors[j] ==  NULL)
				{
					nextInArray = j;
					break;
				}
			}
			arrows[num].startConnectors[nextInArray] = &arrows[i];
			
			//i am arrow[i]'s pre
			for(int j=0; j<MAX_ARROW_CONNECTORS; j++) {
				if (arrows[num].startConnectors[j] ==  NULL)
				{
					nextInArray = j;
					break;
				}
			}
			arrows[i].startConnectors[nextInArray] = &arrows[num];
		}

		//if my end point is some arrow's start point
		if(arrows[i].end.x == arrows[num].start.x && arrows[i].end.y == arrows[num].start.y) {
			//arrow[i] is my next
			for(int j=0; j<MAX_ARROW_CONNECTORS; j++) {
				if (arrows[num].endConnectors[j] ==  NULL)
				{
					nextInArray = j;
					break;
				}
			}
			arrows[num].endConnectors[nextInArray] = &arrows[i];
			
			//i am arrow[i]'s pre
			for(int j=0; j<MAX_ARROW_CONNECTORS; j++) {
				if (arrows[num].startConnectors[j] ==  NULL)
				{
					nextInArray = j;
					break;
				}
			}
			arrows[i].startConnectors[nextInArray] = &arrows[num];
		}
	}
}

//----------------------------------------------------------------------------------------
// create arrows of vertical direction
//----------------------------------------------------------------------------------------
void create_vertical(Point start, Point end)
{
	//number of arrows going to create
	int size = ((int)end.y - (int)start.y)/100 + 1;
	
	//add new arrows at the end of new array
	for(int i=0; i<size; i++) {
		//at the edge of map (left-hand side), no pre arrow
		if(i==0 && start.y==0) {
			arrows[i+arrowNextInArray].start.x = start.x;
			arrows[i+arrowNextInArray].start.y = start.y + i*100;
			arrows[i+arrowNextInArray].end.x = start.x;
			arrows[i+arrowNextInArray].end.y = start.y + (i+1)*100;
		}
		if(i == size-1) {
			arrows[i+arrowNextInArray].start.x = start.x;
			arrows[i+arrowNextInArray].start.y = start.y + i*100;
			arrows[i+arrowNextInArray].end.x = end.x;
			arrows[i+arrowNextInArray].end.y = end.y;
			addPre(i+arrowNextInArray);
		}
		else {
			arrows[i+arrowNextInArray].start.x = start.x;
            arrows[i+arrowNextInArray].start.y = start.y + i*100;
			arrows[i+arrowNextInArray].end.x = start.x;
			arrows[i+arrowNextInArray].end.y = start.y + (i+1)*100;
			addPre(i+arrowNextInArray);
		}
	}
	
	num_arrows = arrowNextInArray + size;
	arrowNextInArray += size;
}

//----------------------------------------------------------------------------------------
// create arrows of horizontal directions
//----------------------------------------------------------------------------------------
void create_horizontal(Point start, Point end)
{
	//number of arrows going to create
	int size = ((int)end.x - (int)start.x)/100 + 1;
	
	//add new arrows at the end of new array
	for(int i=0; i<size; i++) {
		//at the edge of map (left-hand side), no pre arrow
		if(i==0 && start.x==0) {
			arrows[i+arrowNextInArray].start.x = start.x + i*100;
			arrows[i+arrowNextInArray].start.y = start.y;
			arrows[i+arrowNextInArray].end.x = start.x + (i+1)*100;
			arrows[i+arrowNextInArray].end.y = start.y;
		}
		if(i==size-1) {
			arrows[i+arrowNextInArray].start.x = start.x + i*100;
			arrows[i+arrowNextInArray].start.y = start.y;
			arrows[i+arrowNextInArray].end.x = end.x;
			arrows[i+arrowNextInArray].end.y = end.y;
			addPre(i+arrowNextInArray);
		}
		else {
			arrows[i+arrowNextInArray].start.x = start.x + i*100;
			arrows[i+arrowNextInArray].start.y = start.y;
			arrows[i+arrowNextInArray].end.x = start.x + (i+1)*100;
			arrows[i+arrowNextInArray].end.y = start.y;
			addPre(i+arrowNextInArray);
		}
	}
	
	num_arrows = arrowNextInArray + size;
	arrowNextInArray += size;
}

//----------------------------------------------------------------------------------------
// Automatic generation of arrows.
// This function loops through all the intersections and creates arrows around them.
// See infrastrucre.h for the basic algorithm outline.
// This function also creates an intermediate intersectionConstructor structure for each intersection
// which is used to determine whether arrows have been created around the intersection or not
//----------------------------------------------------------------------------------------
void create_arrows()
{
	for(int i=0; i<number_of_intersections; i++) {	
		float x = 0;
		float y = 0;
		
		if(intersectionConstructors[i].northDone == false) {
			y = 0;
			for(int j=0; j<number_of_intersections; j++) {
				if((intersectionConstructors[j].origin.y < intersectionConstructors[i].origin.y) && (intersectionConstructors[j].origin.y > y)) {
					y = intersectionConstructors[j].origin.y;
				}
			}
			
		    Point pt = intersectionConstructors[i].origin;	
			Point start = {pt.x, y};
			Point end = {pt.x, pt.y};
			create_vertical(start, end);
			intersectionConstructors[i].northDone = true;
			for(int j=0; j<number_of_intersections; j++) {
				if(intersectionConstructors[j].origin.x == start.x && intersectionConstructors[j].origin.y == start.y) {
					intersectionConstructors[j].southDone = true;
				}
			}
		}
		
		if(intersectionConstructors[i].eastDone == false) {
			Point pt = intersectionConstructors[i].origin;
			x = MAP_WIDTH;
			for(int j=0; j<number_of_intersections; j++) {
				if((intersectionConstructors[j].origin.x < x) && (intersectionConstructors[j].origin.x > pt.x)) {
					x = intersectionConstructors[j].origin.x;
				}
			}
			Point start = pt;
			Point end = {x, pt.y};
			create_horizontal(start, end);
			intersectionConstructors[i].eastDone = true;
			for(int j=0; j<number_of_intersections; j++) {
				if(intersectionConstructors[j].origin.x == end.x && intersectionConstructors[j].origin.y == end.y) {
					intersectionConstructors[j].westDone = true;
				}
			}
		}
		
		if(intersectionConstructors[i].southDone == false) {
			Point pt = intersectionConstructors[i].origin;
			y = MAP_HEIGHT;
			for(int j=0; j<number_of_intersections; j++) {
				if((intersectionConstructors[j].origin.y < y) && (intersectionConstructors[j].origin.y > pt.y)) {
					y = intersectionConstructors[j].origin.y;
				}
			}
			Point start = pt;
			Point end = {pt.x, y};
			create_vertical(start, end);
			intersectionConstructors[i].southDone = true;
			for(int j=0; j<number_of_intersections; j++) {
				if(intersectionConstructors[j].origin.x == end.x && intersectionConstructors[j].origin.y == end.y) {
					intersectionConstructors[j].northDone = true;
				}
			}
		}
		
		if(intersectionConstructors[i].westDone == false) {
			Point pt = intersectionConstructors[i].origin;
			x = 0;
			for(int j=0; j<number_of_intersections; j++) {
				if((intersectionConstructors[j].origin.x < pt.x) && (intersectionConstructors[j].origin.x > x)) {
					x = intersectionConstructors[j].origin.x;
				}
			}
			Point start = {x, pt.y};
			Point end = pt;
			create_horizontal(start, end);
			intersectionConstructors[i].westDone = true;
			for(int j=0; j<number_of_intersections; j++) {
				if(intersectionConstructors[j].origin.x == start.x && intersectionConstructors[j].origin.y == start.y) {
					intersectionConstructors[j].eastDone = true;
				}
			}
		}
	}
}

//----------------------------------------------------------------------------------------
// Top level init function. Called when "go" button is clicked on GUI
//----------------------------------------------------------------------------------------
void init_infrastructure()
{
	//New Intersection array
	intersectionConstructors = (IntersectionConstructor*) malloc(4 * sizeof(IntersectionConstructor));

	//Create new intersections
	create_Intersections();
	//Create arrows
	create_arrows();
	printf("Num arrows: %i\n", num_arrows);

	Point centre = { intersections[0].origin.x, intersections[0].origin.y };
	init_box(&centre);
	init_lanes();
    init_traffic_lights();
}

//----------------------------------------------------------------------------------------
// Function called by GUI periodically after simulation has started.
// Currently does nothing. Can be used to verify correctness of the model.
//----------------------------------------------------------------------------------------
void update_infrastructure()
{
}


