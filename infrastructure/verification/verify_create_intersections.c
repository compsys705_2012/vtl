//#ifndef VTL_GLOBALS_H
//#define VTL_GLOBALS_H
//#include "dummy_globals.h"
//#include "stdint2.h" //allows using int, 1 and 0
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <string.h>




#define MAX_CARS_PER_LANE 50 //Array of cars in each lane is statically allocated
#define NUM_OF_LANES 8 // Currently 8 to preserve the vehicle and controller model. Should be 22 to support four intersections.

#define LANE_DENSITY 3200 //this is cars per hour

#define MILLISECONDS_PER_UPDATE 100
#define MILLISECONDS_PER_SECOND 1000
#define DEBUG 1

typedef enum Direction {
	UP,
	DOWN,
	LEFT,
	RIGHT,
	SN = UP,    // South to north
	NS = DOWN,  // North to south
	EW = LEFT,  // East to west
	WE = RIGHT  // West to east
} Direction;

//Each car has a TurnIntention which shows its intended direction of travel
typedef enum TurnIntention {
	LEFT_TURN,
	RIGHT_RIGHT,
	STRAIGHT
} TurnIntention;

typedef struct Point {
	float x;
	float y;
} Point;


#define decrement(i) ((i) = ((i) + MAX_CARS_PER_LANE -1) % MAX_CARS_PER_LANE)
#define increment(i) ((i) = ((i) + 1) % MAX_CARS_PER_LANE)
#define compare(i, start, end) (i < end) || (i > start && start > end)
#define foreach_car(i, start, end) for(i = start; (i < end) || ((i >= start) && (start > end)); increment(i))

/**
 * This struct describes the positions of a lane
 * A horizontal lane will have identical y co-ords
 * A vertical lane will have identical x co-ords
 * 
 * All cars in the simulation are associated with a lane. Lanes have direction.
 * Cars in a lane will always be travelling in the direction of the lane
 * 
 * Presently there is only one intersection in the simulation
 * Thus there are only 8 possible lanes.
 * Each lane has an ID number assigned as per <lane_info.txt>
 * The car at the "front" of the lane ie. closest to the next intersection is at start_index
 * The last car in the queue of cars on this lane is at end_index
 */


typedef struct Intersection {
	Point origin;
	float width;
	float height;
	int isGenerated; // Whether the intersection was automatically generated
} Intersection;


// ====================================================================
//INFRASTRUCTURE
// ====================================================================

/* Traffic Light State */
typedef enum {
	RED = 0,
	ORANGE = 1,
	GREEN = 2,
}TrafLightState;

// Individual traffic light
typedef struct{
	float xPos;
	float yPos;
	TrafLightState state;
} trafLight;

// Traffic lights at a single intersection. One for horizontal and one for vertical.
// When turning is implemented, will need four, one for each lane.
typedef struct
{
    trafLight trafLightNS;
    trafLight trafLightEW;
} IntersectionLights;

// Internal struct used for verification by infrastructure
typedef struct Box {
	Point center;
	float width;
	float height;
	Direction direction;
} Box;

/* ---- New Intersection ----*/
typedef struct IntersectionConstructor {
	Point origin;
	int northDone;
	int eastDone;
	int southDone;
	int westDone;
} IntersectionConstructor;

Box goZone[4];


// Traffic lights for the intersection. 2 Traffic lights per intersction.
// Currently only defines 1 intersection.
IntersectionLights* intersectionLights;



/*---------------------------Dimensions-------------------------------*/
/**
 * The units of all the dimensions below are pixels right??
 */
const float CAR_LENGTH = 40.0;
const float CAR_WIDTH = 20.0;

//The dimensions of the map. See globals.h for a decription of
//why these are commented out
//const float MAP_WIDTH = 3000.0;
//const float MAP_HEIGHT = 3000.0;

const float intersection_width = 100.0;
const float intersection_height = 100.0;
const float intersection_padding = 20.0; // How much gap to leave between intersections

const float TRAFFIC_LIGHT_WIDTH = 20.0;
const float TRAFFIC_LIGHT_HEIGHT = 20.0;

//Assuming the avererage family car is 4.4m long (See en.wikipedia.org/wiki/large_family_car)
//And that a car is 40 pixels long, then 40/4.4 = 9.1 pixels/m
#define PIXELS_PER_M 9.1

/*---------------------------End Dimensions-------------------------------*/


int global_speed_limit = 50; //this is in km/h and can vary between 50 and 100;
//refresh rate of simulation
const float UPDATES_PER_SECOND = (float)MILLISECONDS_PER_SECOND / MILLISECONDS_PER_UPDATE;

Intersection* intersections;
int number_of_intersections = 1;
int max_number_of_intersections = 0;

// New Intersection array
IntersectionConstructor* intersectionConstructors;

int to_continue = 1;

void create_Intersections()
{
	//Verification variable
	int success = 0;
	for(int i=0; i<number_of_intersections; i++) {
	
		intersectionConstructors[i].origin.x = intersections[i].origin.x;
		intersectionConstructors[i].origin.y = intersections[i].origin.y;
		
		intersectionConstructors[i].northDone = 0;
		intersectionConstructors[i].eastDone = 0;
		intersectionConstructors[i].southDone = 0;
		intersectionConstructors[i].westDone = 0;
		success = 1;
	}
	    if (success == 0) {
		CREATE_INTERSECTION_ERROR: goto CREATE_INTERSECTION_ERROR;
	}
}
/*
int main(){

	while (1){
		init_traffic_lights();
	}
}*/
