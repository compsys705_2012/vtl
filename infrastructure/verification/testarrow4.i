# 1 "testarrow4.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "testarrow4.c"
# 1 "/afs/ec.auckland.ac.nz/users/j/l/jlin139/unixhome/blast-2.5/blast/test/headers/assert.h" 1
# 36 "/afs/ec.auckland.ac.nz/users/j/l/jlin139/unixhome/blast-2.5/blast/test/headers/assert.h"
# 1 "/usr/include/features.h" 1 3 4
# 323 "/usr/include/features.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/predefs.h" 1 3 4
# 324 "/usr/include/features.h" 2 3 4
# 356 "/usr/include/features.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/sys/cdefs.h" 1 3 4
# 353 "/usr/include/x86_64-linux-gnu/sys/cdefs.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/bits/wordsize.h" 1 3 4
# 354 "/usr/include/x86_64-linux-gnu/sys/cdefs.h" 2 3 4
# 357 "/usr/include/features.h" 2 3 4
# 388 "/usr/include/features.h" 3 4
# 1 "/usr/include/x86_64-linux-gnu/gnu/stubs.h" 1 3 4



# 1 "/usr/include/x86_64-linux-gnu/bits/wordsize.h" 1 3 4
# 5 "/usr/include/x86_64-linux-gnu/gnu/stubs.h" 2 3 4




# 1 "/usr/include/x86_64-linux-gnu/gnu/stubs-64.h" 1 3 4
# 10 "/usr/include/x86_64-linux-gnu/gnu/stubs.h" 2 3 4
# 389 "/usr/include/features.h" 2 3 4
# 37 "/afs/ec.auckland.ac.nz/users/j/l/jlin139/unixhome/blast-2.5/blast/test/headers/assert.h" 2
# 65 "/afs/ec.auckland.ac.nz/users/j/l/jlin139/unixhome/blast-2.5/blast/test/headers/assert.h"



void __blast_assert() __attribute__ ((__noreturn__)) {
ERROR: goto ERROR;
}



void __assert_fail (__const char *__assertion, __const char *__file,
      unsigned int __line, __const char *__function)
     __attribute__ ((__nothrow__)) __attribute__ ((__noreturn__)) {
   __blast_assert();
}


void __assert_perror_fail (int __errnum, __const char *__file,
      unsigned int __line,
      __const char *__function)
     __attribute__ ((__nothrow__)) __attribute__ ((__noreturn__)) {
   __blast_assert();
}




void __assert (const char *__assertion, const char *__file, int __line)
     __attribute__ ((__nothrow__)) __attribute__ ((__noreturn__)) {
 __blast_assert();
}




# 2 "testarrow4.c" 2


typedef struct Point {
 float x;
 float y;
} Point;



typedef struct Arrow {
 Point start;
 Point end;
 struct Arrow* endConnectors[5];
 struct Arrow* startConnectors[5];
} Arrow;
int num_arrows;
Arrow* arrows;

int main() {



 for(int i=0; i<num_arrows; i++) {
     ((void) ((arrows[i].start.x < 0) ? 0 : (__assert_fail ("arrows[i].start.x < 0", "testarrow4.c", 25, __PRETTY_FUNCTION__), 0)));



 }
 return 0;
}
