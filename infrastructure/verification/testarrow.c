#include <stdio.h>
#include <stdlib.h>

//Point
typedef struct Point {
	float x;
	float y;
} Point;

//Intersection
typedef struct Intersection {
	Point origin;
	float width;
	float height;
	int isGenerated; // Whether the intersection was automatically generated
} Intersection;
Intersection* intersections;
int number_of_intersections = 0;
const float intersection_width = 100.0;
const float intersection_height = 100.0;
#define MAP_WIDTH 4000.0
#define MAP_HEIGHT 3000.0
#define MAX_ARROW_CONNECTORS 5

//New Intersection
typedef struct IntersectionConstructor {
	Point origin;
	int northDone;
	int eastDone;
	int southDone;
	int westDone;
} IntersectionConstructor;

IntersectionConstructor* intersectionConstructors;

//Arrow

typedef struct Arrow {
	Point start;
	Point end;
	struct Arrow* endConnectors[MAX_ARROW_CONNECTORS];
	struct Arrow* startConnectors[MAX_ARROW_CONNECTORS];
} Arrow;
#define MAX_ARROWS 1000
int num_arrows = 0;
int arrowNextInArray = 0;
Arrow* arrows;

//GoZone box
typedef struct Box {
	Point center;
	float width;
	float height;
} Box;
int max_goZone = 0;
int goZone_size = 0;
Box* goZone;

/*
void init_box() {
	max_goZone = number_of_intersections * 2;
	goZone = (Box*) malloc(max_goZone * sizeof(Box));
	
	float x;
	float y;
	int found = 0;
	
	for(int i=0; i<number_of_intersections; i++) {
	    if(goZone_size == 0) {
	        x = intersections[i].origin.x;
	        y = MAP_HEIGHT/2;
	        Point ptOne = {x, y};
	        Box boxOne = {ptOne, intersection_width, MAP_HEIGHT};
	        goZone[0] = boxOne;
	        goZone_size += 1;
	        
	        x = MAP_WIDTH/2;
	        y = intersections[i].origin.y;
	        Point ptTwo = {x, y};
	        Box boxTwo = {ptTwo, MAP_WIDTH, intersection_height};
	        goZone[1] = boxTwo;
	        goZone_size += 1;
	    }
	    else
	    {
	        found = 0;
	        x = intersections[i].origin.x;
	        y = MAP_HEIGHT/2;
	        for(int j=0; j<goZone_size; j++) {
	            if(x==goZone[j].center.x && y==goZone[j].center.y) {
	                found = 1;
	            }
	        }
	        if(found) {
	            Point ptOne = {x, y};
	            Box boxOne = {ptOne, intersection_width, MAP_HEIGHT};
	            goZone[goZone_size] = boxOne;
	            goZone_size += 1;
	        }
	        
	        found = 0;
	        x = MAP_WIDTH/2;
	        y = intersections[i].origin.y;
	        for(int j=0; j<goZone_size; j++) {
	            if(x==goZone[j].center.x && y==goZone[j].center.y) {
	                found = 1;
	            }
	        }
	        if(found == 1) {
	            Point ptTwo = {x, y};
	            Box boxTwo = {ptTwo, MAP_WIDTH, intersection_height};
	            goZone[goZone_size] = boxTwo;
	            goZone_size += 1;
	        } 
	    }
	}
}

void create_Intersections() {
	for(int i=0; i<number_of_intersections; i++) {
	
		intersectionConstructors[i].origin.x = intersections[i].origin.x;
		intersectionConstructors[i].origin.y = intersections[i].origin.y;
		
		intersectionConstructors[i].northDone = 0;
		intersectionConstructors[i].eastDone = 0;
		intersectionConstructors[i].southDone = 0;
		intersectionConstructors[i].westDone = 0;
	}
}

//add relation of previous arrow and next arrow
void addPre(int num) {	
	for(int i=0; i<num; i++) {
		int nextInArray = 0;
		
		//if my starting point is some arrow's end point
		if(arrows[i].end.x == arrows[num].start.x && arrows[i].end.y == arrows[num].start.y) {
			//arrow[i] is my pre
			for(int j=0; j<MAX_ARROW_CONNECTORS; j++) {
				if (arrows[num].startConnectors[j] ==  NULL)
				{
					nextInArray = j;
					break;
				}
			}
			arrows[num].startConnectors[nextInArray] = &arrows[i];
			
			//i am arrow[i]'s next
			for(int j=0; j<MAX_ARROW_CONNECTORS; j++) {
			    if (arrows[num].endConnectors[j] ==  NULL)
				{
					nextInArray = j;
					break;
				}
			}
			arrows[i].endConnectors[nextInArray] = &arrows[num];
		}
		
		//if my end point is also some arrow's end point
		if(arrows[i].end.x == arrows[num].end.x && arrows[i].end.y == arrows[num].end.y) {
			//arrow[i] is my next
			for(int j=0; j<MAX_ARROW_CONNECTORS; j++) {
			    if (arrows[num].endConnectors[j] ==  NULL)
				{
					nextInArray = j;
					break;
				}
			}
			arrows[num].endConnectors[nextInArray] = &arrows[i];
			
			//i am arrow[i]'s next
			for(int j=0; j<MAX_ARROW_CONNECTORS; j++) {
				if (arrows[num].endConnectors[j] ==  NULL)
				{
					nextInArray = j;
					break;
				}
			}
			arrows[i].endConnectors[nextInArray] = &arrows[num];
		}
		
		//if my start point is also some arrow's start point
		if(arrows[i].start.x == arrows[num].start.x && arrows[i].start.y == arrows[num].start.y) {
			//arrow[i] is my pre
			for(int j=0; j<MAX_ARROW_CONNECTORS; j++) {
				if (arrows[num].startConnectors[j] ==  NULL)
				{
					nextInArray = j;
					break;
				}
			}
			arrows[num].startConnectors[nextInArray] = &arrows[i];
			
			//i am arrow[i]'s pre
			for(int j=0; j<MAX_ARROW_CONNECTORS; j++) {
				if (arrows[num].startConnectors[j] ==  NULL)
				{
					nextInArray = j;
					break;
				}
			}
			arrows[i].startConnectors[nextInArray] = &arrows[num];
		}

		//if my end point is some arrow's start point
		if(arrows[i].end.x == arrows[num].start.x && arrows[i].end.y == arrows[num].start.y) {
			//arrow[i] is my next
			for(int j=0; j<MAX_ARROW_CONNECTORS; j++) {
				if (arrows[num].endConnectors[j] ==  NULL)
				{
					nextInArray = j;
					break;
				}
			}
			arrows[num].endConnectors[nextInArray] = &arrows[i];
			
			//i am arrow[i]'s pre
			for(int j=0; j<MAX_ARROW_CONNECTORS; j++) {
				if (arrows[num].startConnectors[j] ==  NULL)
				{
					nextInArray = j;
					break;
				}
			}
			arrows[i].startConnectors[nextInArray] = &arrows[num];
		}
	}
}

//create arrows of vertical direction
void create_vertical(Point start, Point end) {
	//number of arrows going to create
	int size = ((int)end.y - (int)start.y)/100 + 1;
	
	//add new arrows at the end of new array
	for(int i=0; i<size; i++) {
		//at the edge of map (left-hand side), no pre arrow
		if(i==0 && start.y==0) {
			arrows[i+arrowNextInArray].start.x = start.x;
			arrows[i+arrowNextInArray].start.y = start.y + i*100;
			arrows[i+arrowNextInArray].end.x = start.x;
			arrows[i+arrowNextInArray].end.y = start.y + (i+1)*100;
		}
		if(i == size-1) {
			arrows[i+arrowNextInArray].start.x = start.x;
			arrows[i+arrowNextInArray].start.y = start.y + i*100;
			arrows[i+arrowNextInArray].end.x = end.x;
			arrows[i+arrowNextInArray].end.y = end.y;
			addPre(i+arrowNextInArray);
		}
		else {
			arrows[i+arrowNextInArray].start.x = start.x;
            arrows[i+arrowNextInArray].start.y = start.y + i*100;
			arrows[i+arrowNextInArray].end.x = start.x;
			arrows[i+arrowNextInArray].end.y = start.y + (i+1)*100;
			addPre(i+arrowNextInArray);
		}
	}
	
	num_arrows = arrowNextInArray + size;
	arrowNextInArray += size;
}

//create arrows of horizontal directions
void create_horizontal(Point start, Point end) {
	//number of arrows going to create
	int size = ((int)end.x - (int)start.x)/100 + 1;
	
	//add new arrows at the end of new array
	for(int i=0; i<size; i++) {
		//at the edge of map (left-hand side), no pre arrow
		if(i==0 && start.x==0) {
			arrows[i+arrowNextInArray].start.x = start.x + i*100;
			arrows[i+arrowNextInArray].start.y = start.y;
			arrows[i+arrowNextInArray].end.x = start.x + (i+1)*100;
			arrows[i+arrowNextInArray].end.y = start.y;
		}
		if(i==size-1) {
			arrows[i+arrowNextInArray].start.x = start.x + i*100;
			arrows[i+arrowNextInArray].start.y = start.y;
			arrows[i+arrowNextInArray].end.x = end.x;
			arrows[i+arrowNextInArray].end.y = end.y;
			addPre(i+arrowNextInArray);
		}
		else {
			arrows[i+arrowNextInArray].start.x = start.x + i*100;
			arrows[i+arrowNextInArray].start.y = start.y;
			arrows[i+arrowNextInArray].end.x = start.x + (i+1)*100;
			arrows[i+arrowNextInArray].end.y = start.y;
			addPre(i+arrowNextInArray);
		}
	}
	
	num_arrows = arrowNextInArray + size;
	arrowNextInArray += size;
}

void create_arrows() {
	for(int i=0; i<number_of_intersections; i++) {	
		float x = 0;
		float y = 0;
		
		if(intersectionConstructors[i].northDone == 0) {
			y = 0;
			for(int j=0; j<number_of_intersections; j++) {
				if((intersectionConstructors[j].origin.y < intersectionConstructors[i].origin.y) && (intersectionConstructors[j].origin.y > y)) {
					y = intersectionConstructors[j].origin.y;
				}
			}
			
		    Point pt = intersectionConstructors[i].origin;	
			Point start = {pt.x, y};
			Point end = {pt.x, pt.y};
			create_vertical(start, end);
			intersectionConstructors[i].northDone = 1;
			for(int j=0; j<number_of_intersections; j++) {
				if(intersectionConstructors[j].origin.x == start.x && intersectionConstructors[j].origin.y == start.y) {
					intersectionConstructors[j].southDone = 1;
				}
			}
		}
		
		if(intersectionConstructors[i].eastDone == 0) {
			Point pt = intersectionConstructors[i].origin;
			x = MAP_WIDTH;
			for(int j=0; j<number_of_intersections; j++) {
				if((intersectionConstructors[j].origin.x < x) && (intersectionConstructors[j].origin.x > pt.x)) {
					x = intersectionConstructors[j].origin.x;
				}
			}
			Point start = pt;
			Point end = {x, pt.y};
			create_horizontal(start, end);
			intersectionConstructors[i].eastDone = 1;
			for(int j=0; j<number_of_intersections; j++) {
				if(intersectionConstructors[j].origin.x == end.x && intersectionConstructors[j].origin.y == end.y) {
					intersectionConstructors[j].westDone = 1;
				}
			}
		}
		
		if(intersectionConstructors[i].southDone == 0) {
			Point pt = intersectionConstructors[i].origin;
			y = MAP_HEIGHT;
			for(int j=0; j<number_of_intersections; j++) {
				if((intersectionConstructors[j].origin.y < y) && (intersectionConstructors[j].origin.y > pt.y)) {
					y = intersectionConstructors[j].origin.y;
				}
			}
			Point start = pt;
			Point end = {pt.x, y};
			create_vertical(start, end);
			intersectionConstructors[i].southDone = 1;
			for(int j=0; j<number_of_intersections; j++) {
				if(intersectionConstructors[j].origin.x == end.x && intersectionConstructors[j].origin.y == end.y) {
					intersectionConstructors[j].northDone = 1;
				}
			}
		}
		
		if(intersectionConstructors[i].westDone == 0) {
			Point pt = intersectionConstructors[i].origin;
			x = 0;
			for(int j=0; j<number_of_intersections; j++) {
				if((intersectionConstructors[j].origin.x < pt.x) && (intersectionConstructors[j].origin.x > x)) {
					x = intersectionConstructors[j].origin.x;
				}
			}
			Point start = {x, pt.y};
			Point end = pt;
			create_horizontal(start, end);
			intersectionConstructors[i].westDone = 1;
			for(int j=0; j<number_of_intersections; j++) {
				if(intersectionConstructors[j].origin.x == start.x && intersectionConstructors[j].origin.y == start.y) {
					intersectionConstructors[j].eastDone = 1;
				}
			}
		}
	}
}
*/

int main() {
/*
    int isGenerated = 1;
    int x = 0;
    int y = 0;
    //Create some dummy datas (4 intersections)
    intersections = (Intersection*) malloc(4 * sizeof(Intersection));
    intersections[0].origin.x = 3000;
    intersections[0].origin.y = 2000;
    intersections[0].width = intersection_width;
    intersections[0].height = intersection_height;
    intersections[0].isGenerated = isGenerated;
    number_of_intersections++;
    
    intersections[1].origin.x = 1000;
    intersections[1].origin.y = 1000;
    intersections[1].width = intersection_width;
    intersections[1].height = intersection_height;
    intersections[1].isGenerated = isGenerated;
    number_of_intersections++;
    
    intersections[2].origin.x = 3000;
    intersections[2].origin.y = 1000;
    intersections[2].width = intersection_width;
    intersections[2].height = intersection_height;
    intersections[2].isGenerated = isGenerated;
    number_of_intersections++;
    
    intersections[3].origin.x = 1000;
    intersections[3].origin.y = 2000;
    intersections[3].width = intersection_width;
    intersections[3].height = intersection_height;
    intersections[3].isGenerated = isGenerated;
    number_of_intersections++;

    //Create goZone
    init_box();
    //New Intersection array
	intersectionConstructors = (IntersectionConstructor*) malloc(4 * sizeof(IntersectionConstructor));
	create_Intersections();
	//Create arrows
	create_arrows();
	
	//If nothing is created, then create a dummy data
	if(num_arrows == 0) {
		num_arrows = 1;
		arrows[0].start.x = 0;
		arrows[0].start.y = 0;
		arrows[0].end.x = 4000;
		arrows[0].end.y = 3000;
	}
	if(goZone_size == 0) {
		goZone_size = 1;
		x = 2000;
	    y = 1000;
	    Point pt = {x, y};
	    Box box = {pt, 10, 10};
	    goZone[0] = box;
	}
*/

	//Check if all arrows are in the go zone
	int found = 0;
	//loop through evrey arrows
	for(int i=0; i<num_arrows; i++) {
	
	    //check with evrey goZone box
		for(int j=0; j<goZone_size; j++) {
		
		    //if it is a vertical arrow
		    //arrows.tart.x is equals goZone.x  			
			if(arrows[i].start.x == goZone[j].center.x) {
			    //arrow.start.y must greater or equals to goZone's top and less then goZone's bottom
			    if((arrows[i].start.y == goZone[j].center.y-goZone[j].height/2 || arrows[i].start.y > goZone[j].center.y-goZone[j].height/2) && (arrows[i].start.y < goZone[j].center.y+goZone[j].height/2)) {
			        found += 1;
			    } else {
					found += 0;
				}
			} else {
				found += 0;
			}
			
			//if it is a horizontal arrow
		    //arrows.tart.y is equals goZone.y
			if(arrows[i].start.y == goZone[j].center.y) {
			    //arrow.start.x must greater or equals to goZone's left and less then goZone's right
			    if((arrows[i].start.x == goZone[j].center.x-goZone[j].width/2 || arrows[i].start.x > goZone[j].center.x-goZone[j].width/2) && (arrows[i].start.x < goZone[j].center.x+goZone[j].width/2)) {
			        found += 1;
			    } else {
					found += 0;
				}
			} else {
				found += 0;
			}
		}
		
		if(found < 1) {
			ERROR: goto ERROR;
		}
	}
}
