#include <assert.h>

//Point
typedef struct Point {
	float x;
	float y;
} Point;

//Arrow
#define MAX_ARROW_CONNECTORS 5
typedef struct Arrow {
	Point start;
	Point end;
	struct Arrow* endConnectors[MAX_ARROW_CONNECTORS];
	struct Arrow* startConnectors[MAX_ARROW_CONNECTORS];
} Arrow;
int num_arrows;
Arrow* arrows;

int main() {
	//Check if any arrow is off the map

	//loop through evrey arrows
	for(int i=0; i<num_arrows; i++) {
	    assert(arrows[i].start.x < 0);
		//assert(arrows[i].start.x > 4000);
		//assert(arrows[i].start.y < 0);
		//assert(arrows[i].start.y > 3000);
	}
	return 0;
}
