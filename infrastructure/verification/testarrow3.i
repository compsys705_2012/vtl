# 1 "testarrow3.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "testarrow3.c"

typedef struct Point {
 float x;
 float y;
} Point;



typedef struct Arrow {
 Point start;
 Point end;
 struct Arrow* endConnectors[5];
 struct Arrow* startConnectors[5];
} Arrow;
int num_arrows = 0;
Arrow* arrows;

int main() {
# 30 "testarrow3.c"
 int found = 0;

 for(int i=0; i<num_arrows; i++) {

  if(arrows[i].start.x < 0 || arrows[i].start.x > 4000 || arrows[i].start.y < 0 || arrows[i].start.y > 3000) {
   found += 1;
  } else {
   found += 0;
  }

  if(arrows[i].end.x < 0 || arrows[i].end.x > 4000 || arrows[i].end.y < 0 || arrows[i].end.y > 3000) {
   found += 1;
  } else {
   found += 0;
  }

  if(found > 0) {
   ERROR: goto ERROR;
  }
 }
}
