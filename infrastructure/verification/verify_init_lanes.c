#include "stdbool2.h"
#include "dummy_globals.h"

void init_lanes()
{
	BOOL fourIntersections = FALSE;
	//lane_ids for each lane are really important for vehicle group
	//they need to be assigned by infrastructure when new lanes are created
	//this is the way that we know how to move cars between lanes
	//please follow id format that is laid out in <lane_info.txt>
	
	for (int i = 0; i < NUM_OF_LANES; ++i)
	{
	    all_lanes[i].lane_id = i;
	}
	
    // Attemp to find four intersections as defined in the .txt file.
    Point* topLeft;
    Point* topRight;
    Point* botLeft;
    Point* botRight;
    
    int minX = 4000;
    int minY = 4000;
    
    int test[4];

    intersections = (Intersection*) malloc(4 * sizeof(Intersection));
    intersections[0].origin.x = 3800;
    intersections[0].origin.y = 3800;

    for (int n = 0; n < number_of_intersections; ++n)
    {
        if (intersections[n].origin.x > 3900)
        {
            intersections[n].origin.x = 3500;
        }
        if (intersections[n].origin.y > 3900)
        {
            intersections[n].origin.y = 3500;
        }
    }

    // Find the most top left intersection
    for (int n = 0; n < number_of_intersections; ++n)
    {
        if (intersections[n].origin.x < minX && intersections[n].origin.y < minY)
        {
            printf("Infrastructure: Found top left (%f,%f)\n", intersections[n].origin.x, intersections[n].origin.y);
            fourIntersections = TRUE;
            topLeft = &intersections[n].origin;
            minX = intersections[n].origin.x;
            minY = intersections[n].origin.y;
        }
    }
    if (fourIntersections == FALSE)
    {
        ERROR: goto ERROR;
    }
    
    // Find the topRight intersection
    if (fourIntersections == TRUE)
    {
        fourIntersections = FALSE;
        minX = 4000;
        for (int n = 0; n < number_of_intersections; ++n)
        {
            if (intersections[n].origin.x < minX && intersections[n].origin.x > topLeft->x && intersections[n].origin.y == topLeft->y)
            {
                printf("Infrastructure: Found top right (%f,%f)\n", intersections[n].origin.x, intersections[n].origin.y);
                fourIntersections = TRUE;
                topRight = &intersections[n].origin;
            }
        }
    }
    
    // Find the bottomLeft intersection
    if (fourIntersections == TRUE)
    {
        fourIntersections = FALSE;
        minY = 4000;
        for (int n = 0; n < number_of_intersections; ++n)
        {
            if (intersections[n].origin.y < minY && intersections[n].origin.y > topLeft->y && intersections[n].origin.x == topLeft->x)
            {   
                printf("Infrastructure: Found bot left (%f,%f)\n", intersections[n].origin.x, intersections[n].origin.y);
                fourIntersections = TRUE;
                botLeft = &intersections[n].origin;
            }
        }
    }
    
    // Find the bottomRight intersection
    if (fourIntersections == TRUE)
    {
        fourIntersections = FALSE;
        for (int n = 0; n < number_of_intersections; ++n)
        {
            if (intersections[n].origin.x == topRight->x && intersections[n].origin.y == botLeft->y)
            {
                printf("Infrastructure: Found bot right (%f,%f)\n", intersections[n].origin.x, intersections[n].origin.y);
                fourIntersections = TRUE;
                botRight = &intersections[n].origin;
            }
        }
    }
    
    //Find the topRight intersection
    

    Point* centre = &intersections[0].origin;

    // Construct the original 1 intersection model, if we can't find a four intersection structure
    if (fourIntersections == FALSE)
    {
        printf("Using one intersection\n");
	    all_lanes[0].direction = WE;
	    Point start_pos0 = { 0, centre->y - intersection_width/4 };
	    Point end_pos0 = { centre->x, centre->y - intersection_width/4 };
	    all_lanes[0].start_pos = start_pos0;
	    all_lanes[0].end_pos = end_pos0;

	    all_lanes[1].direction = WE;
	    Point start_pos1 = { centre->x, centre->y - intersection_width/4 };
	    Point end_pos1 = { MAP_WIDTH, centre->y - intersection_width/4 };
	    all_lanes[1].start_pos = start_pos1;
	    all_lanes[1].end_pos = end_pos1;
	
	    all_lanes[2].direction = EW;
	    Point start_pos2 = { MAP_WIDTH, centre->y + intersection_width/8 };
	    Point end_pos2 = { centre->x, centre->y + intersection_width/8 };
	    all_lanes[2].start_pos = start_pos2;
	    all_lanes[2].end_pos = end_pos2;

	    all_lanes[3].direction = EW;
	    Point start_pos3 = { centre->x, centre->y + intersection_width/8 };
	    Point end_pos3 = { 0, centre->y + intersection_width/8 };
	    all_lanes[3].start_pos = start_pos3;
	    all_lanes[3].end_pos = end_pos3;

	    all_lanes[4].direction = SN;
	    Point start_pos4 = { centre->x - intersection_width/4, MAP_HEIGHT };
	    Point end_pos4 = { centre->x - intersection_width/4, centre->y };
	    all_lanes[4].start_pos = start_pos4;
	    all_lanes[4].end_pos = end_pos4;

	    all_lanes[5].direction = SN;
	    Point start_pos5 = { centre->x - intersection_width/4, centre->y };
	    Point end_pos5 = { centre->x - intersection_width/4, 0 };
	    all_lanes[5].start_pos = start_pos5;
	    all_lanes[5].end_pos = end_pos5;

	    all_lanes[6].direction = NS;
	    Point start_pos6 = { centre->x + intersection_width/8, 0 };
	    Point end_pos6 = { centre->x + intersection_width/8, centre->y };
	    all_lanes[6].start_pos = start_pos6;
	    all_lanes[6].end_pos = end_pos6;

	    all_lanes[7].direction = NS;
	    Point start_pos7 = { centre->x + intersection_width/8, centre->y };
	    Point end_pos7 = { centre->x + intersection_width/8, MAP_HEIGHT };
	    all_lanes[7].start_pos = start_pos7;
	    all_lanes[7].end_pos = end_pos7;
	}
	// Construct the four intersection model
	else
	{
        printf("Using four intersections\n");
	    // WE -------- top
	    all_lanes[0].direction = WE;
	    Point start_pos0 = { 0, topLeft->y - intersection_width/4 };
	    Point end_pos0 = { topLeft->x, topLeft->y - intersection_width/4 };
	    all_lanes[0].start_pos = start_pos0;
	    all_lanes[0].end_pos = end_pos0;

	    all_lanes[1].direction = WE;
	    Point start_pos1 = { topLeft->x, topLeft->y - intersection_width/4 };
	    Point end_pos1 = { topRight->x, topLeft->y - intersection_width/4 };
	    all_lanes[1].start_pos = start_pos1;
	    all_lanes[1].end_pos = end_pos1;
	    
	    all_lanes[22].direction = WE;
	    Point start_pos22 = { topRight->x, topLeft->y - intersection_width/4 };
	    Point end_pos22 = { MAP_WIDTH, topLeft->y - intersection_width/4 };
	    all_lanes[22].start_pos = start_pos22;
	    all_lanes[22].end_pos = end_pos22;
	    
	    // WE -------- bot
	    all_lanes[8].direction = WE;
	    Point start_pos8 = { 0, botLeft->y - intersection_width/4 };
	    Point end_pos8 = { botLeft->x, botLeft->y - intersection_width/4 };
	    all_lanes[8].start_pos = start_pos8;
	    all_lanes[8].end_pos = end_pos8;

	    all_lanes[9].direction = WE;
	    Point start_pos9 = { botLeft->x, botLeft->y - intersection_width/4 };
	    Point end_pos9 = { botRight->x, botLeft->y - intersection_width/4 };
	    all_lanes[9].start_pos = start_pos9;
	    all_lanes[9].end_pos = end_pos9;
	    
	    all_lanes[10].direction = WE;
	    Point start_pos10 = { botRight->x, botLeft->y - intersection_width/4 };
	    Point end_pos10 = { MAP_WIDTH, botLeft->y - intersection_width/4 };
	    all_lanes[10].start_pos = start_pos10;
	    all_lanes[10].end_pos = end_pos10;
	
	    // EW -------- top
	    all_lanes[23].direction = EW;
	    Point start_pos23 = { MAP_WIDTH, topRight->y + intersection_width/8 };
	    Point end_pos23 = { topRight->x, topRight->y + intersection_width/8 };
	    all_lanes[23].start_pos = start_pos23;
	    all_lanes[23].end_pos = end_pos23;
	    
	    all_lanes[2].direction = EW;
	    Point start_pos2 = { topRight->x, topRight->y + intersection_width/8 };
	    Point end_pos2 = { topLeft->x, topRight->y + intersection_width/8 };
	    all_lanes[2].start_pos = start_pos2;
	    all_lanes[2].end_pos = end_pos2;

	    all_lanes[3].direction = EW;
	    Point start_pos3 = { topLeft->x, topRight->y + intersection_width/8 };
	    Point end_pos3 = { 0, topRight->y + intersection_width/8 };
	    all_lanes[3].start_pos = start_pos3;
	    all_lanes[3].end_pos = end_pos3;
	    
	    // EW -------- bot
	    all_lanes[11].direction = EW;
	    Point start_pos11 = { MAP_WIDTH, botRight->y + intersection_width/8 };
	    Point end_pos11 = { botRight->x, botRight->y + intersection_width/8 };
	    all_lanes[11].start_pos = start_pos11;
	    all_lanes[11].end_pos = end_pos11;
	    
	    all_lanes[12].direction = EW;
	    Point start_pos12 = { botRight->x, botRight->y + intersection_width/8 };
	    Point end_pos12 = { botLeft->x, botRight->y + intersection_width/8 };
	    all_lanes[12].start_pos = start_pos12;
	    all_lanes[12].end_pos = end_pos12;

	    all_lanes[13].direction = EW;
	    Point start_pos13 = { botLeft->x, botRight->y + intersection_width/8 };
	    Point end_pos13 = { 0, botRight->y + intersection_width/8 };
	    all_lanes[13].start_pos = start_pos13;
	    all_lanes[13].end_pos = end_pos13;

        // SN -------- left
	    all_lanes[20].direction = SN;
	    Point start_pos20 = { botLeft->x - intersection_width/4, MAP_HEIGHT };
	    Point end_pos20 = { botLeft->x - intersection_width/4, botLeft->y };
	    all_lanes[20].start_pos = start_pos20;
	    all_lanes[20].end_pos = end_pos20;
        
	    all_lanes[4].direction = SN;
	    Point start_pos4 = { botLeft->x - intersection_width/4, botLeft->y };
	    Point end_pos4 = { botLeft->x - intersection_width/4, topLeft->y };
	    all_lanes[4].start_pos = start_pos4;
	    all_lanes[4].end_pos = end_pos4;

	    all_lanes[5].direction = SN;
	    Point start_pos5 = { botLeft->x - intersection_width/4, topLeft->y };
	    Point end_pos5 = { botLeft->x - intersection_width/4, 0 };
	    all_lanes[5].start_pos = start_pos5;
	    all_lanes[5].end_pos = end_pos5;
	    
        // SN -------- right
	    all_lanes[14].direction = SN;
	    Point start_pos14 = { botRight->x - intersection_width/4, MAP_HEIGHT };
	    Point end_pos14 = { botRight->x - intersection_width/4, botRight->y };
	    all_lanes[14].start_pos = start_pos14;
	    all_lanes[14].end_pos = end_pos14;
        
	    all_lanes[15].direction = SN;
	    Point start_pos15 = { botRight->x - intersection_width/4, botRight->y };
	    Point end_pos15 = { botRight->x - intersection_width/4, topRight->y };
	    all_lanes[15].start_pos = start_pos15;
	    all_lanes[15].end_pos = end_pos15;

	    all_lanes[16].direction = SN;
	    Point start_pos16 = { botRight->x - intersection_width/4, topRight->y };
	    Point end_pos16 = { botRight->x - intersection_width/4, 0 };
	    all_lanes[16].start_pos = start_pos16;
	    all_lanes[16].end_pos = end_pos16;

        // NS -------- left
	    all_lanes[6].direction = NS;
	    Point start_pos6 = { topLeft->x + intersection_width/8, 0 };
	    Point end_pos6 = { topLeft->x + intersection_width/8, topLeft->y };
	    all_lanes[6].start_pos = start_pos6;
	    all_lanes[6].end_pos = end_pos6;

	    all_lanes[7].direction = NS;
	    Point start_pos7 = { topLeft->x + intersection_width/8, topLeft->y };
	    Point end_pos7 = { topLeft->x + intersection_width/8, botLeft->y };
	    all_lanes[7].start_pos = start_pos7;
	    all_lanes[7].end_pos = end_pos7;
	    
	    all_lanes[21].direction = NS;
	    Point start_pos21 = { topLeft->x + intersection_width/8, botLeft->y };
	    Point end_pos21 = { topLeft->x + intersection_width/8, MAP_HEIGHT };
	    all_lanes[21].start_pos = start_pos21;
	    all_lanes[21].end_pos = end_pos21;
	    
        // NS -------- right
	    all_lanes[17].direction = NS;
	    Point start_pos17 = { topRight->x + intersection_width/8, 0 };
	    Point end_pos17 = { topRight->x + intersection_width/8, topRight->y };
	    all_lanes[17].start_pos = start_pos17;
	    all_lanes[17].end_pos = end_pos17;

	    all_lanes[18].direction = NS;
	    Point start_pos18 = { topRight->x + intersection_width/8, topRight->y };
	    Point end_pos18 = { topRight->x + intersection_width/8, botRight->y };
	    all_lanes[18].start_pos = start_pos18;
	    all_lanes[18].end_pos = end_pos18;
	    
	    all_lanes[19].direction = NS;
	    Point start_pos19 = { topRight->x + intersection_width/8, botRight->y };
	    Point end_pos19 = { topRight->x + intersection_width/8, MAP_HEIGHT };
	    all_lanes[19].start_pos = start_pos19;
	    all_lanes[19].end_pos = end_pos19;
	}
}

int main()
{
    init_lanes();
    return 0;
}

