//Point
typedef struct Point {
	float x;
	float y;
} Point;

//Arrow
#define MAX_ARROW_CONNECTORS 5
typedef struct Arrow {
	Point start;
	Point end;
	struct Arrow* endConnectors[MAX_ARROW_CONNECTORS];
	struct Arrow* startConnectors[MAX_ARROW_CONNECTORS];
} Arrow;
int num_arrows = 0;
Arrow* arrows;

int main() {
/*
	//If nothing is created, then create a dummy arrow data
	if(num_arrows == 0) {
		num_arrows = 1;
		arrows[0].start.x = 200;
		arrows[0].start.y = 200;
		arrows[0].end.x = 100;
		arrows[0].end.y = 100;
	}
*/
	//Check if any arrow is off the map
	int found = 0;
	//loop through evrey arrows
	for(int i=0; i<num_arrows; i++) {
		//if start point or end point is less then zero		
		if(arrows[i].start.x < 0 || arrows[i].start.x > 4000 || arrows[i].start.y < 0 || arrows[i].start.y > 3000) {
			found += 1;
		} else {
			found += 0;
		}
		
		if(arrows[i].end.x < 0 || arrows[i].end.x > 4000 || arrows[i].end.y < 0 || arrows[i].end.y > 3000) {
			found += 1;
		} else {
			found += 0;
		}
		
		if(found > 0) {
			ERROR: goto ERROR;
		}
	}
}
