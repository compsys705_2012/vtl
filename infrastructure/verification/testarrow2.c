//Point
typedef struct Point {
	float x;
	float y;
} Point;

//Arrow
#define MAX_ARROW_CONNECTORS 5
typedef struct Arrow {
	Point start;
	Point end;
	struct Arrow* endConnectors[MAX_ARROW_CONNECTORS];
	struct Arrow* startConnectors[MAX_ARROW_CONNECTORS];
} Arrow;
int num_arrows = 0;
Arrow* arrows;

int main() {
/*
	//If nothing is created, then create a dummy arrow data
	if(num_arrows == 0) {
		num_arrows = 1;
		arrows[0].start.x = 200;
		arrows[0].start.y = 200;
		arrows[0].end.x = 100;
		arrows[0].end.y = 100;
	}*/
    
	//Check if all arrows have end point greater or equals to start point
	int found = 0;
	//loop through evrey arrows
	for(int i=0; i<num_arrows; i++) {
		//if start point is greater then end point  			
		if(arrows[i].start.x > arrows[i].end.x) {
			found += 1;
		} else {
			found += 0;
		}
		
		if(arrows[i].start.y > arrows[i].end.y) {
			found += 1;
		} else {
			found += 0;
		}
		
		if(found > 0) {
			ERROR: goto ERROR;
		}
	}
}
