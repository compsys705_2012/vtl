This is the git repository for the COMPSYS 705 VTL assignment.

Each group has its own folder and source files. Shared code has been kept in
globals.h and globals.c.

To compile the project type:
    make

To run the program type:
    ./vtl

To compile, GTK development files need to be installed. On Ubuntu use:
    sudo apt-get install libgtk2.0-dev

