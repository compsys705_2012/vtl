// This file initialises some global variables and functions

#include "globals.h"

float distance(Point from, Point to) {
    return sqrt(pow(from.x - to.x, 2) + pow(from.y - to.y, 2));
}

void init_arrow(Arrow *arrow, float x1, float y1, float x2, float y2) {
    arrow->start = (Point) { .x = x1, .y = y1 };
    arrow->end = (Point) { .x = x2, .y = y2 };
}

    // Attemp to find four intersections as defined in the .txt file.
    Point* topLeft;
    Point* topRight;
    Point* botLeft;
    Point* botRight;

