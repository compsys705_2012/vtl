#include "globals.h"

#define __VTL_C__

int max_goZone = 0;
int goZone_size = 0;
Box* goZone;

int num_arrows = 0;
Arrow* arrows;
int arrowNextInArray = 0;

// Traffic lights for the intersection. 2 Traffic lights per intersction.
// Currently only defines 1 intersection.
IntersectionLights* intersectionLights;

// Tempory variable only used for arrow construction
IntersectionConstructor* intersectionConstructors;

/** 
 * This is pretty much the primary structure of this simulation
 * It is the array of all the lanes in the simulation.
 * Each lane defines the physical setup of a lane
 * Each lane contains an array of Cars on that lane
 * Much of the simulation processing is done around this structure
 */
 //Now 24 to support 4 intersections
Lane all_lanes[NUM_OF_LANES];

/*---------------------------Dimensions-------------------------------*/
/**
 * The units of all the dimensions below are pixels right??
 */
const float CAR_LENGTH = 40.0;
const float CAR_WIDTH = 20.0;

//The dimensions of the map. See globals.h for a decription of
//why these are commented out
//const float MAP_WIDTH = 3000.0;
//const float MAP_HEIGHT = 3000.0;

const float intersection_width = 100.0;
const float intersection_height = 100.0;
const float intersection_padding = 20.0; // How much gap to leave between intersections

const float TRAFFIC_LIGHT_WIDTH = 20.0;
const float TRAFFIC_LIGHT_HEIGHT = 20.0;

//Assuming the avererage family car is 4.4m long (See en.wikipedia.org/wiki/large_family_car)
//And that a car is 40 pixels long, then 40/4.4 = 9.1 pixels/m
#define PIXELS_PER_M 9.1

/*---------------------------End Dimensions-------------------------------*/

//this is in km/h and can vary between 50 and 100;
int global_speed_limit = 50;

//refresh rate of simulation
const float UPDATES_PER_SECOND = (float)MILLISECONDS_PER_SECOND / MILLISECONDS_PER_UPDATE;

Intersection* intersections;
int number_of_intersections = 0;
int max_number_of_intersections = 0;

int to_continue = 1;


/**
 * Inconspicuous main function
 */
int main( int argc, char *argv[])
{
    init_gui();
    while (to_continue) {
        update_gui();
    }
    return 0;
}

/*============================ UTILITY FUNCTIONS ===============================*/

//Calculates the total number of cars in the simulation
//This is done by adding up the "count" variables from every lane
int total_cars_in_all_lanes() {
	int i, total_cars = 0;
	for (i = 0; i < NUM_OF_LANES; i++) {
		total_cars += all_lanes[i].count;
	}
	return total_cars;
}

//This function is used to test if one point is less than another
//The assumption of this function is that it will be called for two points
//that have an identical x or y co-ordinate
bool point_less_than(Point lhs, Point rhs) {
	if (lhs.x == rhs.x) {
		return (lhs.y < rhs.y);
	}
	if (lhs.y == rhs.y) {
		return (lhs.x < rhs.x);
	}
	return ((lhs.y < rhs.y) && (lhs.x < rhs.x));
}

//This function is used to test if one point is greater than another
//The assumption of this function is that it will be called for two points
//that have an identical x or y co-ordinate
bool point_greater_than(Point lhs, Point rhs) {
	if (lhs.x == rhs.x) {
		return (lhs.y > rhs.y);
	}
	if (lhs.y == rhs.y) {
		return (lhs.x > rhs.x);
	}
	return ((lhs.y > rhs.y) && (lhs.x > rhs.x));
}

//Returns true if these two points have identical x and y co-ords
bool point_equal_to(Point lhs, Point rhs) {
	return (lhs.x == rhs.x) && (lhs.y == rhs.y);
}

//Returns true if these two points have identical y co-ords
bool indentical_y_coord(Point lhs, Point rhs) {
	return (lhs.y == rhs.y);
}

//Returns true if these two points have identical x co-ords
bool indentical_x_coord(Point lhs, Point rhs) {
	return (lhs.x == rhs.x);
}





